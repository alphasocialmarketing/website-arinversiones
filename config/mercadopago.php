<?php

return [
	'app_id'     => env('MP_APP_ID', ''),
	'app_secret' => env('MP_APP_SECRET', ''),
	'sandbox_mode' => env('MP_SANDBOX_MODE', true),
];