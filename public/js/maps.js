var properties = [];
var loteos = [];
var map = null;
var markers = null;

function drawInfoWindow_prop(property) {

    var image = '';
    if (property.image) {
        image = property.image
    }

    var title = '';
    if (property.title) {
        title = property.title
    }

    var address = '';
    if (property.address) {
        address = property.address
    }

    var description = '';
    if (property.description) {
        description = property.description
    }

    var area = 0;
    if (property.area) {
        area = property.area
    }

    var Beds = 0;
    if (property.bedroom) {
        Beds = property.bedroom
    }

    var bathroom = 0;
    if (property.bathroom) {
        bathroom = property.bathroom
    }

    var habitaciones = 0;
    if (property.habitaciones) {
        habitaciones = property.habitaciones
    }

    var garage = 0;
    if (property.garage) {
        garage = property.garage
    }

    var price = 0;
    if (property.price) {
        price = property.price
    }
    var intro = '';
    if (property.intro) {
        intro = property.intro;
    }
    var slug = '';
    if (property.slug) {
        slug = property.slug;
    }

    var mostrar = '';
    if (property.mostrar) {
        mostrar = property.mostrar;
    }

    var moneda = '';
    if(property.moneda == "PESO"){
        moneda = "AR$"
    }
    else{
        moneda = "U$S"
    }

    var str_precio="";

        if(mostrar)
            str_precio = "<div class='map-properties-btns'><a href='"+slug+"' class='button-sm button-theme'>Detalle</a></div>"
        else
            str_precio = "<div class='map-properties-btns'><a href='#' class='border-button-sm border-button-theme' style='margin-right:5px;'>" + moneda + "" + price + "</a><a href='"+slug+"' class='button-sm button-theme'>Detalle</a></div>" 

    var ibContent = '';
    ibContent =
        "<div class='map-properties'>" +
        "<div class='map-img'>" +
        "<img src='" + image + "' style='height:200px;'/>" +
        "</div>" +
        "<div class='map-content'>" +
        "<div class='map-content-top'><h4><a href='"+slug+"'>" + title + "</a></h4>" +
        "<p class='address'> <i class='fa fa-map-marker'></i>" + address + "</p></div>" +
        "<p class='description'>" + intro + "</p>" +
        "<div class='map-properties-fetures'> " +
        "<span><i class='flaticon-square-layouting-with-black-square-in-east-area'></i>  " + area + " M<sup>2</sup></span> " +
        "<span><i class='flaticon-bed'></i>  " + habitaciones + " Habitaciones</span> " +
        "<span><i class='flaticon-holidays'></i>  " + bathroom + " Baños</span> " +
        "<span><i class='flaticon-vehicle'></i>    " + garage + " Garage</span> " +
        "</div>" +        
        str_precio +
        "</div>";
    return ibContent;
}

function drawInfoWindow_lot(lote) {

    var image = '';
    if (lote.image) {
        image = lote.image
    }

    var title = '';
    if (lote.title) {
        title = lote.title
    }

    var address = '';
    if (lote.address) {
        address = lote.address
    }

    var description = '';
    if (lote.description) {
        description = lote.description
    }

    var area = '';
    if (lote.area) {
        area = lote.area
    }

    var price = '';
    if (lote.price) {
        price = lote.price
    }
    var intro = '';
    if (lote.intro) {
        intro = lote.intro;
    }
    var slug = '';
    if (lote.slug) {
        slug = lote.slug;
    }

    var cerrado = '';
    if (lote.cerrado) {
        cerrado = lote.cerrado;
    }

    var posesion = '';
    if (lote.posesion) {
        posesion = lote.posesion;
    }

    var financiado = '';
    if (lote.financiado) {
        financiado = lote.financiado;
    }
    
    var moneda = '';
    if(lote.moneda=="PESO"){
        moneda = "AR$"
    }
    else{
        moneda = "U$S"
    }

    var ibContent = '';
    ibContent =
        "<div class='map-properties'>" +
        "<div class='map-img'>" +
        "<img src='" + image + "' style='height:200px;'/>" +
        "</div>" +
        "<div class='map-content'>" +
        "<div class='map-content-top'><h4><a href='"+slug+"'>" + title + "</a></h4>" +
        "<p class='address'> <i class='fa fa-map-marker'></i>" + address + "</p></div>" +
        "<p class='description'>" + intro + "</p>" +
        "<div class='map-properties-btns'><a href='"+slug+"' class='button-sm button-theme'>Detalle</a></div>" +
        "</div>";
    return ibContent;
}

function insertPropertyToArray_prop(property, layout) {

    var image = 'img/logo.png';
    if (property.image) {
        image = property.image
    }

    var title = '';
    if (property.title) {
        title = property.title
    }

    var address = '';
    if (property.address) {
        address = property.address
    }

    var description = '';
    if (property.description) {
        description = property.description
    }

    var area = 0;
    if (property.area) {
        area = property.area
    }

    var Beds = 0;
    if (property.bedroom) {
        Beds = property.bedroom
    }

    var bathroom = 0;
    if (property.bathroom) {
        bathroom = property.bathroom
    }

    var habitaciones = 0;
    if (property.habitaciones) {
        habitaciones = property.habitaciones
    }

    var garage = 0;
    if (property.garage) {
        garage = property.garage
    }

    var price = 0;
    if (property.price) {
        price = property.price
    }

    var author = '';
    if (property.author) {
        author = property.author.nombre;
    }

    var barrio = '';
    if (property.barrio) {
        barrio = property.barrio.nombre;
    }

    var slug = '';
    if (property.slug) {
        slug = property.slug;
    }
    var intro = '';
    if (property.intro) {
        intro = property.intro;
    }

    var moneda = '';
    if(property.moneda=="PESO"){
        moneda = "AR$"
    }
    else{
        moneda = "U$S"
    }
    var mostrar = '';
    if (property.mostrar) {
        mostrar = property.mostrar;
    }

    var element = '';

    if(layout == 'grid_layout'){
        element = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="property">' +
            '<!-- Property img --> ' +
            '<a href="'+slug+'" class="property-img">' +                    
            '<div class="property-price" style="background: #802d39;">' + moneda + '' + price + '</div> ' +
            '<img src="'+image+'" alt="properties-3" class="img-responsive"> ' +
            '</a>' +
            '<!-- Property content --> ' +
            '<div class="property-content"> ' +
            '<!-- title --> ' +
            '<h1 class="title">' +
            '<a href="'+slug+'">'+title+'</a> ' +
            '</h1> ' +
            '<!-- Property address --> ' +
            '<h3 class="property-address"> ' +
            '<a href="'+slug+'"> ' +
            '<i class="fa fa-map-marker"></i>'+address+' ' +
            '</a> ' +
            '</h3> ' +
            '<!-- Facilities List --> ' +
            '<ul class="facilities-list clearfix"> ' +
            '<li> ' +
            '<i class="flaticon-square-layouting-with-black-square-in-east-area"></i> ' +
            '<span>'+area+' M2</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-bed"></i> ' +
            '<span>'+ habitaciones +' Habitaciones</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-holidays"></i> ' +
            '<span> '+bathroom+' Baños</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-vehicle"></i> ' +
            '<span>'+garage+' Garage</span> ' +            
            '</ul> ' +
            '<!-- Property footer --> ' +
            '<div class="property-footer"> ' +
            '<a class="left"><i class="fa fa-user"></i>'+author+'</a> ' +
            '<a class="right"><i class="fa fa-home"></i>'+barrio+'</a> ' +
            '</div>' +            
            '</div>' +
            '</div>';
            '</div>';
    }
    else{
        var str_precio="";

        if(mostrar)
            str_precio = "" 
        else
            str_precio = '<div class="property-price" style="background: #802d39;">' + moneda + '' + price + '</div> '


        element = '' +
            '<div class="property map-properties-list clearfix"> ' +
            '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-pad"> ' +
            '<a href="'+slug+'" class="property-img height"> ' +
            str_precio + 
            '<img src="'+image+'" alt="properties" class="img-responsive img-inside-map"> ' +
            '</a> ' +
            '</div> ' +
            '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 property-content "> ' +
            '<!-- title --> ' +
            '<h1 class="title"> ' +
            '<a href="'+slug+'">'+title+'</a> </h1> ' +
            '<!-- Property address --> ' +
            '<h3 class="property-address"> ' +
            '<a href="'+slug+'"> ' +
            '<i class="fa fa-map-marker"></i>'+address+', ' +
            '</a>' +
            '</h3>' +
            '<!-- Facilities List --> ' +
            '<ul class="facilities-list clearfix"> ' +
            '<li> ' +
            '<i class="flaticon-square-layouting-with-black-square-in-east-area"></i> ' +
            '<span>'+area+' M2</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-bed"></i> ' +
            '<span>'+habitaciones+' Habitacion</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-holidays"></i>' +
            '<span> '+bathroom+' Baños</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-vehicle"></i>' +
            '<span>'+garage+' Garage</span>' +
            '</li> ' +            
            '</ul> ' +
            '<!-- Property footer --> ' +
            '<div class="property-footer"> ' +
            '<a class="left"><i class="fa fa-user"></i>'+author+'</a> ' +
            '<a class="right"><i class="fa fa-home"></i>'+barrio+'</a> ' +
            '</div> ' +            
            '</div> ' +
            '</div>';
    }
    return element;
}

function insertPropertyToArray_lot(lote, layout) {

    var image = 'img/logo.png';
    if (lote.image) {
        image = lote.image
    }

    var title = '';
    if (lote.title) {
        title = lote.title
    }

    var address = '';
    if (lote.address) {
        address = lote.address
    }

    var description = '';
    if (lote.description) {
        description = lote.description
    }

    var area = '';
    if (lote.area) {
        area = lote.area
    }

    var price = '';
    if (lote.price) {
        price = lote.price
    }

    var author = '';
    if (lote.author) {
        author = lote.author.nombre;
    }

    var barrio = '';
    if (lote.barrio) {
        barrio = lote.barrio.nombre;
    }

    var slug = '';
    if (lote.slug) {
        slug = lote.slug;
    }
    var intro = '';
    if (lote.intro) {
        intro = lote.intro;
    }

    var cerrado = '';
    if (lote.cerrado) {
        cerrado = lote.cerrado;
    }

    var posesion = '';
    if (lote.posesion) {
        posesion = lote.posesion;
    }

    var financiado = '';
    if (lote.financiado) {
        financiado = lote.financiado;
    }

    var moneda = '';
    if(lote.moneda=="PESO"){
        moneda = "AR$"
    }
    else{
        moneda = "U$S"
    }

    var element = '';

    if(layout == 'grid_layout'){
        element = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="property">' +
            '<!-- Property img --> ' +
            '<a href="'+slug+'" class="property-img">' +
            '<img src="'+image+'" alt="properties-3" class="img-responsive"> ' +
            '</a>' +
            '<!-- Property content --> ' +
            '<div class="property-content"> ' +
            '<!-- title --> ' +
            '<h1 class="title">' +
            '<a href="'+slug+'">'+title+'</a> ' +
            '</h1> ' +
            '<!-- Property address --> ' +
            '<h3 class="property-address"> ' +
            '<a href="'+slug+'"> ' +
            '<i class="fa fa-map-marker"></i>'+address+' ' +
            '</a> ' +
            '</h3> ' +
            '<div class="property-footer"> ' +
            '<a class="left"><i class="fa fa-user"></i>'+author+'</a> ' +
            '<a class="right"><i class="fa fa-home"></i>'+barrio+'</a> ' +
            '</div>' +            
            '</div>' +
            '</div>';
            '</div>';
    }
    else{
        element = '' +
            '<div class="property map-properties-list clearfix"> ' +
            '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-pad"> ' +
            '<a href="'+slug+'" class="property-img height"> ' +
            '<img src="'+image+'" alt="properties" class="img-responsive img-inside-map"> ' +
            '</a> ' +
            '</div> ' +
            '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 property-content "> ' +
            '<!-- title --> ' +
            '<h1 class="title"> ' +
            '<a href="'+slug+'">'+title+'</a> </h1> ' +
            '<!-- Property address --> ' +
            '<h3 class="property-address"> ' +
            '<a href="'+slug+'"> ' +
            '<i class="fa fa-map-marker"></i>'+address+', ' +
            '</a>' +
            '</h3>' +
            '<div class="property-footer"> ' +
            '<a class="left"><i class="fa fa-user"></i>'+author+'</a> ' +
            '</div> ' +            
            '</div> ' +
            '</div>';
    }
    return element;
}

function animatedMarkers(map, genericoMarkers, generico, layout, tipo) {

    var bounds = map.getBounds();
    var genericoArray = [];
       

    $.each(genericoMarkers, function (key, value) {  


        if (bounds.contains(genericoMarkers[key].getLatLng())) {
            
            if(tipo=="loteos")
                genericoArray.push(insertPropertyToArray_lot(generico[key], layout));
            else
                genericoArray.push(insertPropertyToArray_prop(generico[key], layout));

            setTimeout(function () {
                if (genericoMarkers[key]._icon != null) {
                    genericoMarkers[key]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable bounce-animation marker-loaded';
                }
            }, key * 50);
        }
        

        else {
            
            if (genericoMarkers[key]._icon != null) {
                genericoMarkers[key]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable';
            }
        }
    });
    $('.fetching-properties').html(genericoArray);
}

function generateMap(latitude, longitude, mapProvider, layout, tipo, url_api, zoom, scroll) {

    map = L.map('map', {
        center: [latitude, longitude],
        zoom: zoom,
        scrollWheelZoom: scroll
    });

    L.tileLayer.provider(mapProvider).addTo(map);
    markers = L.markerClusterGroup({        
        spiderfyOnMaxZoom: true,
        showCoverageOnHover: true,
        zoomToBoundsOnClick: true,

        iconCreateFunction: function(cluster) {
            return L.divIcon({ html: '<b style="background:#802d39;color:white;font-size:15px;padding:5px;border-radius:25%;">' + cluster.getChildCount() + '</b>' });
        }
    });
    var propertiesMarkers = [];
    var loteosMarkers = [];

    var request = $.ajax({
        url: url_api,
        type: "GET",
        contentType: "application/json; charset=utf-8",
    });

    request.done(function(result){      
                                               
        if(result["data"]!=undefined){
            if(tipo=="loteos"){
                $.each( result["data"], function( index, lote ){
                    loteos.push(lote);
                })
            }
            else{
                $.each( result["data"], function( index, property ){
                    properties.push(property);
                })
            }
        }
       
        if(tipo=="loteos"){
            $.each(loteos, function (index, lote) {

                var icon = '';
                if (lote.type_icon) {
                    icon = '<img src="' + lote.type_icon + '">';
                }
                var color = '#802d39';
                var markerContent =
                    '<div class="map-marker ' + color + '">' +
                    '<div class="icon">' +
                    icon +
                    '</div>' +
                    '</div>';

                var _icon = L.divIcon({
                    html: markerContent,
                    iconSize: [36, 46],
                    iconAnchor: [18, 32],
                    popupAnchor: [130, -28],
                    className: ''
                });

                var marker = L.marker(new L.LatLng(lote.latitude, lote.longitude), {
                    title: lote.title,
                    icon: _icon
                });

                loteosMarkers.push(marker);
                marker.bindPopup(drawInfoWindow_lot(lote));
                markers.addLayer(marker);
                marker.on('popupopen', function () {
                    this._icon.className += ' marker-active';
                });
                marker.on('popupclose', function () {
                    this._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
                });
                    
            });              
            
            map.addLayer(markers);
            animatedMarkers(map, loteosMarkers, loteos, layout, tipo);
            map.on('moveend', function () {
                animatedMarkers(map, loteosMarkers, loteos, layout, tipo);
            });      

            $('.fetching-properties .item').hover(
                function () {
                    loteosMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded marker-active';
                },
                function () {
                    loteosMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
                }
            );            
        }
        else{
            
            $.each(properties, function (index, property) {

                var icon = '';
                if (property.type_icon) {
                    icon = '<img src="' + property.type_icon + '">';
                }
                var color = '#802d39';
                var markerContent =
                    '<div class="map-marker ' + color + '">' +
                    '<div class="icon">' +
                    icon +
                    '</div>' +
                    '</div>';

                var _icon = L.divIcon({
                    html: markerContent,
                    iconSize: [36, 46],
                    iconAnchor: [18, 32],
                    popupAnchor: [130, -28],
                    className: ''
                });
                
                var marker = L.marker(new L.LatLng(property.latitude, property.longitude), {
                    title: property.title,
                    icon: _icon
                });

                propertiesMarkers.push(marker);
                marker.bindPopup(drawInfoWindow_prop(property));
                markers.addLayer(marker);
                marker.on('popupopen', function () {
                    this._icon.className += ' marker-active';
                });
                marker.on('popupclose', function () {
                    this._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
                });
                    
            }); 
            
            map.addLayer(markers);
            animatedMarkers(map, propertiesMarkers, properties, layout, tipo);
            map.on('moveend', function () {
                animatedMarkers(map, propertiesMarkers, properties, layout, tipo);
            });           

            $('.fetching-properties .item').hover(
                function () {
                    propertiesMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded marker-active';
                },
                function () {
                    propertiesMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
                }
            );
            
        }

        $('.geolocation').on("click", function () {
            map.locate({setView: true})
        });
        $('#map').removeClass('fade-map');
    });
}

function actualizarMap(latitude, longitude, mapProvider, layout, tipo, data, zoom, scroll) {
    
    
    markers.clearLayers();
    map.eachLayer(function (layer) {
        map.removeLayer(layer);
    });
    map.remove();
    properties = []
    loteos = []
    
    map = L.map('map', {
        center: [latitude, longitude],
        zoom: zoom,
        scrollWheelZoom: scroll
    });    

    L.tileLayer.provider(mapProvider).addTo(map);
    markers = L.markerClusterGroup({        
        spiderfyOnMaxZoom: true,
        showCoverageOnHover: true,
        zoomToBoundsOnClick: true,

        iconCreateFunction: function(cluster) {
            return L.divIcon({ html: '<b style="background:#802d39;color:white;font-size:15px;padding:5px;border-radius:25%;">' + cluster.getChildCount() + '</b>' });
        }
    });
    var propertiesMarkers = [];
    var loteosMarkers = [];      
    
    if(data != undefined){
        if(tipo=="loteos"){
            $.each( data, function( index, lote ){
                loteos.push(lote);
            })
        }
        else{
            $.each( data, function( index, property ){
                properties.push(property);
            })
        }
    }
    
    if(tipo=="loteos"){
        $.each(loteos, function (index, lote) {

            var icon = '';
            if (lote.type_icon) {
                icon = '<img src="' + lote.type_icon + '">';
            }
            var color = '#802d39';
            var markerContent =
                '<div class="map-marker ' + color + '">' +
                '<div class="icon">' +
                icon +
                '</div>' +
                '</div>';

            var _icon = L.divIcon({
                html: markerContent,
                iconSize: [36, 46],
                iconAnchor: [18, 32],
                popupAnchor: [130, -28],
                className: ''
            });

            var marker = L.marker(new L.LatLng(lote.latitude, lote.longitude), {
                title: lote.title,
                icon: _icon
            });

            loteosMarkers.push(marker);
            marker.bindPopup(drawInfoWindow_lot(lote));
            markers.addLayer(marker);
            marker.on('popupopen', function () {
                this._icon.className += ' marker-active';
            });
            marker.on('popupclose', function () {
                this._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
            });
                
        });              
        
        map.addLayer(markers);
        animatedMarkers(map, loteosMarkers, loteos, layout, tipo);
        map.on('moveend', function () {
            animatedMarkers(map, loteosMarkers, loteos, layout, tipo);
        });      

        $('.fetching-properties .item').hover(
            function () {
                loteosMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded marker-active';
            },
            function () {
                loteosMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
            }
        );            
    }
    else{
        
        $.each(properties, function (index, property) {

            var icon = '';
            if (property.type_icon) {
                icon = '<img src="' + property.type_icon + '">';
            }
            var color = '#802d39';
            var markerContent =
                '<div class="map-marker ' + color + '">' +
                '<div class="icon">' +
                icon +
                '</div>' +
                '</div>';

            var _icon = L.divIcon({
                html: markerContent,
                iconSize: [36, 46],
                iconAnchor: [18, 32],
                popupAnchor: [130, -28],
                className: ''
            });
            
            var marker = L.marker(new L.LatLng(property.latitude, property.longitude), {
                title: property.title,
                icon: _icon
            });

            propertiesMarkers.push(marker);
            marker.bindPopup(drawInfoWindow_prop(property));
            markers.addLayer(marker);
            marker.on('popupopen', function () {
                this._icon.className += ' marker-active';
            });
            marker.on('popupclose', function () {
                this._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
            });
                
        }); 
        
        map.addLayer(markers);
        animatedMarkers(map, propertiesMarkers, properties, layout, tipo);
        map.on('moveend', function () {
            animatedMarkers(map, propertiesMarkers, properties, layout, tipo);
        });           

        $('.fetching-properties .item').hover(
            function () {
                propertiesMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded marker-active';
            },
            function () {
                propertiesMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
            }
        );    
    }

    $('.geolocation').on("click", function () {
        map.locate({setView: true})
    });
    $('#map').removeClass('fade-map');    

}