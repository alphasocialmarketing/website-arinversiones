$(document).ready(function () {

    var $buythemediv = '<div class="buy-theme buy-theme-fb xs-display-none" style="top:175px;">' +
                            '<a href="https://api.whatsapp.com/send?phone=543413692568" target="_blank">' +
                                '<i class="fa fa-whatsapp" aria-hidden="true" style="color:#fff;font-size: 20px;"></i>' +
                                '<span style="color: #fff !important;">Whatsapp</span>' +
                            '</a>' +
                        '</div>' +
                        '<div class="buy-theme buy-theme-in xs-display-none" style="top:225px;">' +
                            '<a href="https://www.instagram.com/arinversiones/" target="_blank">' +
                                '<i class="fa fa-instagram" aria-hidden="true" style="color:#fff;font-size: 20px;"></i>' +
                                '<span style="color: #fff !important;">Instagram</span>' +
                            '</a>' +
                        '</div>' +
                        '<div class="buy-theme buy-theme-yt xs-display-none" style="top:275px;">' +
                            '<a href="https://www.facebook.com/ARInversiones/" target="_blank">' +
                                '<i class="fa fa-facebook" aria-hidden="true" style="color:#fff;font-size: 20px;"></i>' +
                                '<span style="color: #fff !important;">Facebook</span>' +
                            '</a>' +
                        '</div>' +
                        '<div class="buy-theme buy-theme-tel xs-display-none" style="top:325px;">' +
                            '<a href="https://twitter.com/AR_Inversiones" target="_blank">' +
                                '<i class="fa fa-twitter" aria-hidden="true" style="color:#fff;font-size: 20px;"></i>' +
                                '<span style="color: #fff !important;">Twitter</span>' +
                            '</a>' +
                        '</div>' +
                        '<div class="buy-theme buy-theme-ws xs-display-none" style="top:375px;">' +
                            '<a href="tel:+5493416792590" target="_blank">' +
                                '<i class="fa fa-phone " aria-hidden="true" style="color:#fff;font-size: 20px;"></i>' +
                                '<span style="color: #fff !important;">Llamar</span>' +
                            '</a>' +
                        '</div>';

    $('body').append($buythemediv);

});