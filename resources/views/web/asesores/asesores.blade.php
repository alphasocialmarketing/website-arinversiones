@extends('index')

@section('content')

<!-- Sub banner start -->
        <div class="sub-banner overview-bgi">
            <div class="overlay">
                <div class="container">
                    <div class="breadcrumb-area">
                        <h1>Asesores</h1>
                        <ul class="breadcrumbs">
                            <li style="color: #fff !important;">Conocé nuestros asesores</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sub Banner end -->
	<!-- Agent section start -->
<div class="agent-section content-area" style="padding: 100px 0px 100px 0px !important;">
    <div class="container">        
        <div class="clearfix"></div>

        <div class="row">
            @if( !empty($data['asesores']) )
                @foreach( $data['asesores'] as $a)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <!-- Agent box 2start -->
                        <div class="agent-2 clearfix">
                            <div class="col-lg-5 col-md-5 col-sm-4 agent-theme-2">
                                <a href="{{ url('asesores/'.$a->slug) }}">
                                    @if(!empty($a->imagen))
                                        <img src="{{ asset($a->imagen) }}" alt="{{ $a->nombre }}" class="img-responsive">
                                    @else
                                        <img src="{{ asset('images/no-disponible.png') }}" alt="{{ $a->nombre }}" class="img-responsive">
                                    @endif
                                </a>
                                <!-- social list -->
                                <ul class="social-list clearfix">
                                    @if(!empty($a->facebook))
                                        <li>
                                            <a href="{{ url($a->facebook) }}" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($a->twitter))
                                        <li>
                                            <a href="{{ url($a->twitter) }}" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($a->instagram))
                                        <li>
                                            <a href="{{ url($a->instagram) }}" class="instagram">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($a->skype))
                                        <li>
                                            <a href="{{ url($a->skype) }}" class="skype">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        </li>
                                    @endif                                   
                                </ul>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-8 agent-content">
                                <h5>{{ $a->puesto }}</h5>
                                <br>
                                <h5>{{ $a->profesion }}</h5>
                                <h3>
                                    <a href="{{ url('asesores/'.$a->slug) }}">{{ $a->nombre }}</a>
                                </h3>
                                <ul>
                                    <li>
                                        <strong>Email:</strong><a href="mailto:{{ $a->email }}">{{ $a->email }}</a>
                                    </li>
                                    <li>
                                        <strong>Celular:</strong><a href="tel:{{ $a->celular }}"> {{ $a->celular }}</a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                        <!-- Agent box 2 end -->
                    </div>
                @endforeach
            @endif                      
        </div>        
    </div>     
</div>
<div class="intro-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <img src="{{ asset('img/logos/logo-blanco.png') }}" alt="logo-2">
                    </div>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <h3 style="font-weight: 600 !important;">Buscando comprar o alquilar alguna propiedad?</h3>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-12">
                        <a href="{{ url('/propiedades') }}" class="btn button-md button-theme">Ver Más</a>
                    </div>
                </div>
            </div>
        </div>
<!-- Agent section end -->

@endsection