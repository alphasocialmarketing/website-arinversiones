@extends('index')

@section('content')

        <!-- Sub banner start -->
        <div class="sub-banner overview-bgi">
            <div class="overlay">
                <div class="container">
                    <div class="breadcrumb-area">
                        <h1>{{ $data['asesores']->nombre }}</h1>
                        <ul class="breadcrumbs">
                            <li style="color: #fff !important;">{{ $data['asesores']->profesion }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sub Banner end -->

        <!-- Agent section start -->
        <div class="agent-section content-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <!-- Agent detail start -->
                        <div class="agent-detail clearfix">
                            <div class="col-lg-5 col-md-6 col-sm-5 agent-theme">
                                @if(!empty($data['asesores']->imagen))
                                    <img src="{{ asset($data['asesores']->imagen) }}" alt="{{ $data['asesores']->nombre }}" class="img-responsive">
                                @else
                                    <img src="{{ asset('images/no-disponible.png') }}" alt="{{ $data['asesores']->nombre }}" class="img-responsive">
                                @endif
                            </div>
                            <div class="col-lg-7 col-md-6 col-sm-7 agent-content clearfix">
                                <h5>{{ $data['asesores']->puesto }}</h5>
                                <h3>
                                    {{ $data['asesores']->nombre }}
                                </h3>
                                <h5>{{ $data['asesores']->profesion }}</h5>
                                <!-- Address list -->
                                <ul class="address-list">
                                    <li>
                                        <span>
                                            <i class="fa fa-envelope"></i>Email:
                                        </span>
                                        {{ $data['asesores']->email }}
                                    </li>
                                    <li>
                                        <span>
                                            <i class="fa fa-phone"></i>Celular:
                                        </span>
                                        {{ $data['asesores']->celular }}
                                    </li>
                                    @if(!empty($data['asesores']->facebook))
                                        <li>
                                            <a href="{{ url($data['asesores']->facebook) }}" class="facebook">
                                                <i class="fa fa-facebook"></i> Facebook
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($data['asesores']->twitter))
                                        <li>
                                            <a href="{{ url($data['asesores']->twitter) }}" class="twitter">
                                                <i class="fa fa-twitter"></i> Twitter
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($data['asesores']->instagram))
                                        <li>
                                            <a href="{{ url($data['asesores']->instagram) }}" class="instagram">
                                                <i class="fa fa-instagram"></i> Instagram
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($data['asesores']->skype))
                                        <li>
                                            <a href="{{ url($data['asesores']->skype) }}" class="skype">
                                                <i class="fa fa-skype"></i> Skype
                                            </a>
                                        </li>
                                    @endif
                                </ul>

                                <!--<div class="social-media">
                                    <ul class="social-list">
                                        <li><a href="agent-single.html#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="agent-single.html#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="agent-single.html#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="agent-single.html#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="agent-single.html#" class="rss-bg"><i class="fa fa-rss"></i></a></li>
                                    </ul>
                                </div>-->
                            </div>
                        </div>
                        <!-- Agent detail end -->

                        <div class="sidebar-widget clearfix biography">
                            <!-- Main Title 2 -->
                            <div class="main-title-2">
                                <h1><span>Biografía</span></h1>
                            </div>
                            <p>
                                {!! $data['asesores']->biografia !!}
                            </p>
                            <br>
                            <div class="main-title-2">
                                <h1><span>Información</span></h1>
                            </div>
                            <p>
                                {!! $data['asesores']->informacion !!}
                            </p>
                            
                        </div>

                        <!-- Recently properties start -->
                        <div class="recently-properties sidebar-widget clearfix biography" style="background-color: #fff !important;">
                            <!-- Main title -->
                            <div class="main-title-2">
                                <h1>Propiedades <span>a cargo</span></h1>
                            </div>
                            <div class="row">

                                @if( !empty($data['prop_rel']) )
                                    @foreach( $data['prop_rel'] as $a)
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <!-- Property 2 start -->
                                            <div class="property-2">
                                                <!-- Property img -->
                                                <div class="property-img">                                                    
                                                    <div class="price-ratings">
                                                        <div class="price" style="color: #802d39;background:#fff; ">U$S {{ $a->precio }}</div>
                                                    </div>
                                                    @if(!empty($a->principal))
                                                        <img src="{{ asset($a->principal) }}" alt="{{ $a->nombre }}" class="img-responsive" style="height: 300px;">
                                                    @else
                                                        <img src="{{ asset('images/no-disponible.png') }}" alt="{{ $a->nombre }}" class="img-responsive" style="height: 300px;">
                                                    @endif
                                                    <div class="property-overlay">
                                                        <a href="{{ url('propiedades/'.$a->slug) }}" class="overlay-link">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                        <div class="property-magnify-gallery">
                                                            <a href="{{ asset($a->principal) }}" class="overlay-link">
                                                                <i class="fa fa-expand"></i>
                                                            </a>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- content -->
                                                <div class="content">
                                                    <!-- title -->
                                                    <h4 class="title">
                                                        <a href="{{ url('propiedades/'.$a->slug) }}">{{ $a->nombre }}</a>
                                                    </h4>
                                                    
                                                    @if($a->direccion)
                                                        <!-- Property address -->
                                                        <h4 class="property-address" style="height: 40px;">
                                                            <i class="fa fa-map-marker"></i> {{ json_decode($a->direccion)->value }}                                                         
                                                        </h4>
                                                    @else
                                                        <!-- Property address -->
                                                        <h4 class="property-address" style="height: 40px;">
                                                            <i class="fa fa-map-marker"></i> SIN DIRECCION
                                                        </h4>                                                                                                        
                                                    @endif
                                                </div>
                                                <!-- Facilities List -->
                                                <ul class="facilities-list clearfix">
                                                    <li>
                                                        <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                                        <span>{{ $a->medidas }} M2</span>
                                                    </li>
                                                    <li>
                                                        <i class="flaticon-bed"></i>
                                                        <span>{{ $a->habitaciones }}</span>
                                                    </li>
                                                    <li>
                                                        <i class="flaticon-holidays"></i>
                                                        <span>{{ $a->banios }}</span>
                                                    </li>
                                                    @if(!empty($a->cochera))
                                                        <li>
                                                            <i class="flaticon-vehicle"></i>
                                                            <span>Si</span>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                            <!-- Property 2 end -->
                                        </div> 
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <!-- Partners block end -->
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12">
                        <div class="sidebar">
                            <div class="sidebar-widget contact-1">
                                <div class="main-title-2">
                                    <h1><span>Contacto</span></h1>
                                </div>
                                <div class="contact-form">
                                    <div id="mensajes_form" class="col-md-12 no-padding"></div>
                                    <form id="form_asesores">
                                        <input type="hidden" name="asesor_id" value="{{ $data['asesores']->id }}" /> 
                                        <div class="row">
                                            <div class="col-lg-12">                                                
                                                <div class="form-group your-name">
                                                    <input type="text" name="nombre" class="input-text" placeholder="Nombre">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group your-email">
                                                    <input type="email" name="email" class="input-text" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group your-phone">
                                                    <input type="text" name="asunto" class="input-text" placeholder="Asunto">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group your-phone">
                                                    <input type="text" name="telefono" class="input-text" placeholder="Telefono">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group message">
                                                    <textarea class="input-text" name="mensaje" placeholder=" Mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button id="boton_form_enviar" class="button-md button-theme btn-block">Enviar Mensaje</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Popular posts start -->
                            <div class="sidebar-widget popular-posts">
                                @if( !empty($data['relacionados']) )
                                    <div class="main-title-2">
                                        <h1>Te pueden <span>interesar</span></h1>
                                    </div>                                
                                    @foreach($data['relacionados'] as $d)
                                        <div class="media">
                                            <div class="media-left">
                                                @if(!empty($d->principal))
                                                    <img class="media-object" src="{{ asset($d->getImagen()) }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                                @else
                                                    <img class="media-object" src="{{ asset('images/no-disponible.png') }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                                @endif
                                            </div>
                                            <div class="media-body">
                                                <h3 class="media-heading">
                                                    <a href="{{ url('propiedades/'.$d->slug) }}">{{ $d->nombre }}</a>
                                                </h3>
                                                <div class="price">
                                                   U$S {{ $d->precio }}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                
                            </div>
                            <!-- Category posts start -->
                            <div class="sidebar-widget category-posts">
                                <div class="main-title-2">
                                    <h1><span>Categorias</span></h1>
                                </div>
                                <ul class="list-unstyled list-cat">
                                    <li><a href="{{ url('/propiedades') }}">Propiedades </a> <span>({{ $data['total_propiedades'] }})  </span></li>
                                    <li><a href="{{ url('/loteos') }}">Loteos  </a> <span>({{ $data['total_loteos'] }})  </span></li>
                                </ul>
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Agent section end -->        
@endsection


@section('script')

    <!-- FORMULARIO  -->
    <script type="text/javascript">    
        $("#boton_form_enviar").click(function(event){

            event.preventDefault();
            
            var url_consulta = "{{ config('app.url_api').'formulario/asesores' }}";

            var request = $.ajax({
                url: url_consulta,
                type: "POST",                
                data: $("#form_asesores").serialize(),                
            });

            loadingAjax();

            request.done(function(result){  
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-success' role='alert'>" +
                                "se envio el mensaje con exito" +
                             "</div>";            

                $("#mensajes_form").append(alert);                

                $("#form_asesores")[0].reset();
            
                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);                
            });

            request.fail(function(jqXHR, textStatus){               
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-danger' role='alert'>" +
                                jqXHR.responseJSON.mensaje
                             "</div>";
                             
                $("#mensajes_form").append(alert);                

                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);
            });            

        });

        function loadingAjax(){
                var alert = "<div class='alert alert-info' role='alert'>" +
                                "Estamos processando su consulta" +
                             "</div>";

                $("#mensajes_form").append(alert);                
        }

    </script> 

@endsection