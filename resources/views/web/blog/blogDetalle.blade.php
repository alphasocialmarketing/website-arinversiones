@extends('index')

@section('estilos')
    <meta property="og:title" content="{{ $data['blog']->titulo }}" />
    <meta property="og:image" content="{{ asset($data['blog']->getImagen()) }}" />
    <meta property="og:url" content="{{ url('blog/'.$data['blog']->slug) }}" />
    <meta property="og:description" content="{{ $data['blog']->intro }}" />     
    <meta property="og:type" content="website" />
    
    <link href="{{ asset('css/shared/ayoshare.css') }}" rel="stylesheet">
@endsection

@section('content')


	<div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=369928560017341";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection

@section('script')    
    <!-- jQuery Awesome Sosmed Share Button -->
    <script src="{{ asset('js/shared/ayoshare.js') }}"></script>		
    <script>
        $(function() {
            $("#shared-escritorio").ayoshare({
                facebook : true,
                google : true,                
                twitter : true,
                linkedin : true,
                telegram : true,
                whatsapp : true,
                email : true,

                stumbleupon : false,                                
                reddit : false,
                vk : false,
                pocket : false,                
                digg : false,                            
                line : false,
                bbm : false,
                viber : false,
                pinterest : false,
                bufferapp : false
            });
        });
    </script>
@endsection     