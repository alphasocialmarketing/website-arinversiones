
    <div class="modal fade" id="modal-venta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 class="modal-title w-100 font-weight-bold">Formulario venta de propiedad</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div id="mensajes_modal" class="col-md-12"></div>
                    <form id="form_modal_venta">
                        <div class="md-form mb-6">            
                            <i class="fa fa-user prefix grey-text"></i>
                            <label>Nombre y Apellido</label>
                            <input id="nombre_modalVender" type="text" name="nombre" class="form-control">          
                        </div>

                        <div class="md-form mb-6">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <label>Email</label>
                            <input id="email_modalVender" type="email" name="email" class="form-control">          
                        </div>
                            <div class="md-form mb-6">
                            <i class="fa fa-mobile prefix grey-text"></i>
                            <label>Teléfono</label>
                            <input type="text" id="telefono_modalVender" name="telefono" class="form-control">          
                        </div>
                        <div class="md-form mb-6">
                            <i class="fa fa-university prefix grey-text"></i>
                            <label>Información sobre la propiedad</label>
                            <textarea type="text" id="informacion_modalVender" name="informacion" class="md-textarea form-control" rows="4"></textarea>          
                        </div>
                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button id="boton_modal_enviar" type="submit" class="btn btn-web no-margin-bottom btn-medium btn-round no-margin-top">
                        Enviar
                    </button>
                </div>
            </div>
        </div>
    </div>        
  