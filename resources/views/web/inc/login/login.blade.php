@if(empty(Auth()->user()))
  <div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="{{ url('login') }}" method="post" id="register-form">
          {!! csrf_field() !!}  
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="product-name-details text-uppercase font-weight-600 letter-spacing-2 black-text">
              Iniciar Sesión
            </h4>
          </div>
          <div class="modal-body">                      
            <div class="form-group no-margin-bottom">              
              <label for="email" class="text-uppercase">
                <b>Usuario</b>
              </label>         
              <input type="email" name="email" id="email" class="form-control">              
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group no-margin-bottom">              
              <label for="password" class="text-uppercase">
                <b>Contraseña</b>
              </label>
              <input type="password" name="password" id="password" class="form-control">              
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif            
            </div>               
            <div class="modal-footer">
              <div class="form-group no-margin-bottom">              
                <button class="highlight-button-dark btn slider-boton-color pull-right no-margin-right" type="submit" style="margin:0px;">
                  Iniciar
                </button>                      
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div> 
@else
  <div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">


          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="product-name-details text-uppercase font-weight-600 letter-spacing-2 black-text">
              Hola {{Auth()->user()->name}}
            </h4>
          </div>
          <div class="modal-body">                      
            @if(Auth()->user()->hasRole(['mayorista']))
              <p>Ya se encuentra logueado como MAYORISTA</p>       
            @else     
              @if(Auth()->user()->hasRole(['revendedor']))
                <p>Ya se encuentra logueado como REVENDEDOR</p>       
              @else
                <p>Ya se encuentra logueado como ADMINISTRADOR</p>       
              @endif
            @endif
          </div>
        </form>
      </div>
    </div>
  </div> 
@endif




