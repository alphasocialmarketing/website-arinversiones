
<div class="modal fade" id="recoveryForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{ url('recuperar') }}" method="post" id="register-form">
        {!! csrf_field() !!}  
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
            Recuperar Contraseña
          </h4>
        </div>
        <div class="modal-body">                      
          <div class="form-group no-margin-bottom">              
            <label for="email" class="text-uppercase">
              <b>Usuario</b>
            </label>         
            <input type="email" name="email" id="email" class="form-control">              
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>          
          <div class="form-group no-margin-bottom">              
            <button class="btn btn-default" type="submit">
              Recuperar
            </button>            
          </div>
        </div>
      </form>
    </div>
  </div>
</div> 



