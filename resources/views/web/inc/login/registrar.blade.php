
<div class="modal fade" id="registerForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{ url('registrar') }}" method="post" id="register-form">
        {!! csrf_field() !!}  
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
            Registrarse
          </h4>
        </div>
        <div class="modal-body">                      
          <div class="form-group no-margin-bottom">              
            <label for="email" class="text-uppercase">
              <b>Nombre y Apellido</b>
            </label>         
            <input type="text" name="name" required="" value="{{ old('name') }}" required placeholder="Nombre y Apellido" class="form-control">

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group no-margin-bottom">              
            <label for="email" class="text-uppercase">
              <b>Usuario</b>
            </label>         
            <input type="email" name="email" id="email" value="{{ old('email') }}" required placeholder="Email" class="form-control">              
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group no-margin-bottom">              
            <label for="password" class="text-uppercase">
              <b>Contraseña</b>
            </label>
            <input type="password" name="password" id="password" required placeholder="Contraseña" class="form-control">              
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif            
          </div>      

          <div class="form-group no-margin-bottom">              
            <label for="password" class="text-uppercase">
              <b>Confirmar Contraseña</b>
            </label>          
            <input type="password" name="password_confirmation" required placeholder="Confirmar" class="form-control">
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group no-margin-bottom">              
            <button class="btn btn-default" type="submit">
              Crear Cuenta
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div> 



