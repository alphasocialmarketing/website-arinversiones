	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
       
    <title>{{ \Config::get('settings.meta-title') }}</title>

    <meta name="description" content="{{ \Config::get('settings.meta-description') }}">
    <meta name="keywords" content="{{ \Config::get('settings.meta-keywords') }}">
    <meta name="author" content="{{ \Config::get('settings.meta-author') }}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('images/logos/isologo.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('images/logos/isologo.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/logos/isologo.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/logos/isologo.png') }}">