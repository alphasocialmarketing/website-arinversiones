<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-8 col-sm-12">
                &copy;  2019 AR Inversiones. Diseñado por <a href="http://www.alphasocialmarketing.com/" target="_blank">Alpha Social Marketing</a>. 
            </div>
            <div class="col-md-4 col-sm-12">
                <ul class="social-list clearfix">
                    <li>
                        <a href="{{ url('https://www.facebook.com/ARInversiones/') }}" class="facebook" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('https://twitter.com/AR_Inversiones') }}" class="twitter" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('https://www.instagram.com/arinversiones/') }}" class="instagram" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>                    
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Copy end right-->

