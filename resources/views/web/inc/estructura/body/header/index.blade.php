@if( !empty($data['slider']) )

    <!-- Banner start -->
    <div class="banner">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">


                @php
                    $cont = 0;
                @endphp

                
                    @foreach( $data['slider'] as $d )
                        @if($cont == 0)
                            <div class="item banner-max-height active">
                                <img src="{{ url( $d->fondo ) }}" alt="{{ $d->titulo }}">
                                <!--<div class="carousel-caption banner-slider-inner">
                                    <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;">
                                        <a href="{{ url('/propiedades') }}" class="link-principal">
                                            <h1 class="texto-slider-prop">PROPIEDADES</h1>                                            
                                        </a>
         
                                    </div>
                                    <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;">
                                        <a href="{{ url('/loteos') }}" class="link-principal">
                                            <h1 class="texto-slider-lot">LOTEOS</h1>
                                        </a>                                                                  
                                    </div> 
                                </div>-->
                            </div>
                        @else
                            <div class="item banner-max-height">
                                <img src="{{ url( $d->fondo ) }}" alt="{{ $d->titulo }}">
                                <!--<div class="carousel-caption banner-slider-inner">
                                    <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;">
                                        <a href="{{ url('/propiedades') }}" class="link-principal">
                                            <h1 class="texto-slider-prop">PROPIEDADES</h1>
                                        </a>
                 
                                        
                                    </div>
                                    <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;">
                                        <a href="{{ url('/loteos') }}" class="link-principal">
                                            <h1 class="texto-slider-lot">LOTEOS</h1>
                                        </a>                                                                
                                        
                                    </div>
                                </div>-->
                            </div>
                        @endif

                        @php
                            $cont = $cont + 1;
                        @endphp

                    @endforeach

            </div>

                    <!--<div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="carousel slide" data-ride="carousel">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                            <p class="titulo-prop"><a href="{{ url('/propiedades') }}" class="link-principal">PROPIEDADES</a></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p class="titulo-lot"><a href="{{ url('/loteos') }}" class="link-principal">LOTEOS</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->


            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="slider-mover-left" aria-hidden="true">
                    <i class="fa fa-angle-left"></i>
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="slider-mover-right" aria-hidden="true">
                    <i class="fa fa-angle-right"></i>
                </span>
                <span class="sr-only">Next</span>
            </a>       
                        
        </div>     
    </div>
    <!-- Banner end -->
@endif            