			<!--<div class="page_loader"></div>-->

			<!-- Top header start -->
			<header class="top-header hidden-xs" id="top">
			    <div class="container">
			        <div class="row">
			            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			                <div class="list-inline">
			                    <a href="tel:+543413692568"><i class="fa fa-phone"></i>0341-6792590</a>
			                    <a href="mailto:info@arinversiones.com.ar"><i class="fa fa-envelope"></i>info@arinversiones.com.ar</a>
			                </div>
			            </div>
			            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			                <ul class="top-social-media pull-right">
			                     <li>
			                        <a href="{{ url('https://www.facebook.com/ARInversiones/') }}" class="facebook" target="_blank">
			                            <i class="fa fa-facebook"></i>
			                        </a>
			                    </li>
			                    <li>
			                        <a href="{{ url('https://twitter.com/AR_Inversiones') }}" class="twitter" target="_blank">
			                            <i class="fa fa-twitter"></i>
			                        </a>
			                    </li>
			                    <li>
			                        <a href="{{ url('https://www.instagram.com/arinversiones/') }}" class="instagram" target="_blank">
			                            <i class="fa fa-instagram"></i>
			                        </a>
			                    </li>
			                </ul>
			            </div>
			        </div>
			    </div>
			</header>
			<!-- Top header end -->
			<!-- Main header start -->
			<header class="main-header">
			    <div class="container">
			        <nav class="navbar navbar-default">			            

				            @include('web.inc.estructura.body.nav.logo') 
				            @include('web.inc.estructura.body.nav.menu') 
	            
    				</nav>        
    			</div>
    		</header>