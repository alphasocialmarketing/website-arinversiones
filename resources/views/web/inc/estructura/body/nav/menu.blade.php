            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/') }}" >
                    <img src="{{ asset('img/logos/logo-transparente.png') }}" alt="logo-2" style="width: 130px;">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav visible-md visible-lg hidden-xs hidden-sm" style="float: right !important;">
                    <li>
                        <a href="{{ url('/') }}" style="text-transform: uppercase !important;">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/propiedades') }}" style="text-transform: uppercase !important;">
                            Propiedades
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('/loteos') }}" style="text-transform: uppercase !important;">
                            Loteos
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('/asesores') }}" style="text-transform: uppercase !important;">
                            Asesores
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('/#contacto') }}" style="text-transform: uppercase !important;"> 
                            Contacto
                        </a>                        
                    </li>
                    <li>
                        <a class="item-venta" href="" data-toggle="modal" data-target="#modal-venta" style="text-transform: uppercase !important;background: #802d39;color: #fff !important;"> 
                            ¿Querés Vender?
                        </a>                        
                    </li>                    
                    <!--<li class="text-right">
                        <a href="tel:1-8X0-666-8X88"><i class="fa fa-phone"></i>1-8X0-666-8X88</a>
                    </li>
                    <li class="text-right">
                        <a href="tel:info@themevessel.com"><i class="fa fa-envelope"></i>info@themevessel.com</a>
                    </li>-->
                </ul>
                <ul class="nav navbar-nav visible-xs visible-sm hidden-md hidden-lg">
                    <li>
                        <a href="{{ url('/') }}" style="text-transform: uppercase !important;">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/propiedades') }}" style="text-transform: uppercase !important;">
                            Propiedades
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('/loteos') }}" style="text-transform: uppercase !important;">
                            Loteos
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('/asesores') }}" style="text-transform: uppercase !important;">
                            Asesores
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('#contacto') }}" style="text-transform: uppercase !important;">
                            Contacto
                        </a>                        
                    </li>
                    <li>
                        <a class="item-venta" href="" data-toggle="modal" data-target="#modal-venta" style="text-transform: uppercase !important;background: #802d39;color: #fff !important;"> 
                            ¿Querés Vender?
                        </a>                        
                    </li>                     
                    <!--<li class="text-right">
                        <a href="tel:1-8X0-666-8X88"><i class="fa fa-phone"></i>1-8X0-666-8X88</a>
                    </li>
                    <li class="text-right">
                        <a href="tel:info@themevessel.com"><i class="fa fa-envelope"></i>info@themevessel.com</a>
                    </li>-->
                </ul>
            </div>