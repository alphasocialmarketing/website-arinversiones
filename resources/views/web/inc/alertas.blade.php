
  @if(Session::get('success'))
    <div id="alerta_id" class="pull-right" style="position: fixed; top: 125px; z-index: 99">
      <p style="color:green;font-size:20px;background:#f2f2f2;padding:15px;border-radius: 0px;">
        <b>
          <i class="fa fa-thumbs-o-up" style="color:green"></i>
          {{ Session::get('success') }}    
        </b>
      </p>
    </div>

    <script type="text/javascript">     
      $("#alerta_id").delay(8000).hide(600);
    </script>
  @endif

  @if(Session::get('errors'))
    <div id="alerta_id" class="pull-right" style="position: fixed; top: 125px; z-index: 99">
      @foreach($errors->all() as $error)      
          <p style="color:red;font-size:20px;background:#f2f2f2;padding:15px;border-radius: 0px;">
            <b>
              <i class="fa fa-thumbs-o-down" style="color:red"></i>
              {{ $error }}
            </b>
          </p>      
      @endforeach 
    </div>
    
    <script type="text/javascript">     
      $("#alerta_id").delay(8000).hide(600);
    </script>

  @endif




