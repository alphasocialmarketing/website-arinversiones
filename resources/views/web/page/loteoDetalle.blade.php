@extends('index')

@section('content')

@php
    if($data['loteo']->direccion){
        $lat = json_decode($data['loteo']->direccion)->latlng->lat;
        $lng = json_decode($data['loteo']->direccion)->latlng->lng;
    }
    else{
        $lat = $data['loteo']->lat;
        $lng = $data['loteo']->lng;
    }
@endphp

<!-- Sub banner start -->
<div class="fondo-edificio overview-bgi">
    <div class="overlay">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>{{ $data['loteo']->nombre }}</h1>
            </div>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Properties details page start -->
<div class="content-area  properties-details-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <!-- Header -->
                <div class="heading-properties clearfix sidebar-widget">
                    <div class="pull-left">
                        <h3>{{ $data['loteo']->nombre }}</h3>
                        <p>
                            @if($data['loteo']->direccion)
                                <i class="fa fa-map-marker"></i> {{ json_decode($data['loteo']->direccion)->value }}                            
                            @else
                                @if($data['loteo']->direccion_tmp))
                                    <i class="fa fa-map-marker"></i> {{ $data['loteo']->direccion_tmp }}                                                   
                                @else
                                    <i class="fa fa-map-marker"></i> SIN DIRECCION                            
                                @endif                                
                            @endif
                        </p>
                    </div>
                </div>

                <!-- Properties details section start -->
                <div class="Properties-details-section sidebar-widget">
                    <!-- Properties detail slider start -->
                    <div class="properties-detail-slider simple-slider mb-40">
                        <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                            <div class="carousel-outer">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    @php
                                        $cont = 0;
                                    @endphp


                                    @if( !empty($data['loteo']->getImagenesToJson() ))
                                        @foreach( $data['loteo']->getImagenesToJson() as $d )
                                            @if($cont == 0)
                                                <div class="item active">
                                                    <img src="{{ asset($d['imagen']) }}" class="thumb-preview" alt="{{ $data['loteo']->nombre }}">
                                                </div>
                                            @else
                                                <div class="item">
                                                    <img src="{{ asset($d['imagen']) }}" class="thumb-preview" alt="{{ $data['loteo']->nombre }}">
                                                </div>
                                            @endif
                                            
                                            @php
                                                $cont = $cont + 1;
                                            @endphp

                                        @endforeach
                                    @endif                                
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
                                    <span class="slider-mover-left no-bg t-slider-r pojison" aria-hidden="true">
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                                    <span class="sr-only">Anterior</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                    <span class="slider-mover-right no-bg t-slider-l pojison" aria-hidden="true">
                                        <i class="fa fa-angle-right"></i>
                                    </span>
                                    <span class="sr-only">Siguiente</span>
                                </a>
                            </div>

                            <!-- Indicators -->
                            <ol class="carousel-indicators thumbs visible-lg visible-md">
                                @php
                                    $cont = 0;
                                @endphp

                                @if( !empty($data['loteo']->getImagenesToJson() ))
                                    @foreach( $data['loteo']->getImagenesToJson() as $d )
                                        <li data-target="#carousel-custom" data-slide-to="{{ $cont }}" class="">
                                            <img src="{{ asset($d['imagen']) }}" alt="{{ $data['loteo']->nombre }}">
                                        </li>

                                        @php
                                            $cont = $cont + 1;
                                        @endphp
                                    @endforeach
                                @endif                                        
                            </ol>

                        </div>
                    </div>
                    <!-- Properties detail slider end -->             

                    <!-- Property description start -->
                    <div class="panel-box properties-panel-box Property-description">
                        
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in">
                                        <div class="main-title-2">
                                            <h1><span>Descripción</span></h1>
                                        </div>                                        
                                        <p style="padding-bottom: 30px !important;">{!! $data['loteo']->descripcion !!}</p>
                                        <div class="col-md-4 col-sm-4 col-xs-12" style="padding-bottom: 40px !important;">
                                            <ul class="condition">
                                                <li>
                                                    <i class="fa fa-square" style="color: green;font-size: 25px !important;"></i> <span style="font-size: 15px !important;"><b>{{ $data['lotes_disponibles'] }} Lotes Disponibles</b></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12" style="padding-bottom: 40px !important;">
                                            <ul class="condition">
                                                <li>
                                                    <i class="fa fa-square" style="color: yellow;font-size: 25px !important;"></i> <span style="font-size: 15px !important;"><b>{{ $data['lotes_reservados'] }} Lotes Reservados </b></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12" style="padding-bottom: 40px !important;">
                                            <ul class="condition">
                                                <li>
                                                    <i class="fa fa-square" style="color: red;font-size: 25px !important;"></i><span style="font-size: 15px !important;"><b> {{ $data['lotes_vendidos'] }} Lotes Vendidos </b></span>
                                                </li>
                                            </ul>
                                        </div>                                       
                                    </div>
                                    <br>
                                    <div class="tab-pane fade features active in">
                                        <!-- Properties condition start -->
                                        <div class="properties-condition">
                                            <div class="main-title-2">
                                                <h1><span>Caracteristicas</span></h1>
                                            </div>
                                            <div class="row">                                                
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['loteo']->cerrado ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i> Cerrado
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i> Abierto
                                                            </li>    
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['loteo']->posesion ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i> Posesión Inmediata
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Posesión Inmediata
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['loteo']->financiado ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i> Financiado
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i> Financiado
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                                                                                                   
                                            </div>
                                        </div>
                                        <!-- Properties condition end -->
                                    </div>                                                                                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Property description end -->
                </div>
                <!-- Properties details section end -->

                <!-- Location start -->
                <div class="location sidebar-widget">
                    <div class="map">
                        <!-- Main Title 2 -->
                        <div class="main-title-2">
                            <h1><span>Dirección</span></h1>
                        </div>                        
                        <div id="map" class="contact-map"></div>
                    </div>
                </div>
                <!-- Location end -->

                <div class="location sidebar-widget">
                    <div class="map">
                        <!-- Main Title 2 -->
                        <div class="main-title-2">
                            <h1><span>Plano de mensura</span></h1>
                        </div>
                        <div class="col-md-8 alert-style6">
                            <div class="alert alert-info" role="alert">
                                <i class="icon-lightbulb"></i>
                                <span><strong>Sugerencia!</strong> Al hacer click sobre los marcadores de colores obtendras información sobre el lote.</span>
                            </div>
                        </div>                        
                        @if( empty($data["loteo"]->plano_mensura) )
                            <div class="callout callout-warning">
                                <h4>
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Plano de Mensura</font>
                                    </font>
                                </h4>
                
                                <p>
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">El loteo no tiene cargado el plano de mensura.</font>
                                    </font>
                                </p>
                            </div>
                        @else                
                            <canvas id="canvas" width="800" height="600"> 
                                Your browser does not support the canvas element. 
                            </canvas>                             
                        @endif
                    </div>
                </div>


                <!-- Properties details section start -->
                <div class="Properties-details-section sidebar-widget">
                    

                    <!-- Contact 1 start -->
                    <div class="contact-1">
                        <div class="contact-form">
                            <!-- Main Title 2 -->
                            <div class="main-title-2">
                                <h1><span>Contacto</span></h1>
                            </div>
                            <div id="mensajes_form" class="col-md-12"></div>
                            <form id="form_loteos">
                                <input type="hidden" name="loteo_id" value="{{ $data['loteo']->id }}" />
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group fullname">
                                            <input type="text" name="nombre" class="input-text" placeholder="Nombre y apellido">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group enter-email">
                                            <input type="email" name="email" class="input-text"  placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group subject">
                                            <input type="text" name="asunto" class="input-text" placeholder="Asunto">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group number">
                                            <input type="text" name="telefono" class="input-text" placeholder="Número de teléfono">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group message">
                                            <textarea class="input-text" name="mensaje" placeholder="Mensaje"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group send-btn mb-0">
                                            <button id="boton_form_enviar" class="button-md button-theme">Enviar Mensaje</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Contact 1 end -->
                </div>
                <!-- Properties details section end -->
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <!-- Sidebar start -->
                <div class="sidebar right">                    

                    <div class="sidebar-widget popular-posts">
                        @if( !empty($data['ase_rel']) )
                            <div class="main-title-2">
                                <h1><span>Asesor</span> a cargo</h1>
                            </div>
                            @foreach($data['ase_rel'] as $d)
                                <div class="media">
                                    <div class="media-left">
                                        <a href="{{ url('asesores/'.$d->slug) }}">
                                            @if(!empty($d->imagen))
                                                <img class="media-object" src="{{ asset($d->imagen) }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                            @else
                                                <img class="media-object" src="{{ asset('images/no-disponible.png') }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">
                                            <a href="{{ url('asesores/'.$d->slug) }}">{{ $d->nombre }}</a>
                                        </h3>
                                        <p>
                                            {{ $d->puesto }}
                                        </p>
                                        <p>
                                            {{ $d->profesion }}
                                        </p>
                                        <p>
                                            {{ $d->celular }}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- Popular posts start -->                    
                    <div class="sidebar-widget popular-posts">
                        @if( !empty($data['relacionados']) )
                            <div class="main-title-2">
                                <h1>TE PUEDEN <span>INTERESAR</span></h1>
                            </div>
                            @foreach($data['relacionados'] as $d)
                                <div class="media">
                                    <div class="media-left">
                                        @if(!empty($d->principal))
                                            <img class="media-object" src="{{ asset($d->getImagen()) }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                        @else
                                            <img class="media-object" src="{{ asset('images/no-disponible.png') }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                        @endif
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">
                                            <a href="{{ url('loteos/'.$d->slug) }}">{{ $d->nombre }}</a>
                                        </h3>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- Popular posts start -->                    
                    
                    <!-- Social media start -->
                    <div class="social-media sidebar-widget clearfix">
                        <!-- Main Title 2 -->
                        <div class="main-title-2">
                            <h1>Redes <span>Sociales</span></h1>
                        </div>
                        <!-- Social list -->
                        <ul class="social-list">
                            <li><a href="{{ url('https://www.facebook.com/ARInversiones/') }}" class="facebook-bg" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{ url('https://twitter.com/AR_Inversiones') }}" class="twitter-bg" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{ url('https://www.instagram.com/arinversiones/') }}" class="instagram-bg" target="_blank"><i class="fa fa-instagram"></i></a></li>                            
                        </ul>
                    </div>
                    
                </div>
                <!-- Sidebar end -->
            </div>
        </div>

            <div id="modal_lotes" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header" style="border-bottom: 1px solid #802d39 !important;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="modal_titulo">Lote</h2>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">                            
                            <h4 class="text-left" style="color: #802d39;">
                                Detalle del Lote
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <p id="modal_detalle"></p>
                        </div>
                        <hr style="padding: 10px;border-bottom: 1px solid #802d39;border-top: 0px !important;">
                        <div class="col-md-12">                            
                            <h4 class="text-left" style="color: #802d39;margin-top:0px !important;margin-bottom:24px !important;">
                                Caracteristicas del Lote
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <ul class="address-list text-left">
                                    <li>
                                        <span style="float: none !important;">
                                            <i class="fa fa-money"></i><b style="color:#802d39">Precio:</b> <b>U$S</b>
                                        </span>
                                        <span id="modal_precio" style="float: none !important;font-weight: bold;"></span>
                                    </li>                                    
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="address-list">
                                    <li>
                                        <span style="float: none !important;">
                                            <i class="fa fa-money"></i><b style="color:#802d39">Estado: </b>
                                        </span>
                                        <span id="modal_estado" style="float: none !important;color: red;"></span>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-12">
                        <hr style="padding: 1px;border-bottom: 1px solid #802d39;border-top: 0px !important;">                             
                    </div>
                    <div class="modal-footer" style="border-top:0px !important;">
                        <div class="col-md-12">                            
                            <h4 class="text-left" style="color: #802d39;margin-top:0px !important;margin-bottom:24px !important;">
                                Caracteristicas del Asesor
                            </h4>
                        </div>
                        <div class="col-md-6">
                            <ul class="address-list text-left">
                                <li>
                                    <span style="float: none !important;">
                                        <i class="fa fa-user"></i><b style="color:#802d39">Asesor:</b>
                                    </span>
                                    <span id="modal_vendedor" style="float: none !important;"></span>
                                </li>
                                <li>
                                    <span style="float: none !important;">
                                        <i class="fa fa-university"></i><b style="color:#802d39">Profesión:</b>
                                    </span>
                                    <span id="modal_profesion" style="float: none !important;"></span>
                                </li>
                                <li>
                                    <span style="float: none !important;">
                                        <i class="fa fa-phone"></i><b style="color:#802d39">Teléfono:</b>
                                    </span>
                                    <span id="modal_telefono" style="float: none !important;"></span>
                                </li>
                                <li>
                                    <span style="float: none !important;">
                                        <i class="fa fa-envelope"></i><b style="color:#802d39">Email:</b>
                                    </span>
                                    <span id="modal_email" style="float: none !important;"></span>
                                </li>                                                                                                    
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <img id="modal_imagen_asesor" src="" class="img-responsive pull-right" style="width: 50% !important;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Properties details page end -->
@endsection


@section('script')

    <!-- FORMULARIO -->
    <script type="text/javascript">    
        $("#boton_form_enviar").click(function(event){

            event.preventDefault();
            
            var url_consulta = "{{ config('app.url_api').'formulario/loteos' }}";

            var request = $.ajax({
                url: url_consulta,
                type: "POST",                
                data: $("#form_loteos").serialize(),                
            });

            loadingAjax();

            request.done(function(result){  
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-success' role='alert'>" +
                                "se envio el mensaje con exito" +
                             "</div>";
                

                $("#mensajes_form").append(alert);                

                $("#form_loteos")[0].reset();
            
                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);                
            });

            request.fail(function(jqXHR, textStatus){               
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-danger' role='alert'>" +
                                jqXHR.responseJSON.mensaje
                             "</div>";
                             
                $("#mensajes_form").append(alert);                

                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);
            });            

        });

        function loadingAjax(){
                var alert = "<div class='alert alert-info' role='alert'>" +
                                "Estamos processando su consulta" +
                             "</div>";

                $("#mensajes_form").append(alert);                
        }

    </script>
    <script>
        var latitude = "{{ $lat }}";
        var longitude = "{{ $lng }}";
        var providerName = 'OpenStreetMap';
        var zoom = 14;
        var scroll = false;        
        
        
        var url_api = "{{ config('app.url_api') }}" + 'loteos/' + "{{ $data['loteo']->id }}";

        generateMap(latitude, longitude, providerName, null, "loteos", url_api, zoom, scroll);
    </script>

    <!-- CANVAS -->
    <script type="text/javascript"> 
        var canvas = document.getElementById("canvas");        
        var ctx = canvas.getContext("2d");

        var items = [];
        
        $(document).ready(function() {            
            var img = new Image();
            img.src = "{{ asset('storage/'.$data['loteo']->plano_mensura) }}";

            img.onload = function(){
                ctx.drawImage(img, 0, 0);
                actualizarMapa();
            }                    
        });

        canvas.addEventListener('mousemove', (e) => {           
            var offset =  $('#canvas').offset();          
            var x = e.pageX - offset.left
            var y = e.pageY - offset.top        
            
            var status = false
            items.forEach(function(element) {
                var desde_x = parseFloat(element.ref_x) - 10;
                var desde_y = parseFloat(element.ref_y) - 10;
                
                if(x >= desde_x && x <= element.ref_x &&
                   y >= desde_y && y <= element.ref_y){                                
                    status = true
                }
        
            })            

            if(status)
                canvas.style.cursor = "pointer"
            else
                canvas.style.cursor = "default"
        });  

        canvas.addEventListener('click', (e) => {
            var offset =  $('#canvas').offset();          
            var x = e.pageX - offset.left            
            var y = e.pageY - offset.top        
            
            var status = false
            var point = null
            
            items.forEach(function(element) {
                var desde_x = parseFloat(element.ref_x) - 10;
                var desde_y = parseFloat(element.ref_y) - 10;
                
                if(x >= desde_x && x <= element.ref_x &&
                   y >= desde_y && y <= element.ref_y){                                
                    status = true
                    point = element
                }
            })   

            if(status && point)                
                getInfo(x, y, point)
        });  

        /*
        canvas.addEventListener('mousemove', (e) => {
            var offset =  $('#canvas').offset();          
            var x = e.pageX - offset.left            
            var y = e.pageY - offset.top        
            
            var status = false
            var point = null
            
            items.forEach(function(element) {
                var desde_x = parseFloat(element.ref_x) - 10;
                var desde_y = parseFloat(element.ref_y) - 10;
                
                if(x >= desde_x && x <= element.ref_x &&
                   y >= desde_y && y <= element.ref_y){                                
                    status = true
                    point = element
                }
            })   

            if(status && point)                
                getInfo(x, y, point)
        });  
        */

        function dibujarLotes(val){  
            ctx.beginPath();
            
            var x = val.ref_x 
            var y = val.ref_y
            var estado = val.estado
            var id = val.id

            switch(estado){
                case "DISPONIBLE":
                    ctx.fillStyle = "#008000";          
                    break

                case "RESERVADO":
                    ctx.fillStyle = "#ffc807";          
                    break

                case "VENDIDO":
                    ctx.fillStyle = "#f44336";          
                    break
            }
            
            ctx.arc(x, y, 10, 0, 2 * Math.PI, false);            
            ctx.fill();
            ctx.fillRect(x,y,10,10);
            
            items.push(val)
        }
    </script> 

    <script>       

        function actualizarMapa(){            
            var url = "{{ config('app.url_api').'plano-mensura/obtener' }}";        
            
            var request = $.ajax({
                url: url,
                data: JSON.stringify({
                    id: "{{ $data['loteo']->id }}",                    
                }),
                type: "POST",
                contentType: "application/json; charset=utf-8",
            });

            request.done(function(result){     
                items = []                               
                jQuery.each(result, function(i, val) {                    
                    dibujarLotes(val);
                });       
            })
            .fail(function(error){      
                alert("ERROR: " + error.responseText);
            });              
        }

        function getInfo(x, y, data){                            
            $('#modal_lotes').modal({
                show: true
            });      
            
            $("#modal_titulo").text(data.nombre)
            $("#modal_detalle").text(data.detalle)
            $("#modal_precio").text(data.precio)
            $("#modal_estado").text(data.estado)
            $("#modal_vendedor").text(data.asesor.nombre)
            $("#modal_telefono").text(data.asesor.celular)
            $("#modal_email").text(data.asesor.email)
            $("#modal_profesion").text(data.asesor.profesion)
            $("#modal_imagen_asesor").attr("src","{{ asset('/') }}" + data.asesor.imagen)
        }

    </script>  
@endsection