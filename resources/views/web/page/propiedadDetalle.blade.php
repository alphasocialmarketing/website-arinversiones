@extends('index')

@section('metas')   
    <meta name="description" content="{{ $data['propiedad']->titulo }}">
    <meta name="keywords" content="propiedades, loteos, casas, departamentos, negocios, inmobiliaria, ar, negocios inmobiliarios, asesores, venta, alquiler, financiacion">

    <meta name="google-site-verification" content="AZ9geZ_fMAJGzjLDO08MXgMTNbWPQ95CMavnWYYb9Zg" />
@endsection

@section('estilos')
    <meta property="og:title" content="{{ $data['propiedad']->titulo }}" />
    <meta property="og:image" content="{{ asset($data['propiedad']->principal) }}" />
    <meta property="og:url" content="{{ url('propiedades/'.$data['propiedad']->slug) }}" />
    <meta property="og:description" content="{{ $data['propiedad']->intro }}" />     
    <meta property="og:type" content="website" />
    
    <link href="{{ asset('css/shared/ayoshare.css') }}" rel="stylesheet">
@endsection


@section('content')

@php
    if($data['propiedad']->direccion){
        $lat = json_decode($data['propiedad']->direccion)->latlng->lat;
        $lng = json_decode($data['propiedad']->direccion)->latlng->lng;
    }
    else{
        $lat = $data['propiedad']->lat;
        $lng = $data['propiedad']->lng;
    }
@endphp

<!-- Sub banner start -->
<div class="fondo-edificio overview-bgi">
    <div class="overlay">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>{{ $data['propiedad']->nombre }}</h1>
            </div>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Properties details page start -->
<div class="content-area  properties-details-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <!-- Header -->
                <div class="heading-properties clearfix sidebar-widget">
                    <div class="pull-left">
                        <h3>{{ $data['propiedad']->nombre }}</h3>
                        <p>
                            @if($data['propiedad']->direccion)
                                <i class="fa fa-map-marker"></i> {{ json_decode($data['propiedad']->direccion)->value }}                            
                            @else
                                @if($data['propiedad']->direccion_tmp))
                                    <i class="fa fa-map-marker"></i> {{ $data['propiedad']->direccion_tmp }}                                                   
                                @else
                                    <i class="fa fa-map-marker"></i> SIN DIRECCION                            
                                @endif                                
                            @endif
                        </p>
                    </div>
                    <div class="pull-right">

                        @if($data['propiedad']->moneda == "PESO" && $data['propiedad']->ocultar_precio == "false")
                            
                            <h3><span>AR$ {{ $data['propiedad']->precio }}</span></h3>
                        @endif
                        @if($data['propiedad']->moneda == "DOLAR" && $data['propiedad']->ocultar_precio == "false")
                            <h3><span>US$ {{ $data['propiedad']->precio }}</span></h3>
                        @endif                        
                    </div>
                </div>

                <!-- Properties details section start -->
                <div class="Properties-details-section sidebar-widget">
                    <!-- Properties detail slider start -->
                    <div class="properties-detail-slider simple-slider mb-40">
                        <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                            <div class="carousel-outer">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    @php
                                        $cont = 0;
                                    @endphp


                                    @if( !empty($data['propiedad']->getImagenesToJson() ))
                                        @foreach( $data['propiedad']->getImagenesToJson() as $d )
                                            @if($cont == 0)
                                                <div class="item active">
                                                    <img src="{{ asset($d['imagen']) }}" class="thumb-preview" alt="{{ $data['propiedad']->nombre }}">
                                                </div>
                                            @else
                                                <div class="item">
                                                    <img src="{{ asset($d['imagen']) }}" class="thumb-preview" alt="{{ $data['propiedad']->nombre }}">
                                                </div>
                                            @endif
                                            
                                            @php
                                                $cont = $cont + 1;
                                            @endphp

                                        @endforeach
                                    @endif                                
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
                                    <span class="slider-mover-left no-bg t-slider-r pojison" aria-hidden="true">
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                                    <span class="sr-only">Anterior</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                    <span class="slider-mover-right no-bg t-slider-l pojison" aria-hidden="true">
                                        <i class="fa fa-angle-right"></i>
                                    </span>
                                    <span class="sr-only">Siguiente</span>
                                </a>
                            </div>

                            <!-- Indicators -->
                            <ol class="carousel-indicators thumbs visible-lg visible-md">
                                @php
                                    $cont = 0;
                                @endphp

                                @if( !empty($data['propiedad']->getImagenesToJson() ))
                                    @foreach( $data['propiedad']->getImagenesToJson() as $d )
                                        <li data-target="#carousel-custom" data-slide-to="{{ $cont }}" class="">
                                            <img src="{{ asset($d['imagen']) }}" alt="{{ $data['propiedad']->nombre }}">
                                        </li>

                                        @php
                                            $cont = $cont + 1;
                                        @endphp
                                    @endforeach
                                @endif                                        
                            </ol>

                        </div>
                    </div>
                    <!-- Properties detail slider end -->
                    <!-- Property description start -->
                    <div class="panel-box properties-panel-box Property-description">
                        
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in">
                                        <div class="main-title-2">
                                            <h1><span>Descripción</span></h1>
                                        </div>
                                        <p>{!! $data['propiedad']->descripcion !!}</p>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="{{ url('imprimir/'.$data['propiedad']->slug) }}" target="_blank" class="button-sm button-theme">Imprimir</a>
                                            </div>
                                            <div class="col-md-12" style="margin-top: 35px; padding-left: 8px;">
                                                <div id="shared-escritorio" data-ayoshare="{{ url('propiedades/'.$data['propiedad']->slug) }}" style="margin-top: -20px;"></div>                                               
                                            </div>
                                        </div>                                        
                                    </div>
                                    <br>
                                    <div class="tab-pane fade features active in margin-five">
                                        <!-- Properties condition start -->
                                        <div class="properties-condition">
                                            <div class="main-title-2">
                                                <h1><span>Caracteristicas</span></h1>
                                            </div>
                                            <div class="row">                                                
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['propiedad']->habitaciones ))
                                                            <li>
                                                                <i class="fa fa-bed"></i>{{ $data['propiedad']->habitaciones }} Habitaciones
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-bed"></i>Sin Habitaciones
                                                            </li>    
                                                        @endif
                                                        @if( !empty($data['propiedad']->cochera ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i>Cochera 
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Cochera 
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['propiedad']->banios ))
                                                            <li>
                                                                <i class="fa fa-bath"></i>{{ $data['propiedad']->banios }}  Baños
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-bath"></i>Sin Baños
                                                            </li>
                                                        @endif
                                                        
                                                        @if( !empty($data['propiedad']->terminada ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i>Terminada
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Terminada
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['propiedad']->medidas ))
                                                            <li>
                                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>{{ $data['propiedad']->medidas }} M2
                                                            </li>
                                                        @else
                                                        <li>
                                                            <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>Medidas no definidas
                                                        </li>
                                                        @endif
                                                        @if( !empty($data['propiedad']->gas ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i>Gas
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Gas
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Properties condition end -->
                                    </div>                                                                                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Property description end -->
                </div>
                <!-- Properties details section end -->

                <!-- Location start -->
                <div class="location sidebar-widget">
                    <div class="map">
                        <!-- Main Title 2 -->
                        <div class="main-title-2">
                            <h1><span>Dirección</span></h1>
                        </div>                        
                        <div id="map" class="contact-map"></div>
                    </div>
                </div>
                <!-- Location end -->

                <!-- Properties details section start -->
                <div class="Properties-details-section sidebar-widget">
                    

                    <!-- Contact 1 start -->
                    <div class="contact-1">
                        <div class="contact-form">
                            <!-- Main Title 2 -->
                            <div class="main-title-2">
                                <h1><span>Contacto</span></h1>
                            </div>
                            <div id="mensajes_form" class="col-md-12"></div>
                            <form id="form_propiedades">
                                <input type="hidden" name="propiedad_id" value="{{ $data['propiedad']->id }}" />
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group fullname">
                                            <input type="text" name="nombre" class="input-text" placeholder="Nombre y apellido">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group enter-email">
                                            <input type="email" name="email" class="input-text"  placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group subject">
                                            <input type="text" name="asunto" class="input-text" placeholder="Asunto">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group number">
                                            <input type="text" name="telefono" class="input-text" placeholder="Número de teléfono">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group message">
                                            <textarea class="input-text" name="mensaje" placeholder="Mensaje"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group send-btn mb-0">
                                            <button id="boton_form_enviar" class="button-md button-theme">Enviar Mensaje</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Contact 1 end -->
                </div>
                <!-- Properties details section end -->
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <!-- Sidebar start -->
                <div class="sidebar right">                    

                    <div class="sidebar-widget popular-posts">
                        @if( !empty($data['ase_rel']) )
                            <div class="main-title-2">
                                <h1><span>Asesor</span> a cargo</h1>
                            </div>
                            @foreach($data['ase_rel'] as $d)
                                <div class="media">
                                    <div class="media-left">
                                        <a href="{{ url('asesores/'.$d->slug) }}">
                                            @if(!empty($d->imagen))
                                                <img class="media-object" src="{{ asset($d->imagen) }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                            @else
                                                <img class="media-object" src="{{ asset('images/no-disponible.png') }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">
                                            <a href="{{ url('asesores/'.$d->slug) }}">{{ $d->nombre }}</a>
                                        </h3>
                                        <p>
                                            {{ $d->puesto }}
                                        </p>
                                        <p>
                                            {{ $d->profesion }}
                                        </p>
                                        <p>
                                            {{ $d->celular }}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- Popular posts start -->                    
                    <div class="sidebar-widget popular-posts">
                        @if( !empty($data['relacionados']) )
                            <div class="main-title-2">
                                <h1>TE PUEDEN <span>INTERESAR</span></h1>
                            </div>
                            @foreach($data['relacionados'] as $d)
                                <div class="media">
                                    <div class="media-left">
                                        @if(!empty($d->principal))
                                            <img class="media-object" src="{{ asset($d->getImagen()) }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                        @else
                                            <img class="media-object" src="{{ asset('images/no-disponible.png') }}" alt="{{ $d->nombre }}" style="width: 100px;">
                                        @endif
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading">
                                            <a href="{{ url('propiedades/'.$d->slug) }}">{{ $d->nombre }}</a>
                                        </h3>
                                        <div class="price">
                                            @if($d->moneda == "PESO" && $d->ocultar_precio == "false")
                            
                                                AR$ {{ $d->precio }}
                                            @else
                                                AR$
                                            @endif
                                            @if($d->moneda == "DOLAR" && $d->ocultar_precio == "false")
                                                US$ {{ $d->precio }}
                                            @endif 

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- Popular posts start -->                    
                    
                    <!-- Social media start -->
                    <div class="social-media sidebar-widget clearfix">
                        <!-- Main Title 2 -->
                        <div class="main-title-2">
                            <h1>Redes <span>Sociales</span></h1>
                        </div>
                        <!-- Social list -->
                        <ul class="social-list">
                            <li><a href="{{ url('https://www.facebook.com/ARInversiones/') }}" class="facebook-bg" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{ url('https://twitter.com/AR_Inversiones') }}" class="twitter-bg" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{ url('https://www.instagram.com/arinversiones/') }}" class="instagram-bg" target="_blank"><i class="fa fa-instagram"></i></a></li>                            
                        </ul>
                    </div>
                    
                </div>
                <!-- Sidebar end -->
            </div>
        </div>
    </div>
</div>
<!-- Properties details page end -->
@endsection


@section('script')

    <!-- FORMULARIO  -->
    <script type="text/javascript">    
        $("#boton_form_enviar").click(function(event){

            event.preventDefault();
            
            var url_consulta = "{{ config('app.url_api').'formulario/propiedades' }}";

            var request = $.ajax({
                url: url_consulta,
                type: "POST",                
                data: $("#form_propiedades").serialize(),                
            });

            loadingAjax();

            request.done(function(result){  
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-success' role='alert'>" +
                                "se envio el mensaje con exito" +
                             "</div>";
                

                $("#mensajes_form").append(alert);                

                $("#form_propiedades")[0].reset();
            
                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);                
            });

            request.fail(function(jqXHR, textStatus){               
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-danger' role='alert'>" +
                                jqXHR.responseJSON.mensaje
                             "</div>";
                             
                $("#mensajes_form").append(alert);                

                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);
            });            

        });

        function loadingAjax(){
                var alert = "<div class='alert alert-info' role='alert'>" +
                                "Estamos processando su consulta" +
                             "</div>";

                $("#mensajes_form").append(alert);                
        }

    </script>

    <script>
        var latitude = "{{ $lat }}";
        var longitude = "{{ $lng }}";
        var providerName = 'OpenStreetMap';
        var zoom = 14;
        var scroll = false;
        
        var url_api = "{{ config('app.url_api') }}" + 'propiedades/' + "{{ $data['propiedad']->id }}";

        generateMap(latitude, longitude, providerName, null, "propiedades", url_api, zoom, scroll);
    </script>

    <!-- jQuery Awesome Sosmed Share Button -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/shared/ayoshare.css') }}">
    <script src="{{ asset('js/shared/ayoshare.js') }}"></script>        
    <script>
        $(function() {
            $("#shared-escritorio").ayoshare({
                facebook : true,
                google : false,                
                twitter : true,                
                telegram : false,
                whatsapp : true,
                email : true,

                linkedin : false,
                stumbleupon : false,                                
                reddit : false,
                vk : false,
                pocket : false,                
                digg : false,                            
                line : false,
                bbm : false,
                viber : false,
                pinterest : false,
                bufferapp : false
            });
        });

        $(function() {
            $("#shared-mobil").ayoshare({
                facebook : true,
                google : false,                
                twitter : true,                
                telegram : false,
                whatsapp : true,
                email : true,

                linkedin : false,
                stumbleupon : false,                                
                reddit : false,
                vk : false,
                pocket : false,                
                digg : false,                            
                line : false,
                bbm : false,
                viber : false,
                pinterest : false,
                bufferapp : false
            });
        });        
    </script>
@endsection