@extends('index')

@section('content')

@section('estilos')

    <style type="text/css">
        body{
    line-height:18px;
    width:100%;
}

 a[href]:after {
    content: none !important;
}

.soporte_content h1, .sop_listado h1{
    margin-top: -20px!important;
    color: #8a8c8f!important;
}

.content_desc{
    margin-top: 10px!important;
    padding: 0!important;
}

.content_content{
    margin-top: 0px!important;
    padding: 0!important;
    color: #8a8c8f!important;
}

.contenido-general, .contenido-amenidades, .contenido-ubicacion, .contenido-unidades, .contenido-imagenes{
    margin-top: 50px!important;
    color: #8a8c8f!important;
}

.titulos_plantas01{
    color: #007c7c!important;
    padding-top: 50px!important;
}

.titulos_plantas02{
    color: #c4c4c4!important;
    padding-top: 50px!important;
}

.tabla_plantas_01_verde{
    color: #007c7c!important;
}

.tabla_plantas_01_verde strong{
    color: #007c7c!important;
    font-weight: 700!important;
}

.tabla_plantas_01, .tabla_plantas_01_ultima{
      color: #464646!important;
}

.tabla_plantas_leyenda{
      color: #5b5b5f!important;
}

.tabla_plantas_link{
    color: #007c7c!important;
}

.logo_print{
    margin:0 0 30px 0 !important;
    padding:0 0 1px 0 !important
}

.list_port_mod_foto, .list_port_mod_text img{
    margin: 0 auto;
    display: table;
    margin-top: 20px;
}

#mapDiv{
    width: 350px!important;
    overflow: hidden;
}

.footer_print{
    font-size: 12px;
    line-height: 18px;
    font-weight: 400;
    border-top: 1px solid #ccc;
    padding: 15px 0 0 0;
}

    </style>

@endsection

@php
    if($data['propiedad']->direccion){
        $lat = json_decode($data['propiedad']->direccion)->latlng->lat;
        $lng = json_decode($data['propiedad']->direccion)->latlng->lng;
    }
    else{
        $lat = $data['propiedad']->lat;
        $lng = $data['propiedad']->lng;
    }
@endphp

<!-- Properties details page start -->
<div class="content-area  properties-details-page" style="background-color: #eee;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Header -->
                <div class="heading-properties clearfix sidebar-widget">
                    <div class="col-md-6">
                        <div class="pull-left">
                            <h3>
                               {{ $data['propiedad']->nombre }}
                            </h3>
                            <h4>
                               @if($data['propiedad']->direccion)
                                <i class="fa fa-map-marker"></i> {{ json_decode($data['propiedad']->direccion)->value }}                            
                            @else
                                @if($data['propiedad']->direccion_tmp))
                                    <i class="fa fa-map-marker"></i> {{ $data['propiedad']->direccion_tmp }}                                                   
                                @else
                                    <i class="fa fa-map-marker"></i> SIN DIRECCION                            
                                @endif                                
                            @endif 
                            </h4>
                            <h4><span>US$ {{ $data['propiedad']->precio }}</span></h4>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <ul class="condition visible-lg visible-md hidden-xs hidden-sm">
                                <li><a onclick="window.print()" value="Print" href="" target="_blank" class="button-md button-theme">Imprimir</a></li>
                                <!--<li><a href="" style="background: #fff;color:#000 !important; padding: 12px;">Volver</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                

                <!-- Properties details section start -->
                <div class="Properties-details-section sidebar-widget">                                           
                    <img src="{{ asset($data['propiedad']->principal) }}" class="img-responsive col-md-offset-3" style="width: 50% !important;">
                    <!-- Property description start -->
                    <div class="panel-box properties-panel-box Property-description">
                        
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in">
                                        <div class="main-title-2">
                                            <h1><span>Descripción</span></h1>
                                        </div>
                                        <p>{!! $data['propiedad']->descripcion !!}</p>                                        
                                    </div>
                                    <br>
                                    <div class="tab-pane fade features active in">
                                        <!-- Properties condition start -->
                                        <div class="properties-condition">
                                            <div class="main-title-2">
                                                <h1><span>Caracteristicas</span></h1>
                                            </div>
                                            <div class="row">                                                
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['propiedad']->habitaciones ))
                                                            <li>
                                                                <i class="fa fa-bed"></i>{{ $data['propiedad']->habitaciones }} Habitaciones
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-bed"></i>Sin Habitaciones
                                                            </li>    
                                                        @endif
                                                        @if( !empty($data['propiedad']->cochera ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i>Cochera 
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Cochera 
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['propiedad']->banios ))
                                                            <li>
                                                                <i class="fa fa-bath"></i>{{ $data['propiedad']->banios }}  Baños
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-bath"></i>Sin Baños
                                                            </li>
                                                        @endif
                                                        
                                                        @if( !empty($data['propiedad']->terminada ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i>Terminada
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Terminada
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <ul class="condition">
                                                        @if( !empty($data['propiedad']->medidas ))
                                                            <li>
                                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>{{ $data['propiedad']->medidas }} M2
                                                            </li>
                                                        @else
                                                        <li>
                                                            <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>Medidas no definidas
                                                        </li>
                                                        @endif
                                                        @if( !empty($data['propiedad']->gas ))
                                                            <li>
                                                                <i class="fa fa-check-square-o"></i>Gas
                                                            </li>
                                                        @else
                                                            <li>
                                                                <i class="fa fa-times"></i>Gas
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Properties condition end -->
                                    </div>                                                                                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Property description end -->
                </div>
                <!-- Properties details section end -->       
            </div>            
        </div>
    </div>
</div>
<!-- Properties details page end -->
@endsection


@section('script')


    <script>
      
        window.print()
    </script>

    
@endsection