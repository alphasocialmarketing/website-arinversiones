@extends('index')

@section('content')
	<!-- Map content start -->
<div class="map-content content-area container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-5 col-md-push-7 col-lg-6 col-lg-push-6">
        <div class="row">
            <div id="map"></div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-md-pull-5 col-lg-6 col-lg-pull-6 map-content-sidebar">
        <div class="title-area">
            <h2 class="pull-left">Buscar</h2>
            <a class="pull-right" onclick="mostrarFiltros()" data-toggle="collapse" data-target="#options-content">
                <p id="boton_filtro">
                    <i class="fa fa-filter"></i> Ocultar Filtros
                </p>
            </a>
                        
            <div class="clearfix"></div>
        </div>
        <div class="properties-map-search">
            <div class="properties-map-search-content">
            <div id="filtros">
                <div id="options-content" class="row">                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            @if(!empty($data['localidades']))
                                <select class="selectpicker search-fields" id="slt_localidad">
                                    <option value="0">Localidad</option>
                                    @foreach($data['localidades'] as $b)
                                        <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>                   
                                                                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            @if(!empty($data['asesores']))
                                <select class="selectpicker search-fields" id="slt_asesor">
                                    <option value="0">Asesor</option>
                                    @foreach($data['asesores'] as $b)
                                        <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">          
                        <div class="wide-separator-line-filtros no-margin-lr"></div>                              
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="checkbox checkbox-theme checkbox-circle">
                            <input id="checkbox_cerrado" type="checkbox" style="display:none;">
                            <label for="checkbox_cerrado">
                                Cerrado
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="checkbox checkbox-theme checkbox-circle">
                            <input id="checkbox_posesion" type="checkbox" style="display:none;">
                            <label for="checkbox_posesion">
                                Posesión
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="checkbox checkbox-theme checkbox-circle">
                            <input id="checkbox_financiado" type="checkbox" style="display:none;">
                            <label for="checkbox_financiado">
                                Financiado
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">          
                        <div class="wide-separator-line-filtros no-margin-lr"></div>                              
                    </div>                    
                    <div class="container-fluid">
                        <div class="form-group send-btn mb-0">
                            <button id="buscador" class="button-md button-theme pull-right" onclick="filtrarLoteos()">Buscar</button>
                        </div>
                    </div>
                </div>
            </div>

        <div class="map-content-separater"></div>
        <div class="clearfix"></div>

        <div class="title-area">
            <h2 class="pull-left">Loteos</h2>
            <!--<div class="pull-right btns-area">
                <a href="properties-map-rightside-list.html" class="change-view-btn active-view-btn"><i class="fa fa-th-list"></i></a>
                <a href="properties-map-rightside-grid.html" class="change-view-btn"><i class="fa fa-th-large"></i></a>
            </div>-->
            <div class="clearfix"></div>
        </div>
        <div class="fetching-properties"></div>
    </div>
</div>
<!-- Map content end -->
@endsection

@section('script')
    <script>
        function filtrarLoteos(){        
            
            var url = "{{ config('app.url_api').'buscador/loteos/filtrar' }}";            

            var request = $.ajax({
                url: url,
                data: JSON.stringify({
                    localidad: $("#slt_localidad option:selected").val(),
                    asesor: $("#slt_asesor option:selected").val(),
                    zona: $("#slt_zona option:selected").val(),
                    cerrado: $("#checkbox_cerrado").prop('checked'),
                    posesion: $("#checkbox_posesion").prop('checked'),
                    financiado: $("#checkbox_financiado").prop('checked'),
                    
                }),
                type: "POST",
                contentType: "application/json; charset=utf-8",
            });
            
            request.done(function(result){      
                var latitude = -32.9552966;
                var longitude = -60.6719805;
                var providerName = 'OpenStreetMap';
                var zoom = 9;
                var scroll = true;
                var data = result.data;
                
                console.log(data);

                actualizarMap(latitude, longitude, providerName, null, "loteos", data, zoom, scroll);             
            })
            .fail(function(error){      
                alert(error)
            });              
        }
    </script>    

    <script>
        var status_filtro = true;
        function mostrarFiltros(){            
            if(status_filtro){
                $("#filtros").show()    
                $("#boton_filtro").empty()                
                $("#boton_filtro").append("<i class='fa fa-filter'></i> Ocultar Filtros")                
                status_filtro = false;
            }
            
            else{
                $("#filtros").hide()
                $("#boton_filtro").empty()
                $("#boton_filtro").append("<i class='fa fa-filter'></i> Mostrar Filtros")                
                status_filtro = true;  
            }
        }
    </script>

    <script>
        var latitude = -32.9552966;
        var longitude = -60.6719805;
        var providerName = 'OpenStreetMap';
        var zoom = 9;
        var scroll = true;
        var url_api = "{{ config('app.url_api') }}" + 'loteos';

        generateMap(latitude, longitude, providerName, null, "loteos", url_api, zoom, scroll);
    </script>    
@endsection