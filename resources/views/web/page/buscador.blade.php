@extends('index')

@section('content')
	<!-- Map content start -->
<div class="map-content content-area container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-5 col-md-push-7 col-lg-6 col-lg-push-6">
        <div class="row">
            <div id="map"></div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-md-pull-5 col-lg-6 col-lg-pull-6 map-content-sidebar">
        <div class="title-area">
            <h2 class="pull-left">Buscar</h2>
            <a class="pull-right" onclick="mostrarFiltros()" data-toggle="collapse" data-target="#options-content">
                <p id="boton_filtro">
                    <i class="fa fa-filter"></i> Ocultar Filtros
                </p>
            </a>
                        
            <div class="clearfix"></div>
        </div>
        <div class="properties-map-search">
            <div class="properties-map-search-content">
            <div id="filtros">
                <div id="options-content" class="row">                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            @if(!empty($data['localidades']))
                                <select class="selectpicker search-fields" id="slt_localidad">
                                    <option value="0">Localidad</option>
                                    @foreach($data['localidades'] as $b)
                                        <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>                   
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            @if(!empty($data['barrios']))
                                <select class="selectpicker search-fields" id="slt_barrio">
                                    <option value="0">Barrio</option>
                                    @foreach($data['barrios'] as $b)
                                        <option value="{{ $b->id }}">{{ $b->nombre }}</option>                                    
                                    @endforeach
                                </select>
                            @endif                        
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                @if(!empty($data['asesores']))
                                    <select class="selectpicker search-fields" id="slt_asesor">
                                        <option value="0">Asesor</option>
                                        @foreach($data['asesores'] as $b)
                                            <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <select class="selectpicker search-fields" id="slt_habitaciones">
                                <option value="habitaciones">Habitaciones</option>
                                <option value="0">0</option>
                                <option value="1">1</option>                                
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>                                
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <select class="selectpicker search-fields" id="slt_banios" data-live-search="true" data-live-search-placeholder="Buscador">
                                <option value="0">Baños</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">          
                        <div class="wide-separator-line-filtros no-margin-lr"></div>                              
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="checkbox checkbox-theme checkbox-circle">
                            <input id="checkbox_cochera" type="checkbox" style="display:none;">
                            <label for="checkbox_cochera">
                                Cochera
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="checkbox checkbox-theme checkbox-circle">
                            <input id="checkbox_terminado" type="checkbox" style="display:none;"> 
                            <label for="checkbox_terminado">
                                Terminado
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="checkbox checkbox-theme checkbox-circle">
                            <input id="checkbox_gas" type="checkbox" style="display:none;">
                            <label for="checkbox_gas">
                                Gas
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">          
                        <div class="wide-separator-line-filtros no-margin-lr"></div>                              
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="range-slider" id="rng_area">
                            <label>Area</label>
                            <div id="amount" data-min="0" data-max="1000" data-unit="M²" data-min-name="min_area" data-max-name="max_area" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="range-slider" id="rng_precio">
                            <label>Precio</label>
                            <div id="amount" data-min="0" data-max="2000000" data-unit="AR$ / U$S" data-min-name="min_price" data-max-name="max_price" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="form-group send-btn mb-0">
                            <button id="buscador" class="button-md button-theme pull-right" onclick="filtrar()">Buscar</button>
                        </div>
                    </div>
                </div>
            </div>

        <div class="map-content-separater"></div>
        <div class="clearfix"></div>

        <div class="title-area">
            <h2 class="pull-left">Propiedades</h2>
            <!--<div class="pull-right btns-area">
                <a href="properties-map-rightside-list.html" class="change-view-btn active-view-btn"><i class="fa fa-th-list"></i></a>
                <a href="properties-map-rightside-grid.html" class="change-view-btn"><i class="fa fa-th-large"></i></a>
            </div>-->
            <div class="clearfix"></div>
        </div>
        <div class="fetching-properties"></div>
    </div>
</div>
<!-- Map content end -->
@endsection

@section('script')
    <script>
        function filtrar(){        
            
            var url = "{{ config('app.url_api').'buscador/propiedades/filtrar' }}";            

            var request = $.ajax({
                url: url,
                data: JSON.stringify({
                    localidad: $("#slt_localidad option:selected").val(),
                    asesor: $("#slt_asesor option:selected").val(),                
                    barrio: $("#slt_barrio option:selected").val(),
                    habitaciones: $("#slt_habitaciones option:selected").val(),
                    banios: $("#slt_banios option:selected").val(),
                    cochera: $("#checkbox_cochera").prop('checked'),
                    terminado: $("#checkbox_terminado").prop('checked'),
                    gas: $("#checkbox_gas").prop('checked'),
                    area_desde: $("#rng_area .current-min").val(),
                    area_hasta: $("#rng_area .current-max").val(),
                    precio_desde: $("#rng_precio .current-min").val(),
                    precio_hasta: $("#rng_precio .current-max").val(),                                                           
                }),
                type: "POST",
                contentType: "application/json; charset=utf-8",
            });

            request.done(function(result){      

                var latitude = -32.9552966;
                var longitude = -60.6719805;
                var providerName = 'OpenStreetMap';
                var zoom = 12;
                var scroll = true;
                var data = result.data;
                
                actualizarMap(latitude, longitude, providerName, null, "propiedades", data, zoom, scroll);             
                
            })
            .fail(function(error){      
                alert(error)
            });              
        }
    </script>    

    <script>
        var status_filtro = true;
        function mostrarFiltros(){            
            if(status_filtro){
                $("#filtros").show()    
                $("#boton_filtro").empty()                
                $("#boton_filtro").append("<i class='fa fa-filter'></i> Ocultar Filtros")                
                status_filtro = false;
            }
            
            else{
                $("#filtros").hide()
                $("#boton_filtro").empty()
                $("#boton_filtro").append("<i class='fa fa-filter'></i> Mostrar Filtros")                
                status_filtro = true;  
            }
        }
    </script>

    
    <script>
        var latitude = -32.9552966;
        var longitude = -60.6719805;
        var providerName = 'OpenStreetMap';
        var zoom = 12;
        var scroll = true;
        
        var url_api = "{{ config('app.url_api') }}" + 'propiedades';

        generateMap(latitude, longitude, providerName, null, "propiedades", url_api, zoom, scroll);
    </script>
    
@endsection