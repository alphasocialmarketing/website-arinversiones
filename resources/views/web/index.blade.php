@extends('index')

@section('content')   
        

        

        <!-- Testimonial section start-->
       <!-- <div class="testimonials-3 visible-lg visible-md hidden-sm hidden-xs">
            <div class="container">
                <!--<div class="row">
                    <div class="col-lg-12 col-md-12">
                        <h2 style="color: #fff;text-align: center !important;text-transform: uppercase;">Tenga la posibilidad de elegir su propiedad y/o loteo</h2>
                    </div>
                </div>-->
                <!--
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div id="carouse3-example-generic" class="carousel slide" data-ride="carousel">
                            
                            
                            
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <a href="{{ url('/propiedades') }}" class="link-principal">
                                                <p class="titulo-foto-principal-left" style="cursor: pointer;">
                                                    PROPIEDADES
                                                </p>
                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                            <a href="{{ url('/loteos') }}" class="link-principal">
                                                <p class="titulo-foto-principal-right" style="cursor: pointer;">
                                                    LOTEOS
                                                </p>
                                            </a>
                                        </div>
                                    </div>                                                
                        </div>                        
                    </div>                    
                </div>
            </div>
        </div>-->
        <!-- Testimonial  end -->
        <div class="clearfix"></div>

        
        <div class="mb-50 banner visible-lg visible-md hidden-sm hidden-xs">
            <div class="item banner-max-height active">
                <div class="carousel-caption banner-slider-inner">
                    
                        <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;margin-top: 120px !important;">
                            <a href="{{ url('/propiedades') }}" class="link-principal">
                                <h1 class="texto-slider-prop">PROPIEDADES</h1>                                            
                            </a>

                        </div>
                    
                        <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;margin-top: 120px !important;">
                            <a href="{{ url('/loteos') }}" class="link-principal">
                                <h1 class="texto-slider-lot">LOTEOS</h1>
                            </a>                                                                  
                        </div>
                    
                </div>
            </div>
        </div>

        <div class="mb-50 banner visible-sm visible-xs hidden-lg hidden-md">
            <div class="item banner-max-height active">
                <div class="carousel-caption banner-slider-inner">
                    
                        <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;margin-top: 110px !important;">
                            <a href="{{ url('/propiedades') }}" class="link-principal">
                                <h1 class="texto-slider-prop">PROPIEDADES</h1>                                            
                            </a>

                        </div>
                    
                        <div class="banner-content" style="min-width: 50% !important; margin: 0 !important;margin-top: 110px !important;">
                            <a href="{{ url('/loteos') }}" class="link-principal">
                                <h1 class="texto-slider-lot">LOTEOS</h1>
                            </a>                                                                  
                        </div>
                    
                </div>
            </div>
        </div>

        


        <!-- Agent section start -->
        <!--<div class="agent-section content-area" style="background-color: #eee;">
            <div class="container">        
                <div class="clearfix"></div>
                <div class="main-title">
                    <h1 style="color: #802d39;">Asesores Destacados</h1>
                </div>
                <div class="row">
                    @if( !empty($data['asesores']) )
                        @foreach( $data['asesores'] as $a)
                            

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    
                                    <div class="agent-2 clearfix">
                                        <div class="col-lg-5 col-md-5 col-sm-4 agent-theme-2">
                                            <img src="{{ asset($a->imagen) }}" alt="{{ $a->nombre }}" class="img-responsive">                        
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-8 agent-content">
                                            <h5>{{ $a->puesto }}</h5>
                                            <h3>
                                                <a href="{{ url('asesores/'.$a->slug) }}">{{ $a->nombre }}</a>
                                            </h3>
                                            <ul>
                                                <li>
                                                    {{ $a->profesion }}
                                                </li>
                                                <li>
                                                    <strong>Email:</strong><a href="mailto:{{ $a->email }}">{{ $a->email }}</a>
                                                </li>
                                                <li>
                                                    <strong>Celular:</strong><a href="tel:{{ $a->celular }}"> {{ $a->celular }}</a>
                                                </li>
                                                @if(!empty($a->facebook))
                                                    <li>
                                                        <a href="{{ url($a->facebook) }}" class="facebook">
                                                            <i class="fa fa-facebook"></i> Facebook
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(!empty($a->twitter))
                                                    <li>
                                                        <a href="{{ url($a->twitter) }}" class="twitter">
                                                            <i class="fa fa-twitter"></i> Twitter
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(!empty($a->instagram))
                                                    <li>
                                                        <a href="{{ url($a->instagram) }}" class="instagram">
                                                            <i class="fa fa-instagram"></i> Instagram
                                                        </a>
                                                    </li>
                                                @endif                            
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                            
                        @endforeach
                    @endif


                                                    
                </div>
            </div>
        </div>-->
        <!-- Agent section end -->



        





        <!--<div class="intro-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <img src="{{ asset('img/logos/logo-blanco.png') }}" alt="logo-2">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                        <h3 style="font-weight: 600 !important;">Querés comprar o vender alguna propiedad?</h3>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-padding">
                        <a href="{{ url('/propiedades') }}" class="btn button-md button-theme">Comprar</a>                        
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-padding">
                        <a data-toggle="modal" data-target="#modal-venta" class="btn button-md button-theme">Vender</a>                        
                    </div>
                </div>
            </div>
        </div>-->

        <div class="agent-section content-area">
            <div class="container">                
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                        <div class="clearfix"></div>
                        <div class="main-title">
                            <h1 style="color: #802d39;">Número al contactar</h1>
                        </div>
                        <div class="center-col login-box">
                            <!-- form sub title  -->
                            <p class="text-uppercase margin-three no-margin-bottom">Dejanos su número de teléfono y a la brevedad nos comunicaremos.</p>
                            <!-- end form sub title  -->
                            <div class="separator-line bg-web no-margin-lr margin-eight"></div>
                            <form action="{{ url('newsletter/guardar') }}" method="post">
                                {!! csrf_field() !!}
                                <div class="form-group no-margin-bottom">
                                    <!-- label  -->
                                    <label for="nombre" class="text-uppercase" style="color: #802d39;">Nombre</label>
                                    <!-- end label  -->
                                    <!-- input  -->
                                    <input type="text" id="nombre" name="nombre" id="nombre" required="">
                                    <!-- end input  -->
                                </div>
                                <div class="form-group no-margin-bottom">
                                    <div class="col-md-5 no-padding-left">
                                        <!-- label  -->
                                        <label for="codarea" class="text-uppercase" style="color: #802d39;">Cod Area</label>
                                        <!-- end label  -->
                                        <!-- input  -->
                                        <input type="text" id="codarea" name="codarea" id="codarea" required="">
                                        <!-- end input  -->
                                    </div>
                                    <div class="col-md-7 no-padding-right">
                                        <!-- label  -->
                                        <label for="telefono" class="text-uppercase" style="color: #802d39;">Teléfono</label>
                                        <!-- end label  -->
                                        <!-- input  -->
                                        <input type="text" id="telefono" name="telefono" id="telefono" required="">
                                        <!-- end input  -->
                                    </div>
                                </div>                           
                                <!-- button  -->
                                <button class="btn btn-web no-margin-bottom btn-medium btn-round no-margin-top" type="submit">Enviar</button>
                                <!-- end button  -->
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 hidden-xs hidden-sm">
                        <div class="clearfix"></div>
                        <div class="main-title">
                            <h1 style="color: #802d39;">Contactanos por facebook</h1>
                        </div>
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FARInversiones%2F&tabs=messages&width=585&height=411&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=342737416518508" width="585" height="411" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        
                    </div>
                    <!--<div class="col-md-4 col-sm-4 xs-margin-bottom-five">
                        <div class="clearfix"></div>
                        <div class="main-title">
                            <h1 style="color: #802d39;">¿qué hacemos?</h1>
                        </div>
                        <div class="panel-group accordion-style3" id="accordion-three">
                            
                            <div class="panel panel-default">
                                <div class="panel-heading active-accordion overflow-hidden">
                                    <a data-toggle="collapse" data-parent="#accordion-three" href="#accordion-three-link1"><h4 class="panel-title">Administramos su propiedad<span class="pull-right"><i class="fa fa-angle-up"></i></span></h4></a>
                                </div>
                                <div id="accordion-three-link1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        Nuestro índice de morosidad no supera el 1%. Controlamos el pago de impuestos y expensas y depositamos puntualmente a nuestros clientes propietarios. 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading overflow-hidden">
                                    <a data-toggle="collapse" data-parent="#accordion-three" href="#accordion-three-link2"><h4 class="panel-title">Encuentre la propiedad que usted quiere<span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
                                </div>
                                <div id="accordion-three-link2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Nuestra cartera incluye viviendas y productos de inversión destinados al mercado locativo. Asesoramos a nuestros clientes e iniciamos búsquedas específicas en el mercado cuando el potencial comprador lo requiere. 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading overflow-hidden">
                                    <a data-toggle="collapse" data-parent="#accordion-three" href="#accordion-three-link3"><h4 class="panel-title">Vendemos proyectos confiables<span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
                                </div>
                                <div id="accordion-three-link3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        La confianza es el valor que más destacan nuestros más de 5 mil clientes. Ar ofrece inmuebles de inversión de desarrolladores de jerarquía y trayectoria. 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading overflow-hidden">
                                    <a data-toggle="collapse" data-parent="#accordion-three" href="#accordion-three-link4"><h4 class="panel-title">Calidad certificada en nuestros servicios<span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
                                </div>
                                <div id="accordion-three-link4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Desde el 2006 sostenemos la certificación ISO 9001/2008 en los procesos de Tasación, Comercialización, Alquiler y Administración de Inmuebles. 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading overflow-hidden">
                                    <a data-toggle="collapse" data-parent="#accordion-three" href="#accordion-three-link5"><h4 class="panel-title">El cliente esta primero<span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
                                </div>
                                <div id="accordion-three-link5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Sabemos que el cliente es lo mas importante y brindarle un servicio perfecto es nuestra obligación 
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>     -->                
                    
                    <!--<div class="col-md-4 col-sm-4 contact-1 content-area-7" style="padding: 0px !important;">
                        <div class="clearfix"></div>
                        <div class="main-title">
                            <h1 style="color: #802d39;">Número al contactar</h1>
                        </div>
                        <p>Deje su nombre y numero de telefono y a la brevedad nos comunicaremos para resolver sus dudas.</p>

                        <div class="contact-form">
                            <div id="" class="col-md-12"></div>
                            <form id="">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="form-group fullname">
                                            <input type="text" name="nombre" class="input-text" placeholder="Cod Area">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group fullname">
                                            <input type="text" name="nombre" class="input-text" placeholder="Teléfono">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>   -->                
                </div>
            </div>
        </div>


        <div class="intro-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <img src="{{ asset('img/logos/logo-blanco.png') }}" alt="logo-2">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                        <h3 style="font-weight: 600 !important;">Querés comprar o vender alguna propiedad?</h3>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-padding">
                        <a href="{{ url('/propiedades') }}" class="btn button-md button-theme">Comprar</a>                        
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-padding">
                        <a data-toggle="modal" data-target="#modal-venta" class="btn button-md button-theme">Vender</a>                        
                    </div>
                </div>
            </div>
        </div>


        <!-- Contact body start -->
        <div class="contact-1 content-area-7" id="contacto">
            <div class="container">
                <div class="main-title">
                    <h1 style="color: #802d39;">Contacto</h1>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <!-- Contact form start -->
                        <div class="contact-form">
                            <div id="mensajes_form" class="col-md-12"></div>
                            <form id="form_home">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group fullname">
                                            <input type="text" name="nombre" class="input-text" placeholder="Nombre">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group enter-email">
                                            <input type="email" name="email" class="input-text" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group subject">
                                            <input type="text" name="asunto" class="input-text" placeholder="Asunto">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group number">
                                            <input type="text" name="telefono" class="input-text" placeholder="Telefono">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                                        <div class="form-group message">
                                            <textarea class="input-text" name="mensaje" placeholder="Mensaje"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group send-btn mb-0">
                                            <button id="boton_form_enviar" class="button-md button-theme">Enviar Mensaje</button>
                                        </div>
                                    </div>                                    
                                </div>
                            </form>     
                        </div>
                        <!-- Contact form end -->
                    </div>
                    <br>
                    <!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FARInversiones%2F&tabs=timeline&width=370&height=430&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="370" height="430" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                    </div>-->
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <!-- Contact details start -->
                        <div class="contact-details">
                            <div class="main-title-2">
                                <h3></h3>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Dirección</h4>
                                    <p>Balcarce 1424</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Teléfonos</h4>
                                    <p>
                                        <a href="tel:+5493413692568">0341-6792590</a>
                                    </p>                            
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Email</h4>
                                    <p>
                                        <a href="mailto:info@arinversiones.com.ar">info@arinversiones.com.ar</a>
                                    </p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Horarios</h4>
                                    <p>
                                        Lun a Vie: 9hs a 19hs.
                                    </p>
                                </div>
                            </div>                           
                        </div>
                        <!-- Contact details end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact body end -->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3347.9755176207973!2d-60.65619008473673!3d-32.95165577947206!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95b7ab6b7abe2dc5%3A0x6a99e40f08c1beae!2sBalcarce%201424%2C%20S2000DOD%20Rosario%2C%20Santa%20Fe!5e0!3m2!1ses!2sar!4v1568035397281!5m2!1ses!2sar" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div class="intro-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <img src="{{ asset('img/logos/logo-blanco.png') }}" alt="logo-2">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                        <h3 style="font-weight: 600 !important;">Querés comprar o vender alguna propiedad?</h3>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-padding">
                        <a href="{{ url('/propiedades') }}" class="btn button-md button-theme">Comprar</a>                        
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 no-padding">
                        <a data-toggle="modal" data-target="#modal-venta" class="btn button-md button-theme">Vender</a>                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Agent section start -->
        <div class="agent-section content-area" style="background-color: #eee;">
            <div class="container">        
                <div class="clearfix"></div>
                <div class="main-title">
                    <h1 style="color: #802d39;">Asesores</h1>
                </div>
                <div class="row">
                    <div class="carousel our-partners slide" id="ourPartners3">
                        <div class="col-lg-12 mrg-btm-30">
                            <a class="left carousel-control" href="#ourPartners3" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                            <a class="right carousel-control" href="#ourPartners3" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                        </div>
                        <div class="carousel-inner">

                            @php
                                $cont = 0;
                            @endphp



                            @if( !empty($data['asesores']) )
                                @foreach( $data['asesores'] as $a)
                                    @if($cont == 0)
                                        <div class="item active">
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                
                                                <div class="agent-1">
                                                    
                                                    <a href="{{ url('asesores/'.$a->slug) }}" class="agent-img">
                                                        <img src="{{ asset($a->imagen) }}" alt="{{ $a->nombre }}" class="img-responsive">
                                                    </a>
                                                    
                                                    <div class="agent-content">
                                                        <h5><a href="{{ url('asesores/'.$a->slug) }}">{{ $a->nombre }}</a></h5>
                                                        <h6>{{ $a->puesto }}</h6>
                                                        <h6>{{ $a->celular }}</h6>
                                                        <ul class="social-list clearfix">
                                                            @if(!empty($a->facebook))
                                                                <li>
                                                                    <a href="{{ $a->facebook }}" class="facebook">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                            @if(!empty($a->twitter))
                                                                <li>
                                                                    <a href="{{ $a->twitter }}" class="twitter">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                            @if(!empty($a->instagram))
                                                                <li>
                                                                    <a href="{{ $a->instagram }}" class="instagram">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </a>
                                                                </li>
                                                            @endif                                                        
                                                        </ul>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    @else
                                        <div class="item">
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                
                                                <div class="agent-1">
                                                    
                                                    <a href="{{ url('asesores/'.$a->slug) }}" class="agent-img">
                                                        <img src="{{ asset($a->imagen) }}" alt="{{ $a->nombre }}" class="img-responsive">
                                                    </a>
                                                    
                                                    <div class="agent-content">
                                                        <h5><a href="{{ url('asesores/'.$a->slug) }}">{{ $a->nombre }}</a></h5>
                                                        <h6>{{ $a->puesto }}</h6>
                                                        <h6>{{ $a->celular }}</h6>
                                                        <ul class="social-list clearfix">
                                                            @if(!empty($a->facebook))
                                                                <li>
                                                                    <a href="{{ $a->facebook }}" class="facebook">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                            @if(!empty($a->twitter))
                                                                <li>
                                                                    <a href="{{ $a->twitter }}" class="twitter">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                            @if(!empty($a->instagram))
                                                                <li>
                                                                    <a href="{{ $a->instagram }}" class="instagram">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </a>
                                                                </li>
                                                            @endif                                                        
                                                        </ul>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    @endif

                                    @php
                                        $cont = $cont + 1;
                                    @endphp

                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection

@section('script')

    <!-- FORMULARIO CONTACTO -->
    
    <script type="text/javascript">    
        $("#boton_form_enviar").click(function(event){
            
            event.preventDefault();

            var url_consulta = "{{ config('app.url_api').'formulario/consulta/home' }}";

            var request = $.ajax({
                url: url_consulta,
                type: "POST",                
                data: $("#form_home").serialize(),                
            });

            waitingAjax();

            request.done(function(result){  
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-success' role='alert'>" +
                                "se envio el mensaje con exito" +
                             "</div>";

                $("#mensajes_form").append(alert);                

                $("#form_home")[0].reset();

                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);                
            });

            request.fail(function(jqXHR, textStatus){               
                $("#mensajes_form").empty();

                var alert = "<div class='alert alert-danger' role='alert'>" +
                                jqXHR.responseJSON.mensaje
                             "</div>";
                             
                $("#mensajes_form").append(alert);                

                setTimeout(function(){ 
                        $("#mensajes_form").empty();
                },3000);
            });            

        });

        function waitingAjax(){
                var alert = "<div class='alert alert-info' role='alert'>" +
                                "Estamos processando su consulta" +
                             "</div>";

                $("#mensajes_form").append(alert);                
        }
    </script>
@endsection
