@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Plano de Mensura: {{ $data["loteo"]->nombre }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('admin') }}">{{ config('backpack.base.project_name') }}</a></li>
        <li><a href="{{ url('admin/loteos') }}">Loteos</a></li>
        <li class="active">Plano de Mensura</li>        
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            @if( empty($data["loteo"]->plano_mensura) )
                <div class="callout callout-warning">
                    <h4>
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Plano de Mensura</font>
                        </font>
                    </h4>
    
                    <p>
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">El loteo no tiene cargado el plano de mensura.</font>
                        </font>
                    </p>
                </div>
            @else                
                <canvas id="canvas" width="800" height="600"> 
                    Your browser does not support the canvas element. 
                </canvas>                             
            @endif
        </div>
    </div>
    <div id="modal_lotes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_titulo">Lote</h4>
            </div>
            <div class="modal-body">
                <form>
                    <input id="frm_id" type="hidden">
                    <input id="frm_pageX" type="hidden">
                    <input id="frm_pageY" type="hidden">

                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre</label>
                            <input type="text" id="frm_nombre" placeholder="Nombre" class="form-control"/>
                        </div>
                        <div class="col-md-12">
                            <label>Detalle</label>
                            <textarea name="detalle" id="frm_detalle" class="form-control" placeholder="Detalle"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Precio</label>
                            <input type="number" step="0.01" id="frm_precio" placeholder="Precio" class="form-control"/>
                        </div>
                        <div class="col-md-6">
                            <label>Estado</label>
                            <select name="estado" class="form-control" id="frm_estado">
                                <option value="0"></option>
                                <option value="DISPONIBLE">DISPONIBLE</option>
                                <option value="RESERVADO">RESERVADO</option>
                                <option value="VENDIDO">VENDIDO</option>
                            </select>
                        </div>  

                        @php
                            $u = \Auth::user();
                            $vendedor = false;
                            $asesor_id = null;
                            
                            if($u->hasRole('vendedor')){                                            
                                if(!empty($u->asesor)){                                                                        
                                    if(count($u->asesor)>1)
                                        return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 
                                                                        
                                    $vendedor = true;
                                    $asesor_id = $u->asesor()->first()->id;
                                }
                            }                                                        
                        @endphp

                        @if(!$vendedor)                        
                            <div class="col-md-12">
                                <label>Asesores</label>
                                <select name="asesor" class="form-control" id="frm_asesor">
                                </select>
                            </div> 
                        @else                        
                            <input id="frm_asesor" type="hidden" value="{{ $asesor_id }}">
                        @endif
                    </div>                      
                </form>
            </div>
            <div class="modal-footer">
                <button id="modal_boton_eliminar" type="button" class="btn btn-danger pull-left">Eliminar</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="modal_boton_guardar" type="button" class="btn btn-success" onclick="guardarLote()">Guardar</button>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    <script type="text/javascript"> 
        var canvas = document.getElementById("canvas");        
        var ctx = canvas.getContext("2d");

        var items = [];
        
        $(document).ready(function() {            
            var img = new Image();
            img.src = "{{ asset('storage/'.$data['loteo']->plano_mensura) }}";

            img.onload = function(){
                ctx.drawImage(img, 0, 0);
                actualizarMapa();
            }     

            cargarSelectAsesores()                
        });

        canvas.addEventListener('mousemove', (e) => {           
            var offset =  $('#canvas').offset();          
            var x = e.pageX - offset.left
            var y = e.pageY - offset.top        
            
            var status = false
            items.forEach(function(element) {
                var desde_x = parseFloat(element.ref_x) - 10;
                var desde_y = parseFloat(element.ref_y) - 10;
                
                if(x >= desde_x && x <= element.ref_x &&
                   y >= desde_y && y <= element.ref_y){                                
                    status = true
                }
        
            })            

            if(status)
                canvas.style.cursor = "pointer"
            else
                canvas.style.cursor = "default"
        });  

        canvas.addEventListener('click', (e) => {
            var offset =  $('#canvas').offset();          
            var x = e.pageX - offset.left            
            var y = e.pageY - offset.top        
            
            var status = false
            var point = null
            
            items.forEach(function(element) {
                var desde_x = parseFloat(element.ref_x) - 10;
                var desde_y = parseFloat(element.ref_y) - 10;
                
                if(x >= desde_x && x <= element.ref_x &&
                   y >= desde_y && y <= element.ref_y){                                
                    status = true
                    point = element
                }
            })   

            if(status && point!=null)
                crearLote(null, null, point)
            else
                crearLote(x, y, null)
        });  

        function dibujarLotes(val){  
            ctx.beginPath();
            
            var x = val.ref_x 
            var y = val.ref_y
            var estado = val.estado
            var id = val.id

            switch(estado){
                case "DISPONIBLE":
                    ctx.fillStyle = "#008000";          
                    break

                case "RESERVADO":
                    ctx.fillStyle = "#ffc807";          
                    break

                case "VENDIDO":
                    ctx.fillStyle = "#f44336";          
                    break
            }
            
            ctx.arc(x, y, 10, 0, 2 * Math.PI, false);            
            ctx.fill();
            ctx.fillRect(x,y,10,10);
            
            items.push(val)
        }
    </script> 

    <script>

        function crearLote(x, y, data){                            
            $('#modal_lotes').modal({
                show: true
            });      

            if(x && y){
                $("#modal_titulo").text("NUEVO LOTE")
                $("#frm_id").val(0)                
                $("#frm_pageX").val(x)
                $("#frm_pageY").val(y)  
                $("#frm_nombre").val("")
                $("#frm_detalle").val("")
                $("#frm_precio").val("")
                $("#frm_estado").val("")    
                $("#frm_asesor").val("{{ $asesor_id }}") 

                $("#modal_boton_eliminar").hide();   

                if( "{{ $vendedor }}" ){                                        
                    $("#modal_boton_guardar").show();
                    $("#modal_boton_eliminar").show();                            
                }

            }
            else{            
                $("#modal_titulo").text("EDITAR LOTE")
                $("#frm_id").val(data.id)     
                $("#frm_pageX").val(data.ref_x)
                $("#frm_pageY").val(data.ref_y)          
                $("#frm_nombre").val(data.nombre)
                $("#frm_detalle").val(data.detalle)
                $("#frm_precio").val(data.precio)
                $("#frm_estado").val(data.estado) 
                $("#frm_asesor").val(data.asesor_id) 

                $("#modal_boton_eliminar").show();                   
                $("#modal_boton_eliminar").on("click", function(){ eliminarLote(data); });                                           

                if( "{{ $vendedor }}" ){
                    if( "{{ $asesor_id }}" != data.asesor_id){
                        alert("Este lote no es de la propiedad del asesor logueado")
                        $("#modal_boton_guardar").hide();
                        $("#modal_boton_eliminar").hide();
                    }
                    else{                        
                        $("#modal_boton_guardar").show();
                        $("#modal_boton_eliminar").show();
                    }            
                }
            }
        }

        function actualizarMapa(){            
            var url = "{{ config('app.url_api').'plano-mensura/obtener' }}";        
            
            var request = $.ajax({
                url: url,
                data: JSON.stringify({
                    id: "{{ $data['loteo']->id }}",                    
                }),
                type: "POST",
                contentType: "application/json; charset=utf-8",
            });

            request.done(function(result){     
                items = []                               
                jQuery.each(result, function(i, val) {                    
                    dibujarLotes(val);
                });       
            })
            .fail(function(error){      
                alert("ERROR: " + error.responseText);
            });              
        }

        function guardarLote(){
            
            var status = confirm("¿Guardar Lote?")

            if(status){                
                var url = "{{ config('app.url_api').'plano-mensura/crear' }}";                        

                var request = $.ajax({
                    url: url,
                    data: JSON.stringify({
                        id: $("#frm_id").val(),
                        nombre: $("#frm_nombre").val(),
                        detalle: $("#frm_detalle").val(),
                        ref_x: $("#frm_pageX").val(),
                        ref_y: $("#frm_pageY").val(),
                        precio: $("#frm_precio").val(),
                        estado: $("#frm_estado").val(),
                        asesor_id: $("#frm_asesor").val(),
                        loteo_id: "{{ $data['loteo']->id }}" ,
                    }),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                });

                request.done(function(result){                                                                                            
                    actualizarMapa();                           
                    $('#modal_lotes').modal('hide');
                })
                .fail(function(error){                          
                    alert("ERROR: " + error.responseText);
                });  
            }               
        }

        function eliminarLote(data){
            var status = confirm("¿Eliminar Lote [" + data.nombre + "] ?")

            if(status){                
                var url = "{{ config('app.url_api').'plano-mensura/borrar' }}";                        

                var request = $.ajax({
                    url: url,
                    data: JSON.stringify({
                        id: $("#frm_id").val(),
                    }),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                });

                request.done(function(result){                                                                                                                                                  
                    alert("Lote [" + data.nombre + "] borrado")
                    location.reload();                    
                })
                .fail(function(error){      
                    alert("ERROR: " + error.responseText);
                });  
            }                           
        }

        function cargarSelectAsesores(){
            var url = "{{ config('app.url_api').'plano-mensura/obtener/asesores' }}";        
            
            var request = $.ajax({
                url: url,
                type: "GET",
                contentType: "application/json; charset=utf-8",
            });

            request.done(function(result){     
                items = []                              

                $('#frm_asesor').append($('<option>', { 
                    value: 0,
                    text : "" 
                }));

                jQuery.each(result, function(i, val) {                    
                    $('#frm_asesor').append($('<option>', { 
                        value: val.id,
                        text : val.nombre 
                    }));
                });       
            })
            .fail(function(error){                      
                alert("ERROR: " + error.responseText);
            });            
        }
    </script>    

    
@endsection