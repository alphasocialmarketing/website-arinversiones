@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Administrador General
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('admin') }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Administrador</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-success">
                <h4>Bienvenido {{ Auth()->user()->name }}!</h4>

                <p>
                  @if(Auth()->user()->hasRole('superadmin'))
                    {!! \Config::get('settings.mensaje_principal_superusuario') !!}
                  @endif
                  
                  @if(Auth()->user()->hasRole('usuario'))
                    {!! \Config::get('settings.mensaje_principal_usuario') !!}
                  @endif

                  @if(Auth()->user()->hasRole('cliente'))
                    {!! \Config::get('settings.mensaje_principal_cliente') !!}
                  @endif                  
                </p>
              </div>
        </div>
    </div>
@endsection
