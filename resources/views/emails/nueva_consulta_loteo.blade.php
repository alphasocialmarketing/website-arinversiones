<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nueva Consulta Loteo</title>

    <style>
        .negrita {
            font-weight: bold;
        }
    </style>    
</head>
<body>
    <p>
        Nueva consulta sobre loteos desde sitio web
    </p>
    <p>
        <table>
            <tr>
                <td class="negrita">Nombre y Apellido:</td>
                <td>{{$consulta->nombre}}</td>                
            </tr>             
            <tr>
                <td class="negrita">Teléfono:</td>
                <td>{{$consulta->telefono}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Email:</td>
                <td>{{$consulta->email}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Asunto:</td>
                <td>{{$consulta->asunto}}</td>                
            </tr>                      
            <tr>
                <td class="negrita">Mensaje:</td>
                <td>{{$consulta->mensaje}}</td>                
            </tr>                                                                                
        </table>

    </p>
    <p>
        Para más información, ingresar a <a href="{{url('/admin/consultas-loteos')}}">{{url('/admin/consultas-loteos')}}</a>
    </p>
</body>
</html>