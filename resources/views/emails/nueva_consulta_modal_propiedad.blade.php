<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo Consulta por venta de propiedad</title>

    <style>
        .negrita {
            font-weight: bold;
        }
    </style>    
</head>
<body>
    <p>
        Nueva consulta por venta de porpiedad desde sitio web
    </p>
    <p>
        <table>
            <tr>
                <td class="negrita">Nombre:</td>
                <td>{{$contacto->nombre}}</td>                
            </tr>             
            <tr>
                <td class="negrita">Teléfono:</td>
                <td>{{$contacto->telefono}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Email:</td>
                <td>{{$contacto->email}}</td>                
            </tr>                                              
            <tr>
                <td class="negrita">Información:</td>
                <td>{{$contacto->informacion}}</td>                
            </tr>                                                                                
        </table>

    </p>
    <p>
        Para más información, ingresar a <a href="{{url('/admin/consultas-venta-propiedad')}}">{{url('/admin/consultas-venta-propiedad')}}</a>
    </p>
</body>
</html>