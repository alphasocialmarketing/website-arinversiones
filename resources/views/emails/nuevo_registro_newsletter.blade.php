<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo registro de Telefono</title>
</head>
<body>
	<p>
		<b>{{ $contacto->nombre }}</b>
	</p>
    <p>
        <b>{{ $contacto->codarea }}</b>-<b>{{ $contacto->telefono }}</b> se suscribio a la lista de newsletter
    </p>
    <p>
        Para más información, ingresar a <a href="{{url('/admin/newsletter')}}">{{url('/admin/newsletter')}}</a>
    </p>
</body>
</html>