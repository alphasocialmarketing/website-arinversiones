<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo Consulta Asesor</title>

    <style>
        .negrita {
            font-weight: bold;
        }
    </style>    
</head>
<body>
    <p>
        Nueva consulta desde sitio web
    </p>
    <p>
        <table>
            <tr>
                <td class="negrita">Nombre:</td>
                <td>{{$consulta->nombre}}</td>                
            </tr>             
            <tr>
                <td class="negrita">Teléfono:</td>
                <td>{{$consulta->telefono}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Email:</td>
                <td>{{$consulta->email}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Asunto:</td>
                <td>{{$consulta->asunto}}</td>                
            </tr>                      
            <tr>
                <td class="negrita">Mensaje:</td>
                <td>{{$consulta->mensaje}}</td>                
            </tr>                                                                                
        </table>

    </p>
    <p>
        Para más información, ingresar a <a href="{{url('/admin/consultas-asesores')}}">{{url('/admin/consultas-asesores')}}</a>
    </p>
</body>
</html>