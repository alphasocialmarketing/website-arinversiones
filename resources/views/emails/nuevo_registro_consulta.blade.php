<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo Consulta General</title>

    <style>
        .negrita {
            font-weight: bold;
        }
    </style>    
</head>
<body>
    <p>
        Nueva consulta desde sitio web
    </p>
    <p>
        <table>
            <tr>
                <td class="negrita">Nombre:</td>
                <td>{{$contacto->nombre}}</td>                
            </tr>             
            <tr>
                <td class="negrita">Teléfono:</td>
                <td>{{$contacto->telefono}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Email:</td>
                <td>{{$contacto->email}}</td>                
            </tr>                        
            <tr>
                <td class="negrita">Asunto:</td>
                <td>{{$contacto->asunto}}</td>                
            </tr>                      
            <tr>
                <td class="negrita">Mensaje:</td>
                <td>{{$contacto->mensaje}}</td>                
            </tr>                                                                                
        </table>

    </p>
    <p>
        Para más información, ingresar a <a href="{{url('/admin/consultas')}}">{{url('/admin/consultas')}}</a>
    </p>
</body>
</html>