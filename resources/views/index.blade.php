<!-- http://themevessel-item.s3-website-us-east-1.amazonaws.com/nest/index.html -->

<!-- ################################### 
     SCRIPT NAME PAGINA
     ################################### -->
        @php
            if(isset($_GET['nombrePagina'])){
                if(isset($data['page']))
                    echo $data['page'];
                else
                    echo "Falta setear el valor para data['page']";

                exit();
            }        
        @endphp
<!-- ################################### 
     FIN SCRIPT NAME PAGINA
     ################################### -->

<!doctype html>
<html>

    <head>
        <!-- ################################### 
             ################################### 
             ################################### 
             CONFIGURACION DEL SITIO WEB ####### -->
                @include('web.inc.estructura.head.meta') 

                <style type="text/css">
                    :root{
                      --main-bg-color: {{ \Config::get('settings.color-principal') }};
                      --main-bg-color-carrito: {{ \Config::get('settings.color-carrito') }};
                    }
                </style>

                @include('web.inc.estructura.head.estilos') 

                @yield('estilos')
    </head>

    <body>
        <!-- ################################### 
             NAV SUPERIOR ###################### -->
            @include('web.inc.estructura.body.nav.index') 
       
        <!-- ################################### 
             HEADER ############################ -->
            @if($data["page"]=="index")
                @include('web.inc.estructura.body.header.index')                 
            @endif
                   
        <!-- ################################### 
             ################################### 
             ################################### 
             CONTENIDO ######################### -->
            
            @yield('content')    

            <!--<footer>-->
                @if($data["page"]!="propiedades" && $data["page"]!="loteos" && $data["page"]!="imprimir")
                    @include('web.inc.estructura.footer.copyright') 
                @endif
            <!--</footer>-->
            
        
        <!-- ################################### 
             ################################### 
             ################################### 
             SCRIPT ######################### -->
            @include('web.inc.estructura.footer.script')         
            @yield('script')

        <!-- ################################### 
             FORM MODALES ###################### -->
            @include('web.inc.alertas')    
            @include('web.inc.modalVender') 
            
            @include('web.inc.login.login') 
            
            {{--
                @include('web.inc.login.recuperar') 
                @include('web.inc.login.registrar')   
            --}}

        <!-- ################################### 
             SCRIPT AGREGADOS DESDE EL ADMIN 
             ################################### -->            
            @if(!empty( $data['script'] ))
                @foreach($data['script'] as $s)
                    
                    @if($s->page=="")                                    
                        {!!$s->script !!}
                    @endif                    

                    @if($s->page==$data['page'])                    
                        {!!$s->script !!}
                    @endif                    
                
                @endforeach
            @endif
        <!-- ################################### 
             FIN SCRIPT AGREGADOS DESDE EL ADMIN 
             ################################### --> 

            <!-- MODAL  -->
            <script type="text/javascript">    
                $("#boton_modal_enviar").click(function(event){

                    event.preventDefault();
                    
                    var url_consulta = "{{ config('app.url_api').'modal/venta/propiedad' }}";

                    var request = $.ajax({
                        url: url_consulta,
                        type: "POST",                
                        data: $("#form_modal_venta").serialize(),                
                    });

                    loadingAjax();

                    request.done(function(result){  
                        $("#mensajes_modal").empty();

                        var alert = "<div class='alert alert-success' role='alert'>" +
                                        "se envio el mensaje con exito" +
                                    "</div>";
                        

                        $("#mensajes_modal").append(alert);                

                        $("#form_modal_venta")[0].reset();
                    
                        setTimeout(function(){ 
                                $("#mensajes_modal").empty();
                        },3000);                
                    });

                    request.fail(function(jqXHR, textStatus){               
                        $("#mensajes_modal").empty();

                        var alert = "<div class='alert alert-danger' role='alert'>" +
                                        jqXHR.responseJSON.mensaje
                                    "</div>";
                                    
                        $("#mensajes_modal").append(alert);                

                        setTimeout(function(){ 
                                $("#mensajes_modal").empty();
                        },3000);
                    });            

                });

                function loadingAjax(){
                        var alert = "<div class='alert alert-info' role='alert'>" +
                                        "Estamos processando su consulta" +
                                    "</div>";

                        $("#mensajes_modal").append(alert);                
                }

            </script>
                    
    </body>
</html>
