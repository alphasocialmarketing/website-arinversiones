<div class="modal fade" id="modal-form-create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    
      <form method="POST" action="@yield('modal-form-create-action')">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">@yield('modal-form-create-titulo')</h4>
        </div>
        <div class="modal-body">
          @yield('modal-form-create-contenido')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <button type="submit" name='submit' class="btn btn-primary">Guardar</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="modal fade" id="modal-form-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    
      <form method="POST" action="@yield('modal-form-edit-action')">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">@yield('modal-form-edit-titulo')</h4>
        </div>
        <div class="modal-body">
          @yield('modal-form-edit-contenido')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <button type="submit" name='submit' class="btn btn-primary">Guardar</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="modal fade modal-danger" id="modal-form-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
      <form method="post" action="@yield('modal-form-delete-action')">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">@yield('modal-form-delete-titulo')</h4>
        </div>
        <div class="modal-body">
          @yield('modal-form-delete-contenido')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
          <button type="submit" name='submit' class="btn btn-outline">Eliminar</button>
        </div>
      </form>

    </div>
  </div>
</div>