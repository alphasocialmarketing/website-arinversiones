@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ Auth::user()->name[0] }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            @foreach(Auth::user()->roles as $r)
              <a>{{ $r->name }}</a><br>
            @endforeach
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          

          <!-- ############ -->
          <!--   CLIENTES   -->
          <!-- ############ -->
          
          @hasrole(['vendedor'])     
            <li class="header">BUSCADOR</li>
            <li class="treeview">
              <a href="#"><i class="fa fa-search"></i> <span>Buscador</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">              
                <li><a href="{{ url('admin/propiedades') }}"><i class="fa fa-building-o"></i> <span>Propiedades</span></a></li>
                <li><a href="{{ url('admin/loteos') }}"><i class="fa fa-map-signs"></i> <span>Loteos</span></a></li>              
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="fa fa-comments"></i> <span>Formularios</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">                                
                <li><a href="{{ url('admin/consultas-propiedades') }}"><i class="fa fa-inbox"></i> <span>Desde Propiedades</span></a></li>
                <li><a href="{{ url('admin/consultas-loteos') }}"><i class="fa fa-inbox"></i> <span>Desde Loteos</span></a></li>
                <li><a href="{{ url('admin/consultas-asesores') }}"><i class="fa fa-inbox"></i> <span>Desde Asesor</span></a></li>
              </ul>
            </li>                
          @endhasrole

          <!-- ############ -->
          <!--   USUARIOS   -->
          <!-- ############ -->

          @hasrole('usuario')    
            <li class="header">BLOG</li>
          @endhasrole


          <!-- ############ -->
          <!--  SUPERADMIN  -->
          <!-- ############ -->

          
          @hasrole('superadmin')              
            <li class="header">SITIO WEB</li>
                    
            <li><a href="{{ url('admin/slider') }}"><i class="fa fa-sliders"></i> <span>Sliders</span></a></li>
            <li><a href="{{ url('admin/asesores') }}"><i class="fa fa-briefcase"></i> <span>Asesores</span></a></li>

            
            <!-- Busqueda -->           
            <li class="treeview">
              <a href="#"><i class="fa fa-search"></i> <span>Buscador</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">              
                <li><a href="{{ url('admin/propiedades-admin') }}"><i class="fa fa-building-o"></i> <span>Propiedades</span></a></li>
                <li><a href="{{ url('admin/loteos-admin') }}"><i class="fa fa-map-signs"></i> <span>Loteos</span></a></li>              
              </ul>
            </li>
            
            <!-- ABM -->           
            <li class="treeview">
              <a href="#"><i class="fa fa-list"></i> <span>Opciones</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">              
                <li><a href="{{ url('admin/barrios') }}"><i class="fa fa-dot-circle-o"></i> <span>Barrios</span></a></li>
                <li><a href="{{ url('admin/localidades') }}"><i class="fa fa-dot-circle-o"></i> <span>Localidades</span></a></li>
                <li><a href="{{ url('admin/servicios') }}"><i class="fa fa-dot-circle-o"></i> <span>Servicios</span></a></li>              
              </ul>
            </li>            

            <!-- Formularios -->           
            <li class="treeview">
              <a href="#"><i class="fa fa-comments"></i> <span>Formularios</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">              
                <li><a href="{{ url('admin/consultas') }}"><i class="fa fa-inbox"></i> <span>Desde Contacto</span></a></li>
                <li><a href="{{ url('admin/consultas-propiedades') }}"><i class="fa fa-inbox"></i> <span>Desde Propiedades</span></a></li>
                <li><a href="{{ url('admin/consultas-loteos') }}"><i class="fa fa-inbox"></i> <span>Desde Loteos</span></a></li>
                <li><a href="{{ url('admin/consultas-asesores') }}"><i class="fa fa-inbox"></i> <span>Desde Asesor</span></a></li>
                <li><a href="{{ url('admin/consultas-venta-propiedad') }}"><i class="fa fa-inbox"></i> <span>Desde Venta de Propiedad</span></a></li>
                <li><a href="{{ url('admin/newsletter') }}"><i class="fa fa-mobile"></i> <span>Newsletter Teléfono</span></a></li>
              </ul>
            </li>                           

            <li class="header">CONFIGURACIÓN</li>              
            <li><a href="{{ url('admin/elfinder') }}"><i class="fa fa-files-o"></i> <span>Archivos</span></a></li>

            <li class="treeview">
              <a href="#"><i class="fa fa-cogs"></i> <span>Parámetros</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">                                              
                <li><a href="{{ url('admin/script') }}"><i class="fa fa-code"></i> <span>Script</span></a></li>              
                <li><a href="{{ url('admin/setting') }}"><i class="fa fa-cog"></i> <span>Opciones</span></a></li>  
              </ul>
            </li>            
          
            <li class="header">PERMISOS</li>  

            <li class="treeview">
              <a href="#"><i class="fa fa-lock"></i> <span>Accesos al Sistema</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url('admin/user') }}"><i class="fa fa-user"></i> <span>Usuarios</span></a></li>
                <li><a href="{{ url('admin/role') }}"><i class="fa fa-group"></i> <span>Rol</span></a></li>              
              </ul>
            </li>            
          @endhasrole          

        </ul>
      </section>      
    </aside>
@endif
