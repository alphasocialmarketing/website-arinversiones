<?php

namespace App;

use Backpack\CRUD\CrudTrait; 
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;  

//use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use App\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use App\Notifications\NuevoRegistroConsulta;

class User extends Authenticatable
{
    use CrudTrait;
    use HasRoles; 
    use Notifiable;


    protected $fillable = ['name', 'email', 'password', 'direccion', 'localidad', 'empresa', 'cuit', 'web', 'telefono', 'celular'];
    protected $hidden = ['password', 'remember_token',];


    /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
      $this->notify(new ResetPasswordNotification($token));
  }

  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */
  public function isVendedor(){
    return $this->hasRole('vendedor');
  }
  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

    public function articulos()
    {
        return $this->hasMany('App\Models\Articulos');
    }
    public function asesor(){
        return $this->hasMany('App\Models\Asesores');
    }
  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */
  
  /*
  |--------------------------------------------------------------------------
  | ACCESORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
