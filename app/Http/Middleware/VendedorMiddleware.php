<?php

namespace App\Http\Middleware;

use Closure;

class VendedorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if ($request->user()->hasRole(['vendedor'])) {            
            return $next($request);
        }        
        return abort(403, "No tienes permiso para ver esta página");
    }
}
