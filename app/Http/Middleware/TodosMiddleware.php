<?php

namespace App\Http\Middleware;

use Closure;

class TodosMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->hasRole(['superadmin','usuario','vendedor'])) {
            return $next($request);
        }        
        return abort(403, "No tienes permiso para ver esta página");  
    }
}
