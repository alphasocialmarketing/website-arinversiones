<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticuloRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoria_blog_id' => 'required',
            'fecha' => 'required',
            'titulo' => 'required',
            'intro' => 'required',
            'contenido' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'categoria_blog_id.required' => 'La categoria es obligatoria.',
            'fecha.required' => 'La fecha es obligatoria.',
            'titulo.required' => 'El título es obligatorio.',
            'intro.required' => 'La intro es obligatoria.',
            'contenido.required' => 'El contenido es obligatorio.',
        ];
    }
}