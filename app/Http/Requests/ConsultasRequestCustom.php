<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConsultasRequestCustom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'g-recaptcha-response' => 'required|recaptcha',
            'nombre' => 'required',
            'email' => 'required',         
        ];
    }

    public function messages()
    {
        return [            
            'g-recaptcha-response.required' => 'El campo recaptcha es requerido',
            'nombre.required' => "El campo nombre es requerido",
            'email.required' => "El campo email es requerido",           
        ];
    }    
}
