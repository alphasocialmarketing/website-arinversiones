<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Propiedades;

class PropiedadesController extends Controller
{
    function seleccionarPropiedades($propiedades=null){
        if($propiedades)
            $prop = $propiedades;
        else
    	    $prop = Propiedades::where('mostrar',true)->get();

    	foreach ($prop as $key => $value) {
    		
            if(!empty($value->direccion)){
        		$arrDir = json_decode($value->direccion);
        		$lat = (float) $arrDir->latlng->lat;
        		$lng = (float) $arrDir->latlng->lng;
                $dire = $arrDir->value;
            }
            else{
                $lat = (float) str_replace(',', '.', $value->lat);
                $lng = (float) str_replace(',', '.', $value->lng);
                $dire = $value->direccion_tmp;
            } 
          
            if(!empty(json_decode($value->imagenes,true)))
                $img = json_decode($value->imagenes,true)[0]["imagen"];
            else
                $img = '';

    		$data[] = array(
                            "id"=>$value->id,
                            "title"=>$value->nombre,
    						"address"=>$dire, 
    						"latitude"=>$lat,
    						"longitude"=>$lng,
    						"image"=>asset($value->principal),
                            "description"=>$value->descripcion,
                            "intro"=>$value->intro,
                            "habitaciones"=>$value->habitaciones,
                            "bathroom"=>$value->banios,
                            "garage"=>$value->cochera,
                            "terminada"=>$value->terminada,
                            "gas"=>$value->gas,
                            "price"=>$value->precio,
                            "area"=>$value->medidas,
                            "description"=>$value->descripcion,
                            "barrio"=>$value->barrio()->first(),                            
                            "author"=>$value->asesor()->first(),
                            "type_icon"=>asset('img/building.png'),
                            "slug"=>url('propiedades/'.$value->slug),
                            "mostrar"=>$value->ocultar_precio,
                            "moneda"=>$value->moneda,
                            //"listing_for"=>'',
                            //"date"=>'',
                            //"is_featured"=>'',
                            //"balcony"=>'',
                            //"lounge"=>'',
                        );

            
    	}    	

    	return response()->json(compact("data"),200);
    }

    function seleccionarPropiedadesById($id){
        $prop = Propiedades::where("id",$id)->get();
        
        return $this->seleccionarPropiedades($prop);
 
    }

    function filtrar(Request $request){        
        
        $p = new Propiedades;
        
        $p = $p->where("mostrar",true);

        if(!empty($request->asesor))
            $p = $p->where("asesor_id",$request->asesor);

        if(!empty($request->barrio))
            $p = $p->where("barrio_id",$request->barrio);
            
        if(!empty($request->habitaciones))
            $p = $p->where("habitaciones",$request->habitaciones);
            
        if(!empty($request->banios))
            $p = $p->where("banios",$request->banios);

        if($request->cochera)
            $p = $p->where("cochera",$request->cochera);

        if($request->terminado)
            $p = $p->where("terminada",$request->terminado);

        if($request->gas)
            $p = $p->where("gas",$request->gas);                

        if($request->area_hasta)
            $p = $p->whereBetween("medidas",array($request->area_desde,$request->area_hasta));
            
        if($request->precio_hasta)
            $p = $p->whereBetween("precio",array($request->precio_desde,$request->precio_hasta));
        
        return $this->seleccionarPropiedades($p->get());        
        
    }
        
}

