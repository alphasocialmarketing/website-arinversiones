<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Asesores;
use App\Models\ConsultasAsesores;

use Mail;
use App\Mail\NuevaConsultaAsesor;

use App\User;

class FormularioAsesorController extends Controller
{

    public function EnviarConsulta(Request $request){

    	if(empty($request->nombre))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El nombre es obligatorio'),500);

    	if(empty($request->email))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El email es obligatorio'),500);

    	if(empty($request->asunto))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El asunto es obligatorio'),500);

    	if(empty($request->telefono))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El teléfono es obligatorio'),500);    	    	
    	if(empty($request->mensaje))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El mensaje es obligatorio'),500);    	    	

        $c = new ConsultasAsesores;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;
        $c->asunto = $request->asunto;        
        $c->telefono = $request->telefono;
        $c->mensaje = $request->mensaje;
        $c->asesor_id = $request->asesor_id;        
        $c->save();    

        /* pasos a seguir
                -- crear campo (listo)
                -- agregar campo al modelo (listo)
                -- agreagr relacion asesor a consultas asesores (una a muchas) (listo)
            */

        //$asesor = Asesores::find($request->id);
        
       	
        Mail::send(new NuevaConsultaAsesor($c));
       
        return response()->json(array('status' => 'ok', 'consulta'=> $c),200);
    }
}
