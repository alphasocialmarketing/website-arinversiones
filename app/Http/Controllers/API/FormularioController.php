<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Mail;

use App\Models\Consultas;

use App\Mail\NuevoRegistroConsulta;
use App\User;

class FormularioController extends Controller
{
    public function consultaGuardar(Request $request){
        	
    	if(empty($request->nombre))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El nombre es obligatorio'),500);

    	if(empty($request->email))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El email es obligatorio'),500);

        $c = new Consultas;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;        
        $c->telefono = $request->telefono;        
        $c->asunto = $request->asunto;                         
        $c->mensaje = $request->mensaje;        
        $c->save();    
        
        $usuarios = User::All();
        foreach($usuarios as $u){
            if($u->hasrole("superadmin")){
                Mail::to($u->email)->send(new NuevoRegistroConsulta($c));  
            }
        }    

        return response()->json(array('status' => 'ok', 'consulta'=> $c),200);
    }
}
