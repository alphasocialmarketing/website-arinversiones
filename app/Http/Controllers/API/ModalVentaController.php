<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ConsultasVentaPropiedad;

use Mail;

use App\Mail\NuevaConsultaModalVenta;
use App\User;

class ModalVentaController extends Controller
{
    public function ConsultaEnviar(Request $request){

    	if(empty($request->nombre))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El nombre es obligatorio'),500);

    	if(empty($request->email))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El email es obligatorio'),500);

    	if(empty($request->telefono))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El teléfono es obligatorio'),500);
                	    	
    	if(empty($request->informacion))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'La información es obligatoria'),500);    	    	

        $c = new ConsultasVentaPropiedad;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;     
        $c->telefono = $request->telefono;
        $c->informacion = $request->informacion;   
        $c->save();    
        
        $usuarios = User::All();
        foreach($usuarios as $u){
            if($u->hasrole("superadmin")){
                Mail::to($u->email)->send(new NuevaConsultaModalVenta($c));  
            }
        }     

        return response()->json(array('status' => 'ok', 'consulta'=> $c),200);
    }
}
