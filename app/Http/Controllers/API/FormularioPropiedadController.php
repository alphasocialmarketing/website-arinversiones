<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Propiedades;
use App\Models\ConsultasPropiedades;

use Mail;

use App\Mail\NuevaConsultaPropiedad;
use App\User;

class FormularioPropiedadController extends Controller
{
    public function ConsultaEnviar(Request $request){

    	if(empty($request->nombre))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El nombre es obligatorio'),500);

    	if(empty($request->email))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El email es obligatorio'),500);

    	if(empty($request->asunto))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El asunto es obligatorio'),500);

    	if(empty($request->telefono))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El teléfono es obligatorio'),500);    	    	
    	if(empty($request->mensaje))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El mensaje es obligatorio'),500);    	    	

        $c = new ConsultasPropiedades;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;
        $c->asunto = $request->asunto;        
        $c->telefono = $request->telefono;
        $c->mensaje = $request->mensaje; 
        $c->propiedad_id = $request->propiedad_id;       
        $c->save();    
        
            /* pasos a seguir
                -- crear campo (migration)
                -- agregar campo al modelo
                -- agreagr relacion propiedades a consultas propiedade (una a muchas)
            */
        
        Mail::send(new NuevaConsultaPropiedad($c));

        return response()->json(array('status' => 'ok', 'consulta'=> $c),200);
    }
}

