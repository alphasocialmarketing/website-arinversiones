<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Loteos;
use App\Models\ConsultasLoteos;

use Mail;

use App\Mail\NuevaConsultaLoteo;
use App\User;

class FormularioLoteoController extends Controller
{
    public function ConsultaEnviar(Request $request){

    	if(empty($request->nombre))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El nombre es obligatorio'),500);

    	if(empty($request->email))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El email es obligatorio'),500);

    	if(empty($request->asunto))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El asunto es obligatorio'),500);

    	if(empty($request->telefono))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El teléfono es obligatorio'),500);    	    	
    	if(empty($request->mensaje))
    		return response()->json(array('status' => 'error', 'mensaje'=> 'El mensaje es obligatorio'),500);    	    	

        $c = new ConsultasLoteos;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;
        $c->asunto = $request->asunto;        
        $c->telefono = $request->telefono;
        $c->mensaje = $request->mensaje; 
        $c->loteo_id = $request->loteo_id;       
        $c->save();    
        
            /* pasos a seguir
                -- crear campo (migration)
                -- agregar campo al modelo
                -- agreagr relacion propiedades a consultas propiedade (una a muchas)
            */
        
        Mail::send(new NuevaConsultaLoteo($c));

        return response()->json(array('status' => 'ok', 'consulta'=> $c),200);
    }
}
