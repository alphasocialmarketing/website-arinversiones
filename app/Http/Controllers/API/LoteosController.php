<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\LoteosDetalle;
use App\Models\Loteos;
use App\Models\Asesores;

class LoteosController extends Controller
{
    function crearLote(Request $request){ 
        
        if($request->id)
            $lote = LoteosDetalle::find($request->id);
        else
            $lote = new LoteosDetalle;

        if(empty($request->nombre))
            return response()->json("Nombre vacio",500);

        if(empty($request->precio))
            return response()->json("Precio vacio",500);    

        if(empty($request->estado))
            return response()->json("Estado vacio",500);

        if(empty($request->loteo_id))
            return response()->json("Loteo vacio",500);

        if(empty($request->ref_x))
            return response()->json("Ref x vacio",500);
            
        if(empty($request->ref_y))
            return response()->json("Ref y vacio",500);            

        if(empty($request->asesor_id))
            return response()->json("Asesor vacio",500);

        $lote->nombre = $request->nombre;
        $lote->detalle = $request->detalle;
        $lote->ref_x = $request->ref_x;
        $lote->ref_y = $request->ref_y;
        $lote->precio = (float) $request->precio;
        $lote->estado = $request->estado;
        $lote->asesor_id = $request->asesor_id;
        $lote->loteo_id = $request->loteo_id;

        $lote->save();

        return response()->json($lote,200);
    }

    function borrarLote(Request $request){ 
        
        if($request->id){
            $lote = LoteosDetalle::find($request->id);
            $lote->delete();
        }
        else {
            return response()->json("No se seleccionó el lote",500);
        }

        return response()->json($lote,200);
    }

    function obtenerLotes(Request $request){
        $arrLotes = LoteosDetalle::where('loteo_id', $request->id)
                                    ->with('asesor')
                                    ->get();        

        return response()->json($arrLotes,200);
    }

    function obtenerAsesores(){
        $arrAsesores = Asesores::All();        

        return response()->json($arrAsesores,200);
    }

    function seleccionarLoteos($lotes=null){

        if($lotes)
            $lote = $lotes;
        else
            $lote = Loteos::where('mostrar',true)->get();

    	foreach ($lote as $key => $value) {
    		
            if(!empty($value->direccion)){
        		$arrDir = json_decode($value->direccion);
        		$lat = (float) $arrDir->latlng->lat;
        		$lng = (float) $arrDir->latlng->lng;
                $dire = $arrDir->value;
            }
            else{
                $lat = (float) str_replace(',', '.', $value->lat);
                $lng = (float) str_replace(',', '.', $value->lng);
                $dire = $value->direccion_tmp;
            } 
          
            if(!empty(json_decode($value->imagenes,true)))
                $img = json_decode($value->imagenes,true)[0]["imagen"];
            else
                $img = '';

    		$data[] = array(
                            "id"=>$value->id,
                            "title"=>$value->nombre,
    						"address"=>$dire, 
    						"latitude"=>$lat,
    						"longitude"=>$lng,
    						"image"=>asset($value->principal),
                            "description"=>$value->descripcion,
                            "intro"=>$value->intro,
                            "cerrado"=>$value->cerrado,
                            "posesion"=>$value->posesion,
                            "financiado"=>$value->financiado,
                            "price"=>$value->precio,
                            "area"=>$value->medidas,
                            "description"=>$value->descripcion,
                            //"barrio"=>$value->barrio()->first(),                            
                            "author"=>$value->asesor()->first(),
                            "type_icon"=>asset('img/building.png'),
                            "slug"=>url('loteos/'.$value->slug),
                            //"listing_for"=>'',
                            //"date"=>'',
                            //"is_featured"=>'',
                            //"balcony"=>'',
                            //"lounge"=>'',
                        );

            
    	}    	

    	return response()->json(compact("data"),200);
    }

    function seleccionarLoteosById($id){
    	$lote = Loteos::where("id",$id)->get();
        
        return $this->seleccionarLoteos($lote);
        
    }

    function filtrar(Request $request){        
        
        $l = new Loteos;
        
        $l = $l->where("mostrar",true);        

        if($request->localidad)
            $l = $l->where("localidad_id",$request->localidad);

        if($request->asesor)
            $l = $l->where("asesor_id",$request->asesor);    

        if($request->zona)
            $l = $l->where("barrio_id",$request->zona);

        if($request->cerrado)
            $l = $l->where("cerrado",$request->cerrado);

        if($request->posesion)
            $l = $l->where("posesion",$request->posesion);
        
        if($request->financiado)
            $l = $l->where("financiado",$request->financiado);
            
        return $this->seleccionarLoteos($l->get());   
    }

}
