<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Pagina;
use App\Models\Slider;
use App\Models\Timeline;
use App\Models\Testimonio;
use App\Models\Articulo;
use App\Models\CategoriaBlog;
use App\Models\Tagblog;
use App\Models\Script;

use App\Models\Categoria;
use App\Models\Clasificacion;
use App\Models\Producto;
use App\Models\Marca;
use App\Models\Cliente;
use App\Models\Envios;
use App\Models\Asesores;
use App\Models\Loteos;
use App\Models\Localidad;
use App\Models\Servicios;
use App\Models\Barrios;
use App\Models\LoteoServicios;
use App\Models\LoteosDetalle;
use App\Models\Propiedades;

use App\Pedido;

class ViewController extends Controller
{
    // -- INDEX
    public function index(Request $request){

        $data["page"] = "index";        
        $data['script'] = Script::All();        
        $data['asesores'] = Asesores::All();                                                            

        $data['slider'] = Slider::orderBy('lft','ASC')->get();                    

        return view('web.index',compact('data'));
    }

    
    //#################################################################################
    //#################################################################################

    public function asesores(){     

        $data["page"] = "asesores";
        $data['script'] = Script::All();        
        $data['asesores'] = Asesores::All();                              

        return view('web.asesores.asesores',compact('data'));
    }

    public function asesoresDetalle($slug){
        
        $data["page"] = "asesoresDetalle";
        $data['script'] = Script::All();                        
        $data['asesores'] = Asesores::where('slug',$slug)->first();            
        $data['prop_rel'] = $data['asesores']->propiedades()->get();        
        $data['relacionados'] = Propiedades::inRandomOrder()->paginate(3);
        $data['total_propiedades'] = count(Propiedades::All());
        $data['total_loteos'] = count(Loteos::All());
        $data['propiedades'] = Propiedades::All();

        return view('web.asesores.asesoresDetalle',compact('data'));
    } 

    
    public function propiedades(){     

        $data["page"] = "propiedades";
        $data['script'] = Script::All();
        $data['propiedades'] = Propiedades::All();
        $data['barrios'] = Barrios::All();
        $data['asesores'] = Asesores::All();
        $data['localidades'] = Localidad::All();
        
        return view('web.page.buscador',compact('data'));
    }    
    public function propiedadDetalle($slug){     

        $data["page"] = "propiedades_detalle";
        $data['script'] = Script::All();
        $data['propiedad'] = Propiedades::where('slug',$slug)->first();
        $data['ase_rel'] = $data['propiedad']->asesor()->get();
        $data['relacionados'] = Propiedades::where('habitaciones',$data['propiedad']->habitaciones)
                                            ->Where('id','<>',$data['propiedad']->id)
                                            ->inRandomOrder()
                                            ->take(3)
                                            ->get();

                        
        return view('web.page.propiedadDetalle',compact('data'));
    }
    
    public function loteos(){     

        $data["page"] = "loteos";
        $data['script'] = Script::All();
        $data['loteos'] = Loteos::All();
        $data['barrios'] = Barrios::All();
        $data['asesores'] = Asesores::All();
        $data['localidades'] = Localidad::All();        

        return view('web.page.buscadorLoteo',compact('data'));
    }
    public function loteoDetalle($slug){     

        $data["page"] = "loteos_detalle";
        $data['script'] = Script::All();
        $data['loteo'] = Loteos::where('slug',$slug)->first();
        $data['ase_rel'] = $data['loteo']->asesor()->get();
        $data['relacionados'] = Loteos::where('cerrado',$data['loteo']->cerrado)
                                        ->Where('id','<>',$data['loteo']->id)
                                        ->inRandomOrder()
                                        ->take(3)
                                        ->get();        
                                            
        $data['lot_rel'] = $data['loteo']->lotes()->get();
        $data['lotes_vendidos'] = $data['loteo']->lotes()->where('estado','VENDIDO')->get()->count();
        $data['lotes_reservados'] = $data['loteo']->lotes()->where('estado','RESERVADO')->get()->count();
        $data['lotes_disponibles'] = $data['loteo']->lotes()->where('estado','DISPONIBLE')->get()->count();

        return view('web.page.loteoDetalle',compact('data'));       
    }

    public function imprimir($slug){     

        $data["page"] = "imprimir";
        $data['script'] = Script::All();
        $data['propiedad'] = Propiedades::where('slug',$slug)->first();
        $data['ase_rel'] = $data['propiedad']->asesor()->get();
        $data['relacionados'] = Propiedades::inRandomOrder()->paginate(3);        
                        
        return view('web.page.imprimir',compact('data'));
    }
    

    //#################################################################################
    //#################################################################################

    // -- BLOG
    public function blog(){

        $data["page"] = "blog";
        $data['script'] = Script::All();                     

        $data['blogs'] = Articulo::orderBy('fecha','desc')->paginate(8);
        $data['referencia'] = 'TODOS';
 
        return view('web.blog.blog',compact('data'));
    }
    public function blogByCategoria($categoria){

        $data["page"] = "blog";
        $data['script'] = Script::All();

        $data['blogs'] = CategoriaBlog::where('slug',$categoria)->first()
                                                                ->articulos()
                                                                ->orderBy('fecha','desc')
                                                                ->paginate(8);
        $data['referencia'] = $categoria;
 
        return view('web.blog.blog',compact('data'));   
    }        
    public function blogByTag($tag){

        $data["page"] = "blog";
        $data['script'] = Script::All();

        $data['blogs'] = Tagblog::where('slug',$tag)->first()
                                                    ->articulos()
                                                    ->orderBy('fecha','desc')
                                                    ->paginate(8);
        $data['referencia'] = $tag;
 
        return view('web.blog.blog',compact('data'));   
    }       
    public function blogDetalle($slug){

        $data["page"] = "blogDetalle";
        $data['script'] = Script::All();

        $data['blog'] = Articulo::where('slug',$slug)->first();
        $data['categorias'] = CategoriaBlog::All();
        $data['tags'] = Tagblog::All();

        $data['ultimos'] = Articulo::orderBy('fecha','desc')->paginate(4);


        return view('web.blog.blogDetalle',compact('data'));
    }   


}
