<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;

class UserCrudController extends CrudController
{

    public function setUp()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/perfil');
        $this->crud->setEntityNameStrings('Usuario', 'Usuarios');

        
        $field[0] = ['label' => 'Direccion','type' => 'address', 'name' => 'direccion', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] ];        
        //$field[1] = ['label' => 'Localidad','type' => 'text', 'name' => 'localidad' ];                
        $field[1] = ['label' => 'Empresa','type' => 'text', 'name' => 'empresa', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] ];        
        $field[2] = ['label' => 'CUIT','type' => 'text', 'name' => 'cuit', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] ];        
        $field[3] = ['label' => 'Web','type' => 'url', 'name' => 'web', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] ];            
        $field[4] = ['label' => 'Telefono','type' => 'text', 'name' => 'telefono', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] ];        
        $field[5] = ['label' => 'Celular','type' => 'text', 'name' => 'celular', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] ];     

        $this->crud->addFields($field, 'update');

        $this->crud->allowAccess(['update']);
        $this->crud->denyAccess(['list', 'create', 'reorder', 'delete']);  

    }

    public function store(StoreRequest $request)
    {
        
        //$redirect_location = parent::storeCrud();
        
        //return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {

        if(\Auth::user()->id!=$request->id){
            \Alert::error('No tiene permisos para editar este perfil.')->flash();

            return redirect('admin/perfil/'.\Auth::user()->id.'/edit');
        }  
        
        $redirect_location = parent::updateCrud();

        return redirect('admin/perfil/'.\Auth::user()->id.'/edit');
    }
}
