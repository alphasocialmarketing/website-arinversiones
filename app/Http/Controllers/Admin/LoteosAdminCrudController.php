<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LoteosAdminRequest as StoreRequest;
use App\Http\Requests\LoteosAdminRequest as UpdateRequest;

class LoteosAdminCrudController extends CrudController
{
    public function __construct() {        
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Loteos");
        $this->crud->setRoute("admin/loteos-admin");
        $this->crud->setEntityNameStrings('Loteo', 'Loteos');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */               
        $field[0] = ['label' => 'Mostrar','type' => 'checkbox', 'name' => 'mostrar', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];
        $field[1] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];        
        $field[2] = ['label' => 'Slug (URL)', 'type' => 'text', 'name' => 'slug', 'hint' => 'Se generará automáticamente a partir del nombre, si se deja vacío.','wrapperAttributes' => ['class' => 'form-group col-md-12'] , 'tab' => 'General'  ];
        $field[3] = ['label' => 'Imagen Principal','type' => 'browse', 'name' => 'principal', 'wrapperAttributes' => ['class' => 'form-group col-md-12'] , 'tab' => 'General' ];        
        $field[4] = ['label' => 'Intro', 'type' => 'text', 'name' => 'intro', 'wrapperAttributes' => ['class' => 'form-group col-md-12'] , 'tab' => 'General' ];
        $field[5] = ['label' => 'Descripcion', 'type' => 'ckeditor', 'name' => 'descripcion', 'wrapperAttributes' => ['class' => 'form-group col-md-12'] , 'tab' => 'General'];

        $field[6] = ['label' => 'Cerrado','type' => 'checkbox', 'name' => 'cerrado', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] , 'tab' => 'Adiccional' ];
        $field[7] = ['label' => 'Posesión','type' => 'checkbox', 'name' => 'posesion', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] , 'tab' => 'Adiccional' ];
        $field[8] = ['label' => 'Financiado','type' => 'checkbox', 'name' => 'financiado', 'wrapperAttributes' => ['class' => 'form-group col-md-4'] , 'tab' => 'Adiccional' ];                    
        $field[9] = [  'name' => 'imagenes',
                        'label' => 'Imagenes',
                        'type' => 'browse_multiple',
                        'entity_singular' => 'Imagen', 
                        'columns' => [
                            'imagen' => 'Imagen',        
                        ],
                        'max' => 50, 
                        'min' => 0,
                        'wrapperAttributes' => ['class' => 'form-group col-md-12'],
                        'tab' => 'Adiccional' 
                    ];
                    
        $field[10] = ['label' => 'Dirección', 'type' => 'address_google', 'name' => 'direccion', 'store_as_json' => true, 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'Ubicación' ];
        $field[11] = ['label' => 'Dirección Temporal','type' => 'text', 'name' => 'direccion_tmp', 'wrapperAttributes' => ['class' => 'form-group col-md-6'], 'tab' => 'Ubicación' ];
        $field[12] = ['label' => 'Latitud Temporal','type' => 'text', 'name' => 'lat', 'wrapperAttributes' => ['class' => 'form-group col-md-3'], 'tab' => 'Ubicación' ];
        $field[13] = ['label' => 'Longitud Temporal','type' => 'text', 'name' => 'lng', 'wrapperAttributes' => ['class' => 'form-group col-md-3'], 'tab' => 'Ubicación' ];        
                       
        $field[14] = ['label' => "Localidad", 'type' => 'select2', 'name' => 'localidad_id', 'entity' => 'localidades', 'attribute' => 'nombre', 'model' => "App\Models\Localidad", 'wrapperAttributes' => ['class' => 'form-group col-md-6'] , 'tab' => 'Relaciones'];            
        $field[15] = ['label' => "Asesor", 'type' => 'select2', 'name' => 'asesor_id', 'entity' => 'asesores', 'attribute' => 'nombre', 'model' => "App\Models\Asesores", 'wrapperAttributes' => ['class' => 'form-group col-md-6'] , 'tab' => 'Relaciones'];
        //$field[14] = ['type'=>'hidden', 'name'=>'asesor_id', 'value'=>'0', 'tab' => 'Relaciones'];
        $field[16] = ['label' => "Servicios", 'type' => 'select2_multiple', 'name' => 'servicios', 'entity' => 'servicios', 'attribute' => 'nombre', 'model' => "App\Models\Servicios", 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'pivot' => true , 'tab' => 'Relaciones'];

        $field[17] = ['label' => "Plano de Mensura", 'name' => "plano_mensura", 'type' => 'image', 'upload' => true, 'aspect_ratio' => 1, 'prefix' => 'storage/', 'tab' => 'Plano de Mensura'];
       
        $this->crud->addFields($field, 'update/create/both');       

        $col[0] = ['name' => 'mostrar', 'label' => "Mostrar", 'type' => 'check'];        
        $col[1] = ['label' => "Asesor", 'type' => "select", 'name' => 'asesor_id', 'entity' => 'Asesor', 'attribute' => "nombre", 'model' => "App\Models\Asesor"];
        $col[2] = ['name' => "principal", 'label' => "Imagen", 'type' => "model_function", 'function_name' => 'getImagenPreview'];
        $col[3] = ['name' => 'nombre', 'label' => "Nombre"];        
        $col[4] = ['label' => "Dirección", 'type' => "model_function", 'function_name' => 'getDireccionByJson'];                
        $col[5] = ['name' => "lotes", 'label' => "Lotes", 'type' => "model_function", 'function_name' => 'getLotes',];
        $col[6] = ['name' => "cant_vendidos", 'label' => "Vendidos", 'type' => "model_function", 'function_name' => 'getCountLotesVendidos',];
        $col[7] = ['name' => "cant_reservados", 'label' => "Reservados", 'type' => "model_function", 'function_name' => 'getCountLotesReservados',];
        $col[8] = ['name' => "cant_disponibles", 'label' => "Disponibles", 'type' => "model_function", 'function_name' => 'getCountLotesDisponibles',];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);                 
    }


    public function index(){
        $u = \Auth::user();
        
        if($u->hasRole('vendedor')){            
            if(!empty($u->asesor)){
                if(count($u->asesor)>1)
                    return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 

                $this->crud->addClause('where', 'asesor_id', '=', $u->asesor->first()->id);                    
            }
        }
        
        return parent::index();
    }

    public function store(StoreRequest $request)
    {
        $u = \Auth::user();

        if($u->hasRole('vendedor')){            
            if(!empty($u->asesor)){
                if(count($u->asesor)>1)
                    return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 

                $asesor = (string) $u->asesor->first()->id;        
                $request->merge( array( 'asesor_id' => $asesor ) ); 
            }
        }
        
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {   
        $u = \Auth::user();

        if($u->hasRole('vendedor')){            
            if(!empty($u->asesor)){
                if(count($u->asesor)>1)
                    return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 

                $asesor = (string) $u->asesor->first()->id;        
                $request->merge( array( 'asesor_id' => $asesor ) ); 
            }
        }
        
        return parent::updateCrud($request);
    }
}
