<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LocalidadRequest as StoreRequest;
use App\Http\Requests\LocalidadRequest as UpdateRequest;

class LocalidadCrudController extends CrudController
{
    public function __construct() {        
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Localidad");
        $this->crud->setRoute("admin/localidades");
        $this->crud->setEntityNameStrings('Localidad', 'Localidades');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */               
        $field[0] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'wrapperAttributes' => ['class' => 'form-group col-md-12'] ];        
       

        $this->crud->addFields($field, 'update/create/both');       
        
        $col[0] = ['name' => 'nombre', 'label' => "Nombre"];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
                
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
