<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BarriosRequest as StoreRequest;
use App\Http\Requests\BarriosRequest as UpdateRequest;

class BarriosCrudController extends CrudController
{
    public function __construct() {        
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Barrios");
        $this->crud->setRoute("admin/barrios");
        $this->crud->setEntityNameStrings('Barrio', 'Barrios');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */               
        $field[0] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];        
        $field[1] = ['label' => 'Zona', 'type' => 'text','name' => 'zona', 'wrapperAttributes' => ['class' => 'form-group col-md-6']];
        $field[2] = ['label' => "Localidad", 'type' => 'select2', 'name' => 'localidad_id', 'entity' => 'localidades', 'attribute' => 'nombre', 'model' => "App\Models\Localidad", 'wrapperAttributes' => ['class' => 'form-group col-md-12']];
       

        $this->crud->addFields($field, 'update/create/both');       
                
        $col[0] = ['name' => 'nombre', 'label' => "Nombre"];
        $col[1] = ['name' => 'zona', 'label' => "Zona"];
        $col[2] = ['label' => "Localidad", 'type' => "select", 'name' => 'localidad_id', 'entity' => 'localidad', 'attribute' => "nombre", 'model' => "App\Models\Localidad"];
      
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);

    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
