<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AsesoresRequest as StoreRequest;
use App\Http\Requests\AsesoresRequest as UpdateRequest;

class AsesoresCrudController extends CrudController
{
    public function __construct() {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Asesores");
        $this->crud->setRoute("admin/asesores");
        $this->crud->setEntityNameStrings('Asesor', 'Asesores');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $field[0] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'wrapperAttributes' => ['class' => 'form-group col-md-9'], 'tab' => 'General'  ];
        $field[1] = ['label' => 'Destacado','type' => 'checkbox', 'name' => 'destacado', 'wrapperAttributes' => ['class' => 'form-group col-md-3'], 'tab' => 'General' ];
        $field[2] = ['name' => 'slug', 'label' => 'Slug (URL)', 'type' => 'text', 'hint' => 'Se generará automáticamente a partir del nombre, si se deja vacío.','wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General'  ];                                           
        $field[3] = ['label' => 'Puesto','type' => 'text', 'name' => 'puesto', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'General' ];                        
        $field[4] = ['label' => 'Profesion','type' => 'text', 'name' => 'profesion', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'General'  ]; 
        $field[5] = ['label' => 'Imagen', 'type' => 'browse','name' => 'imagen', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'General'];        
        $field[6] = ['label' => 'Biografia','type' => 'ckeditor', 'name' => 'biografia', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];
        $field[7] = ['label' => 'Información','type' => 'ckeditor', 'name' => 'informacion', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];        
        $field[8] = ['label' => 'Email','type' => 'email', 'name' => 'email', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Contacto'  ];
        $field[9] = ['label' => 'Celular','type' => 'text', 'name' => 'celular', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Contacto'  ];
        $field[10] = ['label' => 'Skype','type' => 'text', 'name' => 'skype', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Contacto'  ];                   
        $field[11] = ['label' => 'Facebook','type' => 'url', 'name' => 'facebook', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Contacto'  ];
        $field[12] = ['label' => 'Twitter','type' => 'url', 'name' => 'twitter', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Contacto'  ];
        $field[13] = ['label' => 'Instagram','type' => 'url', 'name' => 'instagram', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Contacto'  ];

        $field[14] = ['label' => "Usuarios", 'type' => 'select2', 'name' => 'user_id', 'entity' => 'usuario', 'attribute' => 'name', 'model' => "App\User", 'wrapperAttributes' => ['class' => 'form-group col-md-6'], 'tab' => 'Usuario'];
        
        $this->crud->addFields($field, 'update/create/both');       
                
        $col[0] = ['name' => 'nombre', 'label' => "Nombre"];
        $col[1] = ['name' => 'puesto', 'label' => "Puesto"];                        
        $col[2] = ['name' => 'email', 'label' => "Email"];
        $col[3] = ['name' => 'celular', 'label' => "Celular"];
        $col[4] = ['name' => "user_id", 'label' => "Usuario", 'type' => "model_function", 'function_name' => 'getUsuario',];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);

        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('nombre', 1);      
        
        $this->crud->orderBy('lft','ASC');           
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
