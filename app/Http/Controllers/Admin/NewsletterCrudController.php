<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\NewsletterRequest as StoreRequest;
use App\Http\Requests\NewsletterRequest as UpdateRequest;

use App\Models\Newsletter;
use App\Mail\NuevoRegistroNewsletter;
use Mail;

class NewsletterCrudController extends CrudController
{
    public function __construct() {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Newsletter");
        $this->crud->setRoute("admin/newsletter");
        $this->crud->setEntityNameStrings('Telefono a contactar', 'Telefonos a contactar');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        
        $field[0] = ['label' => 'Fecha','type' => 'date', 'name' => 'created_at', 'attributes'=>["disabled"=>"true"] ];
        $field[1] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'attributes'=>["disabled"=>"true"]];
        $field[2] = ['label' => 'Codigo Area','type' => 'text', 'name' => 'codarea', 'attributes'=>["disabled"=>"true"]];                
        $field[3] = ['label' => 'Telefono','type' => 'email', 'name' => 'email', 'attributes'=>["disabled"=>"true"]];                

        $this->crud->addFields($field, 'update');       

        $col[0] = ['name' => 'created_at', 'label' => "Fecha"];
        $col[1] = ['name' => 'nombre', 'label' => "Nombre"];
        $col[2] = ['name' => 'codarea', 'label' => "Codigo Area"];
        $col[3] = ['name' => 'telefono', 'label' => "Telefono"];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list','update', 'delete']);
        $this->crud->denyAccess(['create', 'reorder']);  

        $this->crud->enableExportButtons();            

        $this->crud->orderBy('created_at','DESC');
    }

    public function store(StoreRequest $request)
    {
        $result = parent::storeCrud();
        return $result;
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
