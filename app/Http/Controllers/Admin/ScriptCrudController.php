<?php 

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ScriptRequest as StoreRequest;
use App\Http\Requests\ScriptRequest as UpdateRequest;

class ScriptCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Script");
        $this->crud->setRoute("admin/script");
        $this->crud->setEntityNameStrings('Script', 'Scripts');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$field[0] = ['label' => 'Referencia', 'type' => 'text','name' => 'referencia'];        
        $field[1] = ['label' => 'Script', 'type' => 'textarea','name' => 'script', 'attributes' => ["rows" => "10"]];        
        $field[2] = ['label' => 'Página', 'type' => 'text','name' => 'page']; // ejecutar url con parametro ?nombrePagina para ver el nombre de la pagina

		$this->crud->addFields($field, 'update/create/both');       
            
        $col[0] = ['name' => 'referencia', 'label' => "Referencia"];        
        $col[1] = ['name' => 'page', 'label' => "Página"];        
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
