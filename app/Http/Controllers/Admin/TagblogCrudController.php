<?php 

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TagblogRequest as StoreRequest;
use App\Http\Requests\TagblogRequest as UpdateRequest;

class TagblogCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Tagblog");
        $this->crud->setRoute("admin/tagblog");
        $this->crud->setEntityNameStrings('Tag para Blog', 'Tags para Blog');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $field[0] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre' ];                        

        $this->crud->addFields($field, 'update/create/both');       

        $col[0] = ['name' => 'nombre', 'label' => "Nombre"];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
