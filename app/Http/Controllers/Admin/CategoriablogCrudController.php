<?php 

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoriablogRequest as StoreRequest;
use App\Http\Requests\CategoriablogRequest as UpdateRequest;

class CategoriablogCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Categoriablog");
        $this->crud->setRoute("admin/blog-categoria");
        $this->crud->setEntityNameStrings('Categoría para Blog', 'Categorías para Blog');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        
        $field[0] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre' ];                

        $this->crud->addFields($field, 'update/create/both');       

        $col[0] = ['name' => 'nombre', 'label' => "Nombre"];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
