<?php 

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SliderRequest as StoreRequest;
use App\Http\Requests\SliderRequest as UpdateRequest;

class SliderCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Slider");
        $this->crud->setRoute("admin/slider");
        $this->crud->setEntityNameStrings('Slider', 'Sliders');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $field[0] = ['label' => 'Título','type' => 'text', 'name' => 'titulo', 'wrapperAttributes' => ['class' => 'form-group col-md-12'] ];                
        $field[1] = ['label' => 'Bajada','type' => 'text', 'name' => 'detalle', 'wrapperAttributes' => ['class' => 'form-group col-md-12'] ];                
        $field[2] = ['label' => 'Imagen', 'type' => 'browse','name' => 'fondo', 'wrapperAttributes' => ['class' => 'form-group col-md-6']];
        //$field[3] = ['label' => 'Imagen Mobile', 'type' => 'browse','name' => 'fondo_mobile', 'wrapperAttributes' => ['class' => 'form-group col-md-6']];                
        $field[3] = ['label' => 'Botón','type' => 'text', 'name' => 'boton', 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];        
        $field[4] = ['label' => 'Link','type' => 'text', 'name' => 'link', 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];                   
        //$field[5] = ['label' => 'Alineación Texto', 'type' => 'select_from_array', 'name' => 'orientacion', 'options' => ['IZQUIERDA' => 'IZQUIERDA', 'CENTRO' => 'CENTRO', 'DERECHA' => 'DERECHA'], 'allows_null' => false];

        $this->crud->addFields($field, 'update/create/both');       
            
        $col[0] = ['name' => 'titulo', 'label' => "Titulo"];        
        $col[1] = ['name' => "fondo", 'label' => "Imagen", 'type' => "model_function", 'function_name' => 'getImagenPreview'];
        //$col[2] = ['name' => "fondo_mobile", 'label' => "Imagen Mobile", 'type' => "model_function", 'function_name' => 'getImagenMobilePreview'];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);

        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('titulo', 1);      
        
        $this->crud->orderBy('lft','ASC');           
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
