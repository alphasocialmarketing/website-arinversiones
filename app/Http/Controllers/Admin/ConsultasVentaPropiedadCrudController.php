<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ConsultasVentaPropiedadRequest as StoreRequest;
use App\Http\Requests\ConsultasVentaPropiedadRequest as UpdateRequest;

use App\Models\ConsultasVentaPropiedad;
use App\Mail\NuevaConsultaModalVenta;
use Mail;

class ConsultasVentaPropiedadCrudController extends CrudController
{
    public function __construct() {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\ConsultasVentaPropiedad");
        $this->crud->setRoute("admin/consultas-venta-propiedad");
        $this->crud->setEntityNameStrings('Consulta Venta Propiedad', 'Consultas Venta Propiedad');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $field[0] = ['label' => 'Fecha','type' => 'date', 'name' => 'created_at', 'attributes'=>["disabled"=>"true"], 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];                
        $field[1] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'attributes'=>["disabled"=>"true"], 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];          
        $field[2] = ['label' => 'Email','type' => 'email', 'name' => 'email', 'attributes'=>["disabled"=>"true"], 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];
        $field[3] = ['label' => 'Telefono','type' => 'text', 'name' => 'telefono', 'attributes'=>["disabled"=>"true"], 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];                
        $field[4] = ['label' => 'Información sobre propiedad','type' => 'ckeditor', 'name' => 'informacion', 'attributes'=>["disabled"=>"true"] ];               
    
        $this->crud->addFields($field, 'update');       
        
        $col[0] = ['name' => 'created_at', 'label' => "Fecha",'sort' => 'desc'];
        $col[1] = ['name' => 'nombre', 'label' => "Nombre"];
        $col[2] = ['name' => 'email', 'label' => "Email"];
        $col[3] = ['name' => 'telefono', 'label' => "Telefono"];
        $col[4] = ['name' => 'informacion', 'label' => "Información Propiedad"];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list','update']);
        $this->crud->denyAccess(['create', 'reorder', 'delete']);  

        $this->crud->enableExportButtons();    

        $this->crud->orderBy('created_at','DESC');
        
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
