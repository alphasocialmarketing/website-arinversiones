<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PropiedadesRequest as StoreRequest;
use App\Http\Requests\PropiedadesRequest as UpdateRequest;

class PropiedadesCrudController extends CrudController
{
    public function __construct() {        
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Propiedades");
        $this->crud->setRoute("admin/propiedades");
        $this->crud->setEntityNameStrings('Propiedad', 'Propiedades');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */               
        $field[0] = ['label' => 'Mostrar','type' => 'checkbox', 'name' => 'mostrar', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];
        $field[1] = ['label' => 'Nombre','type' => 'text', 'name' => 'nombre', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];        
        $field[2] = ['label' => 'Slug (URL)', 'type' => 'text', 'name' => 'slug', 'hint' => 'Se generará automáticamente a partir del nombre, si se deja vacío.','wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General'  ];
        $field[3] = ['label' => 'Imagen Principal','type' => 'browse', 'name' => 'principal', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];        
        $field[4] = ['label' => 'Intro', 'type' => 'text', 'name' => 'intro', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];        
        $field[5] = ['label' => 'Descripción', 'type' => 'ckeditor', 'name' => 'descripcion', 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'General' ];                    

        $field[6] = [  'name' => 'imagenes',
                        'label' => 'Imagenes',
                        'type' => 'browse_multiple',
                        'entity_singular' => 'Imagen', 
                        'columns' => [
                            'imagen' => 'Imagen',        
                        ],
                        'max' => 50, 
                        'min' => 0,
                        'wrapperAttributes' => ['class' => 'form-group col-md-12'], 
                        'tab' => 'Addicional'
                    ];
        $field[7] = ['label' => 'Precio', 'type' => 'number', 'name' => 'precio', 'attributes' => ["step" => "0.01"], 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Precio' ];    
        $field[8] = ['label' => 'Moneda', 'type' => 'select_from_array', 'name' => 'moneda', 'options' => ['PESO' => 'PESO', 'DOLAR' => 'DOLAR'], 'allows_null' => false, 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Precio'];
        $field[9] = ['label' => 'Ocultar Precio','type' => 'checkbox', 'name' => 'ocultar_precio', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Precio' ];                    
        $field[10] = ['label' => 'Habitaciones', 'type' => 'text', 'name' => 'habitaciones', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Addicional'];
        $field[11] = ['label' => 'Baños', 'type' => 'text', 'name' => 'banios', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Addicional'];
        $field[12] = ['label' => 'Medidas (M2)', 'type' => 'number', 'name' => 'medidas', 'attributes' => ["step" => "0.01"], 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Addicional'];        
        $field[13] = ['label' => 'Cochera','type' => 'checkbox', 'name' => 'cochera', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Addicional' ];
        $field[14] = ['label' => 'Terminada','type' => 'checkbox', 'name' => 'terminada', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Addicional' ];
        $field[15] = ['label' => 'Gas','type' => 'checkbox', 'name' => 'gas', 'wrapperAttributes' => ['class' => 'form-group col-md-4'], 'tab' => 'Addicional' ];        

        $field[16] = ['label' => 'Dirección', 'type' => 'address_google', 'name' => 'direccion', 'store_as_json' => true, 'wrapperAttributes' => ['class' => 'form-group col-md-12'], 'tab' => 'Ubicación'];
        $field[17] = ['label' => 'Dirección Temporal','type' => 'text', 'name' => 'direccion_tmp', 'wrapperAttributes' => ['class' => 'form-group col-md-6'], 'tab' => 'Ubicación' ];
        $field[18] = ['label' => 'Latitud Temporal','type' => 'text', 'name' => 'lat', 'wrapperAttributes' => ['class' => 'form-group col-md-3'], 'tab' => 'Ubicación' ];
        $field[19] = ['label' => 'Longitud Temporal','type' => 'text', 'name' => 'lng', 'wrapperAttributes' => ['class' => 'form-group col-md-3'], 'tab' => 'Ubicación' ];        
                
        $field[20] = ['label' => "Barrio", 'type' => 'select2', 'name' => 'barrio_id', 'entity' => 'propiedades', 'attribute' => 'nombre', 'model' => "App\Models\Barrios", 'wrapperAttributes' => ['class' => 'form-group col-md-6'], 'tab' => 'Relaciones'];
        //$field[18] = ['label' => "Asesor", 'type' => 'select2', 'name' => 'asesor_id', 'entity' => 'asesores', 'attribute' => 'nombre', 'model' => "App\Models\Asesores", 'wrapperAttributes' => ['class' => 'form-group col-md-6'], 'tab' => 'Relaciones'];        
        $field[21] = ['type'=>'hidden', 'name'=>'asesor_id', 'value'=>'0', 'tab' => 'Relaciones'];

        $this->crud->addFields($field, 'update/create/both');       
                    
        $col[0] = ['name' => 'mostrar', 'label' => "Mostrar", 'type' => 'check'];        
        $col[1] = ['label' => "Asesor", 'type' => "select", 'name' => 'asesor_id', 'entity' => 'Asesor', 'attribute' => "nombre", 'model' => "App\Models\Asesor"];
        $col[2] = ['name' => "principal", 'label' => "Imagen", 'type' => "model_function", 'function_name' => 'getImagenPreview'];
        $col[3] = ['name' => 'nombre', 'label' => "Nombre"];        
        $col[4] = ['label' => "Dirección", 'type' => "model_function", 'function_name' => 'getDireccionByJson'];
        $col[5] = ['name' => 'precio', 'label' => "Precio"];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
                 
    }

    public function index(){
        $u = \Auth::user();
        
        if($u->hasRole('vendedor')){            
            if(!empty($u->asesor)){
                if(count($u->asesor)>1)
                    return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 

                $this->crud->addClause('where', 'asesor_id', '=', $u->asesor->first()->id);                    
            }
        }
        
        return parent::index();
    }

    public function store(StoreRequest $request)
    {
        $u = \Auth::user();

        if($u->hasRole('vendedor')){            
            if(!empty($u->asesor)){
                if(count($u->asesor)>1)
                    return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 

                $asesor = (string) $u->asesor->first()->id;        
                $request->merge( array( 'asesor_id' => $asesor ) ); 
            }
        }
        
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {   
        $u = \Auth::user();

        if($u->hasRole('vendedor')){            
            if(!empty($u->asesor)){
                if(count($u->asesor)>1)
                    return redirect()->back()->withErrors('El Usuario tiene Asosiado mas de un Asesor'); 

                $asesor = (string) $u->asesor->first()->id;        
                $request->merge( array( 'asesor_id' => $asesor ) ); 
            }
        }
        
        return parent::updateCrud($request);
    }
}
