<?php 

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArticuloRequest as StoreRequest;
use App\Http\Requests\ArticuloRequest as UpdateRequest;

class ArticuloCrudController extends CrudController {

	public function __construct() {        
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Articulo");
        $this->crud->setRoute("admin/articulo");
        $this->crud->setEntityNameStrings('Artículo', 'Artículos');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/               
        $field[0] = ['label' => "Categoria", 'type' => 'select2', 'name' => 'categoria_blog_id', 'entity' => 'categorias_blog', 'attribute' => 'nombre', 'model' => "App\Models\Categoriablog", 'wrapperAttributes' => ['class' => 'form-group col-md-6']];
        $field[1] = ['label' => 'Fecha', 'type' => 'date', 'name' => 'fecha', 'value' => date('Y-m-d'), 'wrapperAttributes' => ['class' => 'form-group col-md-6']];
        $field[2] = ['label' => 'Titulo','type' => 'text', 'name' => 'titulo', 'wrapperAttributes' => ['class' => 'form-group col-md-6'] ];        
        $field[3] = ['label' => 'Imagen', 'type' => 'browse','name' => 'imagen', 'wrapperAttributes' => ['class' => 'form-group col-md-6']];
        $field[4] = ['label' => 'Intro', 'type' => 'text', 'name' => 'intro'];
        $field[5] = ['label' => "Etiquetas", 'type' => 'select2_multiple', 'name' => 'tags', 'entity' => 'tagblog', 'attribute' => 'nombre', 'model' => "App\Models\Tagblog", 'pivot' => true];        
        $field[6] = ['label' => 'Contenido', 'type' => 'ckeditor', 'name' => 'contenido', 'placeholder' => 'Ingresar el contenido'];        
        $field[7] = ['type'=>'hidden', 'name'=>'usuario_id', 'value'=>'0'];

        $this->crud->addFields($field, 'update/create/both');       
        
        $col[0] = ['label' => "Categoria", 'type' => "select", 'name' => 'categoria_id', 'entity' => 'Categoria', 'attribute' => "nombre", 'model' => "App\Models\Categoria"];
        $col[1] = ['name' => 'fecha', 'label' => "Fecha"];
        $col[2] = ['name' => 'titulo', 'label' => "Titulo"];
        $col[3] = ['label' => "Autor", 'type' => "model_function", 'function_name' => 'getNombreAutor'];
        
        $this->crud->addColumns($col);        

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);        

        $this->crud->orderBy('fecha','DESC');
    }

    public function index(){
        $u = \Auth::user();
        
        if($u->hasRole('usuario'))
            $this->crud->addClause('where', 'usuario_id', '=', $u->id);                
        
        return parent::index();
    }

	public function store(StoreRequest $request)
	{        

        $usuario = (string) \Auth::user()->id;        
        $request->merge( array( 'usuario_id' => $usuario ) );       

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
