<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConsultasRequestCustom;

use App\Http\Requests;

use App\Models\Newsletter;
use App\Models\Consultas;
use App\Models\ConsultasPrecios;

use App\User;

use App\Notifications\NuevoRegistroNewsletter;
use App\Notifications\NuevoRegistroConsulta;
use App\Notifications\NuevoRegistroConsultaPrecios;

class FormularioController extends Controller
{
    public function newsletterGuardar(Request $request){
    
        $n = new Newsletter;
        
        $n->nombre = $request->nombre;
        $n->codarea = $request->codarea;
        $n->telefono = $request->telefono;        
        $n->save();    

        $usuarios = User::All();
        foreach($usuarios as $u){
            if($u->hasrole("superadmin")){
                $u->notify(new NuevoRegistroNewsletter());
            }
        }

        return redirect()->back()                        
                        ->withSuccess('El telefono fue guardado con éxito');
    }

    public function consultaGuardar(ConsultasRequestCustom $request){
    
        $c = new Consultas;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;        
        $c->mensaje = $request->mensaje;        
        $c->save();    

        $consulta = Consultas::All()->last();

        $usuarios = User::All();
        foreach($usuarios as $u){
            if($u->hasrole("superadmin")){
                $u->notify(new NuevoRegistroConsulta($consulta));                
            }
        }

        return redirect()->back()                        
                        ->withSuccess('La consulta fue enviada con éxito');

    }

    public function consultaPreciosGuardar(Request $request){
    
        $c = new ConsultasPrecios;
        
        $c->nombre = $request->nombre;        
        $c->email = $request->email;     
        $c->asunto = $request->asunto;   
        $c->mensaje = $request->mensaje;        
        $c->save();    

        $usuarios = User::All();
        foreach($usuarios as $u){
            if($u->hasrole("superadmin")){
                $u->notify(new NuevoRegistroConsultaPrecios());
            }
        }

        return redirect()->back()                        
                        ->withSuccess('La consulta fue enviada con éxito. Puede continuar con su compra.');

    }

}
