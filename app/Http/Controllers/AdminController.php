<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use App\Models\Script;
use App\Models\Loteos;

class AdminController extends Controller
{

    public function redirect(){        
        return redirect()->back();            
    }

    public function viewLogin(){
        
        if(!empty(Auth::user())){
            return redirect('admin/dashboard');
        }

        $this->data['title'] = trans('backpack::base.login');

        return view('backpack::auth.login', $this->data);  
    }

    public function viewDashboard()
    {   
        $u = \Auth::user();
           
        if($u->isVendedor()){            
            if(empty($u->asesor->count()))
                return redirect()->route('logout');                                        
        }
        
        $data["page"] = "dashboard";
        $data['script'] = Script::All();

        return view('admin.panelcontrol.dashboard', compact('data'));
    }   

    public function viewLoteosPlanoMensura(Request $request){
        $data["page"] = "Plano de Mensura";
        $data['script'] = Script::All();

        $data["loteo"] = Loteos::find($request->id);

        return view('admin.loteos.plano-mensura', compact('data'));
    }
}
