<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPasswordNotification extends ResetPassword
{
    /**
     * Build the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Restablecer contraseña')
            ->line([
                'Recibirá este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.',
                'Haga clic en el botón de abajo para restablecer su contraseña:',
            ])
            ->action('Restablecer la contraseña', url(config('backpack.base.route_prefix').'/password/reset', $this->token))
            ->line('Si no solicitó un restablecimiento de contraseña, no se requiere ninguna acción adicional.');
    }

}
