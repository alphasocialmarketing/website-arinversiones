<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use Illuminate\Database\Eloquent\SoftDeletes;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Loteos extends Model
{
    use CrudTrait;
    use SoftDeletes;
    use Sluggable, SluggableScopeHelpers;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'loteos';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['nombre', 'imagenes','principal','direccion','descripcion','intro','cerrado','posesion','financiado','localidad_id','asesor_id','direccion_tmp','lat','lng','plano_mensura','mostrar','moneda','ocultar_precio'];
    protected $hidden = ['deleted_at'];
    // protected $dates = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getImagen(){
        return str_replace("\\", "/",$this->principal);
    }
    
    public function getImagenesToJson() {       
        if($this->imagenes!=null){      
            
            $datos = json_decode($this->imagenes,true); 
            
            if(empty($datos[0]))
                return null;
            else
                return json_decode($this->imagenes,true);
        }
        else{
            return null;
        }
                        
    }
    public function getUnaImagen() {        
        if($this->imagenes!=null){          
            $img = json_decode($this->imagenes,true);
            
            if(empty($img[0]))
                return 'images/no-disponible.png';
            else            
                return $img[0]['imagen'];
        }
        else{
            return 'images/no-disponible.png';
        }
                        
    }

    public function getImagenPreview(){
        if($this->principal==null){
            return '<img src="'.asset('images/no-disponible.png').'" class="img-responsive" style="height:100px;">';
        }
        else{
            return '<img src="'.asset($this->principal).'" class="img-responsive" style="height:100px;">';
        }   
    }     

    public function getDireccionByJson(){        
        if(!empty($this->direccion)){
            $tmp = json_decode($this->direccion);
            //$dir = $tmp->name.", ".$tmp->city.", ".$tmp->country;
            $dir = $tmp->value;

            return $dir;
        }
        else{
            return $this->direccion_tmp;
        }
    }

    public function getLotes(){            
        return '<a href="plano-mensura/lote/'.$this->id.'">Cargar Lotes</a>';       
    }

    public function getCountLotesVendidos() {
        return count($this->lotes()->where("estado","VENDIDO")->get());
    }
    public function getCountLotesReservados() {

        return count($this->lotes()->where("estado","RESERVADO")->get());
    }
    public function getCountLotesDisponibles() {

        return count($this->lotes()->where("estado","DISPONIBLE")->get());
    }        
    
    
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function asesor()
    {
        return $this->belongsTo('App\Models\Asesores', 'asesor_id');
    }
    public function localidad()
    {
        return $this->belongsTo('App\Models\Localidades');
    }     
    public function lotes()
    {
        return $this->hasMany('App\Models\LoteosDetalle', 'loteo_id');
    }  
    public function servicios()
    {
        return $this->belongsToMany('App\Models\Servicios','loteos_servicios');
    }      
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->nombre;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setPlanoMensuraAttribute($value)
    {

        $attribute_name = "plano_mensura";
        $disk = "public";
        $destination_path = "plano_mensura/lote_".$this->id;

        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }        
    }
}
