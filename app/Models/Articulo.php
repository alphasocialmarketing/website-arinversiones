<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use Illuminate\Database\Eloquent\SoftDeletes;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\User;

class Articulo extends Model
{
	use CrudTrait;
	use SoftDeletes;
	use Sluggable, SluggableScopeHelpers;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'articulos';
	protected $primaryKey = 'id';
	public $timestamps = true;
	// protected $guarded = ['id'];	
	protected $fillable = ['categoria_blog_id', 'fecha', 'titulo', 'slug', 'intro', 'contenido', 'imagen', 'usuario_id'];
	protected $hidden = ['deleted_at'];
    // protected $dates = [];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/	
	public function getNombreAutor(){

		return User::find($this->usuario_id)->name;
	}

	public function getFormatFechaCorta(){
		return Carbon::createFromFormat('Y-m-d', $this->fecha)->format('d M Y');				
	}

	public function getImagen(){
		return str_replace("\\", "/",$this->imagen);
	}
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
    public function categoria()
    {
        return $this->belongsTo('App\Models\Categoriablog', 'categoria_blog_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tagblog', 'articulos_tagblog');
    }

    public function usuario()
    {
        return $this->belongsTo('App\User', 'usuario_id');
    }

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->titulo;
    }
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/

	public function setUsuarioIdAttribute($value)
    {
    	$u = \Auth::user();        

        if($u->hasRole('usuario')){
    		$this->attributes['usuario_id'] = $u->id;
        }
    	else{
    		if(isset($this->attributes['usuario_id'])){
	    		if( $this->attributes['usuario_id'] == $u->id ){
	    			$this->attributes['usuario_id'] = $u->id;
	    		}
	    		else{
	    			$this->attributes['usuario_id'] = $this->attributes['usuario_id'];
	    		}
	    	}
	    	else{
	    		$this->attributes['usuario_id'] = $value;
	    	}
    	}    		    
    }
}
