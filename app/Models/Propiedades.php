<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use Illuminate\Database\Eloquent\SoftDeletes;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Propiedades extends Model
{
    use CrudTrait;
    use SoftDeletes;
    use Sluggable, SluggableScopeHelpers;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'propiedades';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['nombre', 'imagenes','principal','direccion','intro','habitaciones','banios','cochera','terminada','gas','precio','medidas','descripcion','barrio_id','asesor_id','direccion_tmp','lat','lng','mostrar','moneda','ocultar_precio'];
    protected $hidden = ['deleted_at'];
    // protected $dates = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getImagen(){
        return str_replace("\\", "/",$this->principal);
    }
    public function getImagenesToJson() {       
        if($this->imagenes!=null){      
            
            $datos = json_decode($this->imagenes,true); 
            
            if(empty($datos[0]))
                return null;
            else
                return json_decode($this->imagenes,true);
        }
        else{
            return null;
        }
                        
    }
    public function getUnaImagen() {        
        if($this->imagenes!=null){          
            $img = json_decode($this->imagenes,true);
            
            if(empty($img[0]))
                return 'images/no-disponible.png';
            else            
                return $img[0]['imagen'];
        }
        else{
            return 'images/no-disponible.png';
        }
                        
    }

    public function getImagenPreview(){
        if($this->principal==null){
            return '<img src="'.asset('images/no-disponible.png').'" class="img-responsive" style="height:100px;">';
        }
        else{
            return '<img src="'.asset($this->principal).'" class="img-responsive" style="height:100px;">';
        }   
    }     

    public function getDireccionByJson(){        
        if(!empty($this->direccion)){
            $tmp = json_decode($this->direccion);            
            //$dir = $tmp->name.", ".$tmp->city.", ".$tmp->country;
            $dir = $tmp->value;

            return $dir;
        }
        else{
            return $this->direccion_tmp;
        }
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function asesor()
    {
        return $this->BelongsTo('App\Models\Asesores');
    }

    public function barrio()
    {
        return $this->BelongsTo('App\Models\Barrios');
    }

    public function consultaPropiedades()
    {
        return $this->hasMany('App\Models\ConsultasPropiedades');
    }
    public function consultaAserores()
    {
        return $this->hasMany('App\Models\ConsultasAsesores');
    }    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->nombre;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
