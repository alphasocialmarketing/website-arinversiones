<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
	use CrudTrait;
	use SoftDeletes;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'sliders';
	protected $primaryKey = 'id';
	public $timestamps = true;
	// protected $guarded = ['id'];
	protected $fillable = ['fondo', 'titulo', 'detalle', 'link', 'boton','orientacion'];
	protected $hidden = ['deleted_at'];
    // protected $dates = [];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	public function getFondo(){
		return str_replace("\\", "/",$this->fondo);
	}
	public function getImagenPreview(){
		if($this->fondo==null){
			return '<img src="'.asset('images/no-disponible.png').'" class="img-responsive" style="height:100px;">';
		}
		else{
			return '<img src="'.asset($this->fondo).'" class="img-responsive" style="height:100px;">';
		}  	
	}	


	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
