<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use Illuminate\Database\Eloquent\SoftDeletes;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Asesores extends Model
{
    use CrudTrait;
    use SoftDeletes;
    use Sluggable, SluggableScopeHelpers;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'asesores';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['puesto', 'nombre', 'profesion', 'imagen', 'email','celular','skype','facebook','twitter','instagram','biografia','informacion','destacado','slug','user_id'];
    protected $hidden = ['deleted_at'];
    // protected $dates = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */    
    public function getImagenPreview(){
        if($this->imagen==null){
            return '<img src="'.asset('images/no-disponible.png').'" class="img-responsive" style="height:100px;">';
        }
        else{
            return '<img src="'.asset($this->imagen).'" class="img-responsive" style="height:100px;">';
        }   
    }

    public function getUsuario(){       
        if($this->usuario()->first()->hasRole("superadmin"))
            return $this->usuario()->first()->name." - ADMINISTRADOR";
        else
            return $this->usuario()->first()->name." - VENDEDOR";
    }   
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function propiedades()
    {
        return $this->hasMany('App\Models\Propiedades', 'asesor_id');
    }
    public function loteos()
    {
        return $this->hasMany('App\Models\Loteos');
    }
    public function loteosDetalles()
    {
        return $this->hasMany('App\Models\LoteosDetalle');
    }
    public function usuario(){        
        return $this->belongsTo('App\User','user_id');        
    }
    public function consulta()
    {
        return $this->hasMany('App\Models\ConsultasAsesores');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->nombre;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
