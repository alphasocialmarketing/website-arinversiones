<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class NuevaConsultaPropiedad extends Mailable
{
    use Queueable, SerializesModels;

    public $consulta;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->consulta = $request;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
            
        $email_copia_send = config("app.email_copia_send");
        $nombre_copia_send = config("app.nombre_copia_send");
        
        return $this->
                    to($this->consulta->propiedad->asesor->email, $this->consulta->propiedad->asesor->nombre)
                    ->cc("$email_copia_send", "$nombre_copia_send")
                    ->subject("Nueva consulta de propiedad")                        
                    ->view('emails.nueva_consulta_propiedad');


    }
}
