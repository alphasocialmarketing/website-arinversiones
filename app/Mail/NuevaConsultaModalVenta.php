<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class NuevaConsultaModalVenta extends Mailable
{
    use Queueable, SerializesModels;

    public $contacto;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->contacto = $request;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.nueva_consulta_modal_propiedad')
                    ->subject("Nueva consulta por venta de propiedad");        
    }
}
