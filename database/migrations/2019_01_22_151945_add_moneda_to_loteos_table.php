<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonedaToLoteosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loteos', function (Blueprint $table) {   
            $table->enum('moneda', ['PESO','DOLAR'])->default('PESO');
            $table->boolean('ocultar_precio')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loteos', function (Blueprint $table) {            
            $table->dropColumn('moneda');
            $table->dropColumn('ocultar_precio');
        });
    }
}
