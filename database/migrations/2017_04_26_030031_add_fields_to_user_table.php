<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {            
            $table->string('direccion')->nullable();
            $table->string('localidad')->nullable();
            $table->string('empresa')->nullable();
            $table->string('cuit')->nullable();
            $table->string('web')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('direccion');
            $table->dropColumn('localidad');
            $table->dropColumn('empresa');
            $table->dropColumn('cuit');
            $table->dropColumn('web');
            $table->dropColumn('telefono');
            $table->dropColumn('celular');
        }); 
    }
}
