<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropiedadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedades', function (Blueprint $table) {
            $table->increments('id');                        
            $table->string('nombre');                                
            $table->text('imagenes')->nullable();
            $table->string('direccion',1000)->nullable();
            $table->string('intro')->nullable();
            $table->string('habitaciones')->nullable();
            $table->string('banios')->nullable();
            $table->boolean('cochera')->default(0);
            $table->boolean('terminada')->default(0);
            $table->boolean('gas')->default(0);
            $table->decimal('precio',10,2)->default(0);
            $table->integer('medidas')->nullable();                
            $table->text('descripcion')->nullable();
            $table->integer('barrio_id')->nullable(); 
            $table->integer('asesor_id')->nullable();                 
                        
        
            $table->timestamps();
            $table->softDeletes();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propiedades');
    }
}
