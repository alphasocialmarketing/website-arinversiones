<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoteosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loteos', function (Blueprint $table) {
            $table->increments('id');                        
            $table->string('nombre');                                
            $table->text('imagenes')->nullable();
            $table->string('direccion',1000)->nullable();
            $table->string('intro')->nullable();
            $table->boolean('cerrado')->default(0);
            $table->boolean('posesion')->default(0);
            $table->boolean('financiado')->default(0);
            $table->integer('medidas')->nullable();                
            $table->text('descripcion')->nullable();
            $table->integer('localidad_id')->nullable(); 
            $table->integer('asesor_id')->nullable();                 
                        
        
            $table->timestamps();
            $table->softDeletes();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loteos');
    }
}
