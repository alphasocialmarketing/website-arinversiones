<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoteoDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loteos_detalle', function (Blueprint $table) {
            $table->increments('id');                        
            $table->string('nombre')->nullable();                
            $table->text('detalle')->nullable();                
            $table->string('ref_x')->nullable();                
            $table->string('ref_y')->nullable();      
            $table->decimal('precio',10,2)->default(0);
            $table->integer('asesor_id')->nullable();               
            $table->integer('loteo_id')->nullable();                 
            $table->enum('estado', ['DISPONIBLE', 'RESERVADO', 'VENDIDO'])->nullable();             

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loteos_detalle');
    }
}
