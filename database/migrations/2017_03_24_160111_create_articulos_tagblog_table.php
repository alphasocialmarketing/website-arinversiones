<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTagBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('articulos_tagblog', function (Blueprint $table) {                    
            $table->increments('id');
            $table->integer('articulo_id')->unsigned();
            $table->integer('tagblog_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });   

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulos_tagblog');
    }
}
