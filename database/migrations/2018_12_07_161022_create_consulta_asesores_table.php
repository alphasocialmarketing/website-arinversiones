<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaAsesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas_asesores', function (Blueprint $table) {
            $table->increments('id');                        
            $table->string('nombre');            
            $table->string('email');            
            $table->string('telefono')->nullable();
            $table->string('asunto')->nullable();        
            $table->text('mensaje')->nullable();                

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consultas_asesores');
    }
}
