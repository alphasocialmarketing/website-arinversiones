<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultasVentaPropiedadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas_venta_propiedad', function (Blueprint $table) {
            $table->increments('id');                        
            $table->string('nombre');            
            $table->string('email');            
            $table->string('telefono')->nullable();
            $table->text('informacion')->nullable();                

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consultas_venta_propiedad');
    }
}
