<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToLoteosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loteos', function (Blueprint $table) {   
            $table->string('direccion_tmp')->nullable(); 
            $table->string('lng')->nullable(); 
            $table->string('lat')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loteos', function (Blueprint $table) {
            $table->dropColumn('direccion_tmp');
            $table->dropColumn('lng');
            $table->dropColumn('lat');
        });
    }
}
