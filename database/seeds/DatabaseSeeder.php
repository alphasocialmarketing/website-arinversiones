<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //se no encuentra los seeder, ejecutar la siguiente sentencia, "composer dump-autoload"

        $this->call("UserTableSeeder");
        $this->call("SettingsTableSeeder");
        $this->call("ScriptTableSeeder");
    }
}
