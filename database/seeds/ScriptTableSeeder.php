<?php

use Illuminate\Database\Seeder;

class ScriptTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scripts')->insert([
            'id' => 1,
            'referencia' => 'Chat Online',                        
            'script' => '',                        
            'page' => '',                                    
        ]);    	
        DB::table('scripts')->insert([
            'id' => 2,
            'referencia' => 'Google Analytics',                        
            'script' => '',                        
            'page' => '',                                    
        ]);
        DB::table('scripts')->insert([
            'id' => 3,
            'referencia' => 'Google Conversión',                        
            'script' => '',                        
            'page' => 'gracias',                                   
        ]);                
    }
}
