<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('settings')->insert([
            'key'           => 'mensaje_principal_superusuario',
            'name'          => 'Mensaje principal para los superusuarios',
            'description'   => 'Mensaje que se visualiza en el panel principal del administrador',
            'value'         => 'Estas en el sistema administrador del portal web.<br>Para poder operar, navegue por los distintos items del menu de la izquierda.',
            'field'         => '{"name":"value","label":"Valor","type":"ckeditor"}',
            'active'        => 1,
        ]);

        DB::table('settings')->insert([
            'key'           => 'mensaje_principal_usuario',
            'name'          => 'Mensaje principal para los usuarios',
            'description'   => 'Mensaje que se visualiza en el panel principal del administrador',
            'value'         => 'Estas en el sistema administrador del portal web.<br>Para poder operar, navegue por los distintos items del menu de la izquierda.',
            'field'         => '{"name":"value","label":"Valor","type":"ckeditor"}',
            'active'        => 1,
        ]);

        DB::table('settings')->insert([
            'key'           => 'mensaje_principal_cliente',
            'name'          => 'Mensaje principal para los clientes',
            'description'   => 'Mensaje que se visualiza en el panel principal del administrador',
            'value'         => 'Estas en el sistema administrador del portal web.<br>Para poder operar, navegue por los distintos items del menu de la izquierda.',
            'field'         => '{"name":"value","label":"Valor","type":"ckeditor"}',
            'active'        => 1,
        ]);       

        DB::table('settings')->insert([
            'key'           => 'meta-title',
            'name'          => 'Titulo de la web',
            'description'   => 'Se visualizara en la parte superior, en la ventana del navegador',
            'value'         => 'Sistema Base',
            'field'         => '{"name":"value","label":"Valor","type":"text"}',
            'active'        => 1,
        ]);       

        DB::table('settings')->insert([
            'key'           => 'meta-description',
            'name'          => 'Descripcion de la web',
            'description'   => 'Se visualizara en los buscadores como Google o Bing',
            'value'         => 'Este es un software estandar para crear sitios web con ecommerce integrado',
            'field'         => '{"name":"value","label":"Valor","type":"text"}',
            'active'        => 1,
        ]);       

        DB::table('settings')->insert([
            'key'           => 'meta-keywords',
            'name'          => 'Keywords de la web',
            'description'   => 'Se usa para indexación de los buscadores como Google o Bing',
            'value'         => 'sitio web, ecommerce, comercio electronico, software',
            'field'         => '{"name":"value","label":"Valor","type":"text"}',
            'active'        => 1,
        ]);                                      
       
        DB::table('settings')->insert([
            'key'           => 'meta-author',
            'name'          => 'Author de la web',
            'description'   => 'Se usa para indexación',
            'value'         => 'www.alphasocialmarketing.com',
            'field'         => '{"name":"value","label":"Valor","type":"text"}',
            'active'        => 1,
        ]);              

        DB::table('settings')->insert([
            'key'           => 'color-principal',
            'name'          => 'Color principal del ecommerce',
            'description'   => 'El color seleccionado es el que se va a utilizar en el ecommerce como el principal',
            'value'         => '#96c72e',
            'field'         => '{"name":"value","label":"Valor","type":"color_picker"}',
            'active'        => 1,
        ]);  

        DB::table('settings')->insert([
            'key'           => 'afip-qr',
            'name'          => 'Codigo QR Afip',
            'description'   => 'Es obligación de afip agregarlo al sitio web de la empresa',
            'value'         => '',
            'field'         => '{"name":"value","label":"Valor","type":"textarea"}',
            'active'        => 1,
        ]);        
       
    }
}
