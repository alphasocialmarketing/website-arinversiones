<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ROLES
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'superadmin',                        
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'vendedor',                        
        ]);                

        //USUARIOS
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Matias Carbini',
            'email' => 'matias.carbini@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'vendedor',
            'email' => 'vendedor@prueba.com',
            'password' => bcrypt('123456'),
        ]);        
        
        //ROLES - USUARIOS
        DB::table('role_users')->insert([
            'role_id' => 1,
            'user_id' => 1,                                            
        ]);
        DB::table('role_users')->insert([
            'role_id' => 2,
            'user_id' => 2,                                            
        ]);                     
    
    }
}
