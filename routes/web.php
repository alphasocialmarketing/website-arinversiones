<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// ##########################################
// ##########################################
// ##########################################
// ##########################################
//            PORTAL WEB - SITIO
// ##########################################
// ##########################################
// ##########################################
// ##########################################

    // -- INDEX
    Route::get('/', 'ViewController@index');

    // -- ASESORES
    Route::get('asesores', 'ViewController@asesores');
    Route::get('asesores/{slug}', 'ViewController@asesoresDetalle');
    
    // -- BLOG    
    //Route::get('blog', 'ViewController@blog');
    //Route::get('blog/categoria/{slug}', 'ViewController@blogByCategoria');
    //Route::get('blog/tag/{slug}', 'ViewController@blogByTag');
    //Route::get('blog/{slug}', 'ViewController@blogDetalle');


    // -- PAGINAS    
    Route::get('propiedades', 'ViewController@propiedades');
    Route::get('propiedades/{slug}', 'ViewController@propiedadDetalle');    
    Route::get('loteos', 'ViewController@loteos');
    Route::get('loteos/{slug}', 'ViewController@loteoDetalle');
    Route::get('imprimir/{slug}', 'ViewController@imprimir');
    Route::get('contacto', 'ViewController@contacto');
    Route::post('newsletter/guardar','FormularioController@newsletterGuardar');
    // -- AUTH
    Route::get('/login', 'AdminController@viewLogin');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/registrar', '\App\Http\Controllers\Auth\RegisterController@register');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::post('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::post('/recuperar','\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail');


// ##########################################
// ##########################################
//            REDIRECT ADMINISTRADOR
// ##########################################
// ##########################################

// -- REDIRECT ADMIN
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function()
{
    Route::get('/', 'AdminController@redirect');    
    Route::get('dashboard', 'AdminController@viewDashboard');    
});

// ##########################################
// ##########################################
// ##########################################
// ##########################################
//      PERFIL SUPERADMIN ADMINISTRADOR
// ##########################################
// ##########################################
// ##########################################
// ##########################################

// -- ADMIN BACKPACK PERFIL SUPERADMIN
Route::group(['prefix' => 'admin', 'middleware' => ['auth','role_superadmin'], 'namespace' => 'Admin'], function () {    

    CRUD::resource('slider', 'SliderCrudController');
    CRUD::resource('asesores', 'AsesoresCrudController');
    
    CRUD::resource('loteos-admin', 'LoteosAdminCrudController');
    CRUD::resource('propiedades-admin', 'PropiedadesAdminCrudController');   

    CRUD::resource('servicios', 'ServiciosCrudController');
    CRUD::resource('barrios', 'BarriosCrudController');
    CRUD::resource('localidades', 'LocalidadCrudController');

    CRUD::resource('consultas', 'ConsultasCrudController');
    CRUD::resource('consultas-venta-propiedad', 'ConsultasVentaPropiedadCrudController');
    CRUD::resource('newsletter', 'NewsletterCrudController'); 

    CRUD::resource('script', 'ScriptCrudController');                 
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth','role_todos'], 'namespace' => 'Admin'], function () {
    CRUD::resource('loteos', 'LoteosCrudController');
    CRUD::resource('propiedades', 'PropiedadesCrudController');   

    CRUD::resource('consultas-asesores', 'ConsultasAsesoresCrudController'); 
    CRUD::resource('consultas-propiedades', 'ConsultasPropiedadesCrudController');
    CRUD::resource('consultas-loteos', 'ConsultasLoteosCrudController');
    CRUD::resource('consultas-loteos', 'ConsultasLoteosCrudController');     

    CRUD::resource('articulo', 'ArticuloCrudController');
    CRUD::resource('perfil', 'UserCrudController');    
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth','role_todos']], function () {
    Route::get('plano-mensura/lote/{id}', 'AdminController@viewLoteosPlanoMensura');   
});

