<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// trae todos los loteos y propiedades
Route::get('propiedades', 'API\PropiedadesController@seleccionarPropiedades');
Route::get('loteos', 'API\LoteosController@seleccionarLoteos');

// trae todos los loteos y propiedades segun filtro
Route::post('buscador/propiedades/filtrar','API\PropiedadesController@filtrar');
Route::post('buscador/loteos/filtrar','API\LoteosController@filtrar');

// trae el loteo o propiedad segun el id
Route::get('propiedades/{id}','API\PropiedadesController@seleccionarPropiedadesById');
Route::get('loteos/{id}','API\LoteosController@seleccionarLoteosById');

Route::post('plano-mensura/crear', 'API\LoteosController@crearLote');
Route::post('plano-mensura/borrar', 'API\LoteosController@borrarLote');
Route::post('plano-mensura/obtener', 'API\LoteosController@obtenerLotes');
Route::get('plano-mensura/obtener/asesores', 'API\LoteosController@obtenerAsesores');

Route::post('formulario/consulta/home', 'API\FormularioController@consultaGuardar');
Route::post('formulario/asesores', 'API\FormularioAsesorController@EnviarConsulta');
Route::post('formulario/propiedades', 'API\FormularioPropiedadController@ConsultaEnviar');
Route::post('formulario/loteos', 'API\FormularioLoteoController@ConsultaEnviar');

Route::post('modal/venta/propiedad', 'API\ModalVentaController@ConsultaEnviar');